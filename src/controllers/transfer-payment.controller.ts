import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {transferPaymentTransformer} from '../common/transformers';
import {TransferPayment} from '../models';
import {CartItemRepository, CourseRepository, GlobalPaymentRepository, GlobalTestRepository, LocalPaymentRepository, OrderRepository, PaymentMethodRepository, ShopItemRepository} from '../repositories';
import { TransferPaymentRepository } from '../repositories/transfer-payment.repository';
import {TransferPaymentResponse} from '../types/responses';

export class TransferPaymentController {
  constructor(
    @repository(TransferPaymentRepository)
    public transferPaymentRepository : TransferPaymentRepository,

    @repository(ShopItemRepository)
    public shopItemRepository : ShopItemRepository,


    @repository(CartItemRepository)
    public cartItemRepository : CartItemRepository,


    @repository(CourseRepository)
    public courseRepository : CourseRepository,


    @repository(GlobalTestRepository)
    public globalTestRepository : GlobalTestRepository,


    @repository(GlobalPaymentRepository)
    public globalPaymentRepository : GlobalPaymentRepository,


    @repository(PaymentMethodRepository)
    public paymentMethodRepository : PaymentMethodRepository,


    @repository(LocalPaymentRepository)
    public localPaymentRepository : LocalPaymentRepository,


    @repository(OrderRepository)
    public orderRepository : OrderRepository
  ) {}

  @post('/transfer-payments')
  @response(200, {
    description: 'TransferPayment model instance',
    content: {'application/json': {schema: getModelSchemaRef(TransferPayment)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TransferPayment, {
            title: 'NewTransferPayment',
            exclude: ['id'],
          }),
        },
      },
    })
    transferPayment: Omit<TransferPayment, 'id'>,
  ): Promise<TransferPayment> {
    return this.transferPaymentRepository.create(transferPayment);
  }

  @get('/transfer-payments/count')
  @response(200, {
    description: 'TransferPayment model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(TransferPayment) where?: Where<TransferPayment>,
  ): Promise<Count> {
    return this.transferPaymentRepository.count(where);
  }

  @get('/transfer-payments')
  @response(200, {
    description: 'Array of TransferPayment model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TransferPayment, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(TransferPayment) filter?: Filter<TransferPayment>,
  ): Promise<TransferPaymentResponse[]> {
    const transferPayments = await transferPaymentTransformer(await this.transferPaymentRepository.find(filter),
    this.shopItemRepository,
    this.cartItemRepository,
    this.courseRepository,
    this.globalTestRepository,
    this.globalPaymentRepository,
    this.paymentMethodRepository,
    this.localPaymentRepository,
    this.orderRepository);
    return transferPayments;
  }

  @patch('/transfer-payments')
  @response(200, {
    description: 'TransferPayment PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TransferPayment, {partial: true}),
        },
      },
    })
    transferPayment: TransferPayment,
    @param.where(TransferPayment) where?: Where<TransferPayment>,
  ): Promise<Count> {
    return this.transferPaymentRepository.updateAll(transferPayment, where);
  }

  @get('/transfer-payments/{id}')
  @response(200, {
    description: 'TransferPayment model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(TransferPayment, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(TransferPayment, {exclude: 'where'}) filter?: FilterExcludingWhere<TransferPayment>
  ): Promise<TransferPayment> {
    return this.transferPaymentRepository.findById(id, filter);
  }

  @patch('/transfer-payments/{id}')
  @response(204, {
    description: 'TransferPayment PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TransferPayment, {partial: true}),
        },
      },
    })
    transferPayment: TransferPayment,
  ): Promise<void> {
    await this.transferPaymentRepository.updateById(id, transferPayment);
  }

  @put('/transfer-payments/{id}')
  @response(204, {
    description: 'TransferPayment PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() transferPayment: TransferPayment,
  ): Promise<void> {
    await this.transferPaymentRepository.replaceById(id, transferPayment);
  }

  @del('/transfer-payments/{id}')
  @response(204, {
    description: 'TransferPayment DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.transferPaymentRepository.deleteById(id);
  }
}
