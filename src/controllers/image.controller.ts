import {post, requestBody, response} from '@loopback/rest';
import {generateImageFileFromBase64} from '../common';

export class ImageController {
  constructor() {}

  @post('/images')
  @response(201)
  async create(
    @requestBody({
      base64Image: String,
      baseName: String,
      extenstion: String || undefined,
    })
    base64Image: string,
    baseName: string,
    extenstion?: string,
  ): Promise<{link: string}> {
    return {
      link: await generateImageFileFromBase64(
        baseName,
        base64Image,
        extenstion ?? '' + Math.floor(Math.random() * 22424),
      ),
    };
  }
}
