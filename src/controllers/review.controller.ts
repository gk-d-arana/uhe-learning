import {repository} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {isStudent} from '../common/middleware';
import {Review} from '../models';
import {ReviewObjectClass} from '../models/review.model';
import {ReviewRepository} from '../repositories';
import {UserRepository} from '../repositories/user.repository';
import {CourseReview} from '../types/responses';
import { deleteImage } from '../common';

export class ReviewController {
  constructor(
    @repository(ReviewRepository)
    public reviewRepository: ReviewRepository,

    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @post('/reviews')
  @response(201, {
    description: 'Review model instance',
    content: {'application/json': {schema: getModelSchemaRef(Review)}},
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ReviewObjectClass, {
            title: 'NewReview',
          }),
        },
      },
    })
    review: ReviewObjectClass,
  ): Promise<{message: string; id: number | undefined}> {
    const userId = await isStudent(token, this.userRepository);
    const res = await this.reviewRepository.addReview(review, userId);
    if (res) {
      return {
        message: 'success',
        id: res.id,
      };
    } else {
      return {
        message: 'error',
        id: -1
      };
    }

  }

  @get('/reviews')
  @response(200, {
    description: 'Array of Order model instances',
  })
  async find(
    @param.query.number('course') courseId: number,
  ): Promise<CourseReview[]> {
    const res = await this.reviewRepository.findCourseReviews(
      courseId,
      this.userRepository,
    );
    return res;
  }

  @put('/reviews/{id}')
  @response(200, {
    description: 'Review PUT success',
  })
  async replaceById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @requestBody() review: ReviewObjectClass,
  ): Promise<{message: string}> {
    await isStudent(token, this.userRepository);
    review.id = id;
    const res = await this.reviewRepository.updateComment(review);
    return {
      message: res,
    };
  }

  @del('/reviews/{id}')
  @response(200, {
    description: 'Review DELETE success',
  })
  async deleteById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
  ): Promise<{message: string}> {
    const userId = await isStudent(token, this.userRepository);
    const review = await this.reviewRepository.findById(id);
    if(review.user == userId){
      if(review.voiceContent){
        deleteImage(review.voiceContent)
      }else if(review.videoContent){
        deleteImage(review.videoContent)
      }
      await this.reviewRepository.deleteById(id);
    }
    return {message: 'success'};
  }
}
