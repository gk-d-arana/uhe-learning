import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {LessonAsset} from '../models';
import {LessonAssetCreationCleass} from '../models/lesson-asset.model';
import {LessonAssetRepository} from '../repositories';
import { deleteAsset } from '../common/deleters';

export class LessonAssetController {
  constructor(
    @repository(LessonAssetRepository)
    public lessonAssetRepository: LessonAssetRepository,
  ) {}

  @post('/lesson-assets')
  @response(200, {
    description: 'LessonAsset model instance',
    content: {'application/json': {schema: getModelSchemaRef(LessonAsset)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LessonAssetCreationCleass, {
            title: 'NewLessonAsset',
          }),
        },
      },
    })
    lessonAsset: LessonAssetCreationCleass,
  ): Promise<LessonAsset> {
    return this.lessonAssetRepository.create(lessonAsset);
  }

  @get('/lesson-assets/count')
  @response(200, {
    description: 'LessonAsset model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(LessonAsset) where?: Where<LessonAsset>,
  ): Promise<Count> {
    return this.lessonAssetRepository.count(where);
  }

  @get('/lesson-assets')
  @response(200, {
    description: 'Array of LessonAsset model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(LessonAsset, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(LessonAsset) filter?: Filter<LessonAsset>,
  ): Promise<LessonAsset[]> {
    return this.lessonAssetRepository.find(filter);
  }

  @patch('/lesson-assets')
  @response(200, {
    description: 'LessonAsset PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LessonAsset, {partial: true}),
        },
      },
    })
    lessonAsset: LessonAsset,
    @param.where(LessonAsset) where?: Where<LessonAsset>,
  ): Promise<Count> {
    return this.lessonAssetRepository.updateAll(lessonAsset, where);
  }

  @get('/lesson-assets/{id}')
  @response(200, {
    description: 'LessonAsset model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(LessonAsset, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(LessonAsset, {exclude: 'where'})
    filter?: FilterExcludingWhere<LessonAsset>,
  ): Promise<LessonAsset> {
    return this.lessonAssetRepository.findById(id, filter);
  }

  @patch('/lesson-assets/{id}')
  @response(204, {
    description: 'LessonAsset PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LessonAsset, {partial: true}),
        },
      },
    })
    lessonAsset: LessonAsset,
  ): Promise<void> {
    await this.lessonAssetRepository.updateById(id, lessonAsset);
  }

  @put('/lesson-assets/{id}')
  @response(200, {
    description: 'LessonAsset PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LessonAssetCreationCleass, {
            title: 'NewVideo',
          }),
        },
      },
    })
    lessonAsset: LessonAssetCreationCleass,
  ){
    const lessonAssetDb = await this.lessonAssetRepository.findById(id);
    await this.lessonAssetRepository.updateLessonAsset(lessonAssetDb, lessonAsset);
    return {message: 'success'}
  }

  @del('/lesson-assets/{id}')
  @response(200, {
    description: 'LessonAsset DELETE success',
  })
  async deleteById(@param.path.number('id') id: number){
    const lessonAssetDb = await this.lessonAssetRepository.findById(id);
    deleteAsset(lessonAssetDb, this.lessonAssetRepository);
    return {message: 'success'}
  }
}
