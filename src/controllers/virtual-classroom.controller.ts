import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {generateImageFileFromBase64} from '../common/generator';
import {canAddCourse, isAdmin, isInstructor, isStudent} from '../common/middleware';
import {VirtualClassroom} from '../models';
import {
  VirtualClassroomCreationClass,
  VRCResponse,
} from '../models/virtual-classroom.model';
import {VirtualClassroomRepository} from '../repositories';
import {ChatGroupRepository} from '../repositories/chat-group.repository';
import {UserRepository} from '../repositories/user.repository';
import {
  VRCInstanceMembersResponse,
  VRCInstanceResponse,
} from '../types/responses';
import { deleteImage } from '../common';

export class VirtualClassroomController {
  constructor(
    @repository(VirtualClassroomRepository)
    public virtualClassroomRepository: VirtualClassroomRepository,

    @repository(UserRepository)
    public userRepository: UserRepository,

    @repository(ChatGroupRepository)
    public chatGroupRepository: ChatGroupRepository,
  ) { }

  @post('/virtual-classrooms')
  @response(200, {
    description: 'VirtualClassroom model instance',
    content: {
      'application/json': {schema: getModelSchemaRef(VirtualClassroom)},
    },
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(VirtualClassroomCreationClass),
        },
      },
    })
    virtualClassroom: VirtualClassroomCreationClass,
  ): Promise<VirtualClassroom> {
    const userId = await canAddCourse(token, this.userRepository);
    virtualClassroom.user = userId;
    virtualClassroom.coverImage = await generateImageFileFromBase64(
      'virtual-classrooms',
      virtualClassroom.coverImage,
      virtualClassroom.name,
    );
    const chatGroup: {users: number[]; messages: []; name: string; user: number} = {
      users: [],
      user: userId ?? 0,
      messages: [],
      name: virtualClassroom.name,
    };
    if (userId) {
      chatGroup.users.push(userId);
    }
    virtualClassroom.chatGroup = (
      await this.chatGroupRepository.create(chatGroup)
    ).id;
    const res = await this.virtualClassroomRepository.create(virtualClassroom);
    return res;
  }

  @get('/virtual-classrooms/count')
  @response(200, {
    description: 'VirtualClassroom model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(VirtualClassroom) where?: Where<VirtualClassroom>,
  ): Promise<Count> {
    return this.virtualClassroomRepository.count(where);
  }

  @get('/virtual-classrooms')
  @response(200, {
    description: 'Array of VirtualClassroom model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(VirtualClassroom, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(VirtualClassroom) filter?: Filter<VirtualClassroom>,
  ): Promise<VRCResponse[]> {
    const resDb = await this.virtualClassroomRepository.find(filter);
    const res: VRCResponse[] = [];
    for (const vrc of resDb) {
      const instruc = await this.userRepository.find({where: {id: vrc.user}});
      if (instruc[0]) {
        res.push({
          id: vrc.id,
          name: vrc.name,
          chatGroupId: vrc.chatGroup ?? 0,
          members: [],
          coverImage: vrc.coverImage,
          instructor: {
            id: vrc.user,
            email: instruc[0].email,
            firstName: instruc[0].firstName,
            lastName: instruc[0].lastName,
            phoneNumber: instruc[0].phoneNumber,
            profileImage: instruc[0].profileImage
          }
        });
      }
    }
    return res;
  }

  @patch('/virtual-classrooms')
  @response(200, {
    description: 'VirtualClassroom PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(VirtualClassroom, {partial: true}),
        },
      },
    })
    virtualClassroom: VirtualClassroom,
    @param.where(VirtualClassroom) where?: Where<VirtualClassroom>,
  ): Promise<Count> {
    return this.virtualClassroomRepository.updateAll(virtualClassroom, where);
  }

  @get('/virtual-classrooms/{id}')
  @response(200, {
    description: 'VirtualClassroom model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(VirtualClassroom, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @param.filter(VirtualClassroom, {exclude: 'where'})
    filter?: FilterExcludingWhere<VirtualClassroom>,
  ): Promise<VRCInstanceResponse> {
    let userId;
    let isInstructorBool = false;
    try {
      userId = await isInstructor(token, this.userRepository);
      isInstructorBool = true;
    } catch (err) {
      try {
        userId = await isAdmin(token, this.userRepository);
      } catch (err) {
        throw new HttpErrors['401']();
      }
    }
    const vrc = await this.virtualClassroomRepository.findById(id, filter);
    if (isInstructorBool && vrc.user !== userId) {
      throw new HttpErrors['401']();
    }
    const res: VRCInstanceResponse = new VRCInstanceResponse();
    const chatGroup = await this.chatGroupRepository.findById(vrc.chatGroup);
    res.coverImage = vrc.coverImage;
    res.createdAt = vrc.createdAt;
    res.updatedAt = vrc.updatedAt;
    res.description = vrc.description;
    res.name = vrc.name;
    res.id = vrc.id;
    res.chatGroup = vrc.chatGroup;
    res.members = [];
    for (const user of chatGroup.users) {
      const userObj = await this.userRepository.findById(user);
      res.members.push({
        role: userObj.role,
        id: userObj.id,
        lastName: userObj.lastName,
        firstName: userObj.firstName,
        email: userObj.email,
        phoneNumber: userObj.phoneNumber,
        profileImage: userObj.profileImage,
      });
    }
    return res;
  }

  @patch('/virtual-classrooms/{id}')
  @response(204, {
    description: 'VirtualClassroom PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(VirtualClassroom, {partial: true}),
        },
      },
    })
    virtualClassroom: VirtualClassroom,
  ): Promise<void> {
    await this.virtualClassroomRepository.updateById(id, virtualClassroom);
  }


  @put('/virtual-classrooms/{id}')
  @response(200, {
    description: 'VirtualClassroom PUT success',
  })
  async updateVrc(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(VirtualClassroomCreationClass),
        },
      },
    })
    virtualClassroom: VirtualClassroomCreationClass,
  ): Promise<any> {
    const userId = await canAddCourse(token, this.userRepository);
    const vrc = await this.virtualClassroomRepository.findById(id);
    if(virtualClassroom.coverImage){
      if(vrc.coverImage){
        deleteImage(vrc.coverImage);
      }
      virtualClassroom.coverImage = await generateImageFileFromBase64(
        'virtual-classrooms',
        virtualClassroom.coverImage,
        virtualClassroom.name,
      );
    }else{
      virtualClassroom.coverImage = vrc.coverImage;  
    }
    virtualClassroom.user = userId;
    
    await this.virtualClassroomRepository.updateById(vrc.id,virtualClassroom);
    return {message: 'success'};
  }

  @put('/virtual-classrooms/{id}/add-members')
  @response(200, {
    description: 'VirtualClassroom PUT success',
  })
  async replaceById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @requestBody() data: {users: number[]},
  ): Promise<{message: string}> {
    const userId = await canAddCourse(token, this.userRepository);
    const vrc = await this.virtualClassroomRepository.findById(id);
    if (vrc.user !== userId) {
      throw new HttpErrors['401']();
    }
    const chatGroup = await this.chatGroupRepository.findById(vrc.chatGroup);
    const resSet = new Set(chatGroup.users.concat(data.users));
    chatGroup.users = [...resSet];
    await this.chatGroupRepository.updateById(chatGroup.id, chatGroup);
    return {message: 'success'};
  }

  @get('/vrc-students')
  @response(200, {
    description: 'Array of Student model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
        },
      },
    },
  })
  async findVrcUsers(
    @param.header.string('Authorization') token: string,
  ): Promise<VRCInstanceMembersResponse[]> {
    await canAddCourse(token, this.userRepository);
    const users = await this.userRepository.find({});
    const res = [];
    for (const user of users) {
      res.push({
        id: user.id,
        role: user.role,
        firstName: user.firstName,
        lastName: user.lastName,
        phoneNumber: user.phoneNumber,
        profileImage: user.profileImage,
        email: user.email,
      });
    }
    return res;
  }

  @del('/virtual-classrooms/{id}')
  @response(200, {
    description: 'VirtualClassroom DELETE success',
  })
  async deleteById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
  ): Promise<any> {
    const userId = await canAddCourse(token, this.userRepository);
    const vrc = await this.virtualClassroomRepository.findById(id);
    if (vrc.user !== userId) {
      throw new HttpErrors['401']();
    }
    if(vrc.coverImage){
      deleteImage(vrc.coverImage);
    }
    await this.virtualClassroomRepository.deleteById(id);
    return {message: 'success'};
  }


  @get('/my-virtual-classrooms')
  @response(200, {
    description: 'Array of VirtualClassroom model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(VirtualClassroom, {includeRelations: true}),
        },
      },
    },
  })
  async findMyVrcs(
    @param.header.string('Authorization') token: string,
    @param.filter(VirtualClassroom) filter?: Filter<VirtualClassroom>,
  ): Promise<any> {
    const res: VRCResponse[] = [];
    const doneIds = [];
    let user;
    try {
      user = await isStudent(token, this.userRepository);
      // return await this.chatGroupRepository.execute(`SELECT * FROM ChatGroup WHERE (CAST(users AS BINARY) REGEXP BINARY '%,46,%') OR (CAST(users as BINARY) REGEXP BINARY '46,%') OR (CAST(users AS BINARY) REGEXP BINARY '%,46') ORDER BY id`)
      const myChatGroups = await this.chatGroupRepository.execute(`SELECT * FROM ChatGroup WHERE (users LIKE '%,${user},%') OR (users LIKE '[${user},%') OR (users LIKE '%,${user}]') ORDER BY id`)
      if (Array.isArray(myChatGroups)) {
        for (const myChatGroup of myChatGroups) {
          const vrc = await this.virtualClassroomRepository.find({where: {chatGroup: myChatGroup.id}});
          const users = JSON.parse(myChatGroup.users);
          const members: VRCInstanceMembersResponse[] = [];
          for (const user of users) {
            try {
              const userObj = await this.userRepository.findById(user)
              members.push({
                id: user,
                email: userObj.email,
                role: userObj.role,
                firstName: userObj.firstName,
                lastName: userObj.lastName,
                phoneNumber: userObj.phoneNumber,
                profileImage: userObj.profileImage
              })
            } catch (err) {
              //
            }
          }
          if (vrc?.[0]) {
            const instruc = await this.userRepository.find({where: {id: vrc[0].user}});
            if (instruc[0]) {
              res.push({
                id: vrc[0].id,
                name: vrc[0].name,
                chatGroupId: vrc[0].chatGroup ?? 0,
                members: members,
                coverImage: vrc[0].coverImage,
                instructor: {
                  id: vrc[0].user,
                  email: instruc[0].email,
                  role: instruc[0].role,
                  firstName: instruc[0].firstName,
                  lastName: instruc[0].lastName,
                  phoneNumber: instruc[0].phoneNumber,
                  profileImage: instruc[0].profileImage
                }
              });
            }
          }
        }
      }

      return res;
    } catch (err) {
      user = await isInstructor(token, this.userRepository);
      const myChatGroups = await this.chatGroupRepository.execute(`SELECT * FROM ChatGroup WHERE (users LIKE '%,${user},%') OR (users LIKE '[${user},%') OR (users LIKE '%,${user}]') ORDER BY id`)
      if (Array.isArray(myChatGroups)) {
        for (const myChatGroup of myChatGroups) {
          const vrc = await this.virtualClassroomRepository.find({where: {chatGroup: myChatGroup.id}});
          const users = JSON.parse(myChatGroup.users);
          const members: VRCInstanceMembersResponse[] = [];
          for (const user of users) {
            try {
              const userObj = await this.userRepository.findById(user)
              members.push({
                id: user,
                email: userObj.email,
                role: userObj.role,
                firstName: userObj.firstName,
                lastName: userObj.lastName,
                phoneNumber: userObj.phoneNumber,
                profileImage: userObj.profileImage
              })
            } catch (err) {
              //
            }
          }
          if (vrc?.[0]) {
            if (vrc[0].user == user) {
              doneIds.push(vrc[0].id);
            }
            const instruc = await this.userRepository.find({where: {id: vrc[0].user}});
            if (instruc[0]) {
              res.push({
                id: vrc[0].id,
                chatGroupId: vrc[0].chatGroup ?? 0,
                name: vrc[0].name,
                members: members,
                coverImage: vrc[0].coverImage,
                instructor: {
                  id: vrc[0].user,
                  email: instruc[0].email,
                  role: instruc[0].role,
                  firstName: instruc[0].firstName,
                  lastName: instruc[0].lastName,
                  phoneNumber: instruc[0].phoneNumber,
                  profileImage: instruc[0].profileImage
                }
              });
            }
          }
        }
      }
      const vrcs = await this.virtualClassroomRepository.find({where: {user: user}});

      for (const vrc of vrcs) {
        if (doneIds.findIndex(el => el == vrc.id) == -1) {
          try {
            const chatGroup = await this.chatGroupRepository.findById(vrc.chatGroup);
            const members: VRCInstanceMembersResponse[] = [];
            for (const user of chatGroup.users) {
              try {
                const userObj = await this.userRepository.findById(user)
                members.push({
                  id: user,
                  email: userObj.email,
                  role: userObj.role,
                  firstName: userObj.firstName,
                  lastName: userObj.lastName,
                  phoneNumber: userObj.phoneNumber,
                  profileImage: userObj.profileImage
                })
              } catch (err) {
                //
              }
            }
            const instruc = await this.userRepository.find({where: {id: user}});
            if (instruc?.[0]) {
              res.push({
                id: vrc.id,
                name: vrc.name,
                chatGroupId: vrc.chatGroup ?? 0,
                members: members,
                coverImage: vrc.coverImage,
                instructor: {
                  id: vrc.user,
                  email: instruc[0].email,
                  role: instruc[0].role,
                  firstName: instruc[0].firstName,
                  lastName: instruc[0].lastName,
                  phoneNumber: instruc[0].phoneNumber,
                  profileImage: instruc[0].profileImage
                }
              });
            }
          } catch (err) {
            //
          }
        }
      }

    }
    return res;
  }
}
