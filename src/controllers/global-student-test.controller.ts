import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response
} from '@loopback/rest';
import {isStudent} from '../common/middleware';
import {GlobalStudentTest} from '../models';
import {GlobalStudentTestRepository} from '../repositories';
import {UserRepository} from '../repositories/user.repository';

export class GlobalStudentTestController {
  constructor(
    @repository(GlobalStudentTestRepository)
    public globalStudentTestRepository : GlobalStudentTestRepository,
    @repository(UserRepository)
    public userRepository : UserRepository,
  ) {}

  @post('/global-student-tests')
  @response(200, {
    description: 'GlobalStudentTest model instance',
    content: {'application/json': {schema: getModelSchemaRef(GlobalStudentTest)}},
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
        },
      },
    })
    globalStudentTest: {globalTest: number, user?:number},
  ): Promise<GlobalStudentTest> {
    const userId = await isStudent(token, this.userRepository);
    globalStudentTest.user = userId;
    return this.globalStudentTestRepository.create(globalStudentTest);
  }

  @get('/global-student-tests/count')
  @response(200, {
    description: 'GlobalStudentTest model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(GlobalStudentTest) where?: Where<GlobalStudentTest>,
  ): Promise<Count> {
    return this.globalStudentTestRepository.count(where);
  }

  @get('/global-student-tests')
  @response(200, {
    description: 'Array of GlobalStudentTest model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(GlobalStudentTest, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(GlobalStudentTest) filter?: Filter<GlobalStudentTest>,
  ): Promise<GlobalStudentTest[]> {
    return this.globalStudentTestRepository.find(filter);
  }

  @patch('/global-student-tests')
  @response(200, {
    description: 'GlobalStudentTest PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GlobalStudentTest, {partial: true}),
        },
      },
    })
    globalStudentTest: GlobalStudentTest,
    @param.where(GlobalStudentTest) where?: Where<GlobalStudentTest>,
  ): Promise<Count> {
    return this.globalStudentTestRepository.updateAll(globalStudentTest, where);
  }

  @get('/global-student-tests/{id}')
  @response(200, {
    description: 'GlobalStudentTest model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(GlobalStudentTest, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(GlobalStudentTest, {exclude: 'where'}) filter?: FilterExcludingWhere<GlobalStudentTest>
  ): Promise<GlobalStudentTest> {
    return this.globalStudentTestRepository.findById(id, filter);
  }

  @patch('/global-student-tests/{id}')
  @response(204, {
    description: 'GlobalStudentTest PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GlobalStudentTest, {partial: true}),
        },
      },
    })
    globalStudentTest: GlobalStudentTest,
  ): Promise<void> {
    await this.globalStudentTestRepository.updateById(id, globalStudentTest);
  }

  @put('/global-student-tests/{id}')
  @response(204, {
    description: 'GlobalStudentTest PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() globalStudentTest: GlobalStudentTest,
  ): Promise<void> {
    await this.globalStudentTestRepository.replaceById(id, globalStudentTest);
  }

  @del('/global-student-tests/{id}')
  @response(200, {
    description: 'GlobalStudentTest DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<{message: string}> {
    await this.globalStudentTestRepository.deleteById(id);
    return {message: 'success'};
  }

}
