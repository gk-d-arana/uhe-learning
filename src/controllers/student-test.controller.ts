import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {
  HttpErrors,
  get,
  getModelSchemaRef,
  param,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {canAddCourse, isAdmin, isInstructor, isStudent} from '../common/middleware';
import {GlobalPayment, GlobalStudentTest, StudentTest} from '../models';
import {StudentTestRepository} from '../repositories';
import {AdminRepository} from '../repositories/admin.repository';
import {CourseRepository} from '../repositories/course.repository';
import {GlobalStudentTestRepository} from '../repositories/global-student-test.repository';
import {GlobalTestRepository} from '../repositories/global-test.repository';
import {InstructorRepository} from '../repositories/instructor.repository';
import {UserRepository} from '../repositories/user.repository';
import {FullStudentTestCreation, StudentTestCreation} from '../types/requests';
import {StudentTestListResponse, TestListResponse, TestSolvingResponse, UserTests} from '../types/responses';

export class StudentTestController {
  constructor(
    @repository(StudentTestRepository)
    public studentTestRepository: StudentTestRepository,

    @repository(AdminRepository)
    public adminRepository: AdminRepository,

    @repository(InstructorRepository)
    public instructorRepository: InstructorRepository,

    @repository(GlobalStudentTestRepository)
    public globalStudentTestRepository: GlobalStudentTestRepository,

    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,


    @repository(UserRepository)
    public userRepository: UserRepository,

    @repository(CourseRepository)
    public courseRepository: CourseRepository,
  ) { }

  @post('/student-tests')
  @response(200, {
    description: 'StudentTest model instance',
    content: {'application/json': {schema: getModelSchemaRef(StudentTest)}},
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentTestCreation, {
            title: 'NewStudentTest',
          }),
        },
      },
    })
    studentTest: StudentTestCreation,
  ): Promise<{message: string; id: number | undefined}> {
    await isStudent(token, this.userRepository);

    const res = await this.studentTestRepository.createMyTest(
      studentTest
    );
    return res;
  }

  @post('/complete-student-tests')
  @response(201, {
    description: 'StudentTest model instance',
    content: {'application/json': {schema: getModelSchemaRef(StudentTest)}},
  })
  async fullCreate(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StudentTestCreation, {
            title: 'NewStudentTest',
          }),
        },
      },
    })
    studentTestFull: FullStudentTestCreation,
  ): Promise<{message: string;}> {
    await isStudent(token, this.userRepository);
    for (const studentTest of studentTestFull.answers) {
      const studentTestObj : StudentTestCreation = {
        globalStudentTestId: studentTestFull.globalStudentTestId,
        test: studentTest.test,
        writtenAnswer : studentTest.writtenAnswer,
        optionalAnswer : studentTest.optionalAnswer,
        columnAnswers : studentTest.columnAnswers
      }
      console.log(studentTestObj);
      await this.studentTestRepository.createMyTest(
        studentTestObj
      );
    }
    
    return {message : 'success'};
  }

  @get('/student-tests')
  @response(200, {
    description: 'Array of StudentTest model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TestListResponse, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.query.number('userId') _userId: number,
    @param.filter(StudentTest) filter?: Filter<StudentTest>,
  ): Promise<TestListResponse[]> {
    try {
      const userId = await isStudent(token, this.userRepository);
      const res = await this.studentTestRepository.getTests(userId);
      return res;
    } catch (err) {
      await canAddCourse(
        token,
        this.userRepository
      );
      const res = this.studentTestRepository.getTests(_userId);
      return res;
    }
  }

  @get('/student-tests/{id}')
  @response(200, {
    description: 'StudentTest model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StudentTest, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.header.string('Authorization') token: string,
    @param.query.number('userId') _userId: number | undefined,
    @param.path.number('id') id: number,
    @param.filter(StudentTest, {exclude: 'where'})
    filter?: FilterExcludingWhere<StudentTest>,
  ): Promise<TestSolvingResponse[]> {
    try {
      const userId = await isStudent(token, this.userRepository);
      const res = await this.studentTestRepository.getTestSolves(id, userId);
      return res;
    } catch (err) {
      console.log(err);
      await canAddCourse(
        token,
        this.userRepository
      );
      const res = this.studentTestRepository.getTestSolves(id, _userId ?? 0);
      return res;
    }

  }

  @put('/student-tests/{id}')
  @response(200, {
    description: 'StudentTest model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StudentTest, {includeRelations: true}),
      },
    },
  })
  async updateCheck(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @requestBody({}) data: {isTrue: boolean}
  ): Promise<{message: string;}> {
    await canAddCourse(
      token,
      this.userRepository
    );
    const res = await this.studentTestRepository.updateTestCheck(id, data);
    return res;
  }

  @get('/dashboard-tests')
  @response(200, {
    description: 'Array of StudentTest model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TestListResponse, {includeRelations: true}),
        },
      },
    },
  })
  async findStudentsAttendingTests(
    @param.header.string('Authorization') token: string,
  ): Promise<UserTests[]> {
    const userId = await isInstructor(token, this.userRepository);
    const globalTests = await this.globalTestRepository.find({where: {user: userId}})
    const resTests: UserTests[] = []
    for (const globalTest of globalTests) {
      const studentTests = await this.globalStudentTestRepository.find({where: {globalTest: globalTest.id}})
      for (const studentTest of studentTests) {
        if (studentTest) {
          const user = await this.userRepository.findById(studentTest.user);
          const index = resTests.findIndex(el => el.userId === user.id);
          if (index !== -1) {
            resTests[index].testsCount += 1;
          } else {
            resTests.push({
              userId: user.id,
              firstName: user.firstName,
              lastName: user.lastName,
              profileImage: user.profileImage,
              testsCount: 1
            })
          }
        }
      }
    }
    return resTests;
  }

  @get('/my-dashboard-tests')
  @response(200, {
    description: 'Array of StudentTest model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(TestListResponse, {includeRelations: true}),
        },
      },
    },
  })
  async findMyAttendingTests(
    @param.header.string('Authorization') token: string,
    @param.query.string("ref") ref: "student" | "admin",
    @param.query.integer("refId") refId: number
  ): Promise<StudentTestListResponse[]> {
    let userId;
    if (ref === "student") {
      userId = await isStudent(token, this.userRepository);
    } else {
      await isAdmin(token, this.userRepository);
      userId = refId;
    }
    const studentTests = await this.globalStudentTestRepository.find({where: {user: userId}});
    const resTests: StudentTestListResponse[] = [];
    for (const studentTest of studentTests) {
      const globalTest = await this.globalTestRepository.find({where: {id: studentTest.globalTest}});
      if (globalTest?.[0]) {
        const course = await this.courseRepository.find({where: {id: globalTest[0].course}});
        resTests.push({
          id: studentTest.id,
          name: globalTest[0].name,
          createdAt: studentTest.createdAt,
          answeredAt: studentTest.updatedAt,
          course: course?.[0] ? course[0].name : '0',
          globalTest: globalTest[0].id ?? 0,
          isAnswered: studentTest.isAnswered ?? false
        })
      }
    }
    return resTests;
  }

  @get('/global-student-test-by-global-test-id/{id}')
  @response(200, {
    description: 'GlobalStudentTest model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(GlobalStudentTest, {includeRelations: true}),
      },
    },
  })
  async getByGlobalTestId(
    @param.path.number('id') globalTest: number,
    @param.header.string('Authorization') token: string
  ) {
    const userId = await isStudent(token, this.userRepository);
    const globalStudentTest = await this.globalStudentTestRepository.find({where: {user: userId, globalTest: globalTest}});
    if (globalStudentTest?.[0]) {

      return await this.findById(token, undefined, globalStudentTest[0].id ?? 0);
    } else {
      throw new HttpErrors[404];
    }
  }

  //   @get('/my-dashboard-tests-solving')
  // @response(200, {
  //   description: 'Array of StudentTest model instances',
  //   content: {
  //     'application/json': {
  //       schema: {
  //         type: 'array',
  //         items: getModelSchemaRef(TestListResponse, {includeRelations: true}),
  //       },
  //     },
  //   },
  // })
  // async findMyAttendingTestsSolving(
  //   @param.header.string('Authorization') token: string,
  //   @param.query.string("ref") ref: "student" | "admin",
  //   @param.query.integer("refId") refId: number
  // ): Promise<any[]> {
  //   let userId;
  //   if(ref === "student"){
  //     userId = await isStudent(token, this.userRepository);
  //   }else{
  //     await isAdmin(token, this.userRepository);
  //     userId = refId;
  //   }
  //   const studentTests = await this.globalStudentTestRepository.find({where:{user: userId}});
  //   const resTests: StudentTestListResponse[] = [];
  //   for (const studentTest of studentTests) {
  //     const globalTest = await this.globalTestRepository.find({where:{id:studentTest.globalTest}});
  //     if(globalTest?.[0]){
  //       const course = await this.courseRepository.find({where:{id:globalTest[0].course}});
  //       resTests.push({
  //         id: globalTest[0].id,
  //         name: globalTest[0].name,
  //         createdAt: globalTest[0].createdAt,
  //         answeredAt: studentTest.createdAt,
  //         course: course?.[0] ? course[9].name : '0'
  //       })
  //     }
  //   }
  //     return resTests;
  // }

}
