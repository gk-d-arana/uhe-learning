import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  HttpErrors,
  RequestContext,
  RestBindings,
} from '@loopback/rest';
import {Certificate} from '../models';
import {CertificateRepository} from '../repositories';
import {readFileSync} from 'fs-extra';

import { getCertificateUser, getUser } from '../common/middleware';
import { UserRepository } from '../repositories/user.repository';
import { inject } from '@loopback/core';

export class CertificateController {
  constructor(
    @inject(RestBindings.Http.CONTEXT) private requestCtx: RequestContext,
    @repository(CertificateRepository)
    public certificateRepository : CertificateRepository,
    @repository(UserRepository)
    public userRepository : UserRepository,
  ) {}

  @post('/certificates')
  @response(200, {
    description: 'Certificate model instance',
    content: {'application/json': {schema: getModelSchemaRef(Certificate)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Certificate, {
            title: 'NewCertificate',
            exclude: ['id'],
          }),
        },
      },
    })
    certificate: Omit<Certificate, 'id'>,
  ): Promise<Certificate> {
    return this.certificateRepository.create(certificate);
  }

  @get('/certificates/count')
  @response(200, {
    description: 'Certificate model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Certificate) where?: Where<Certificate>,
  ): Promise<Count> {
    return this.certificateRepository.count(where);
  }

  @get('/certificates')
  @response(200, {
    description: 'Array of Certificate model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Certificate, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string("Authorization") token: string,
    @param.filter(Certificate) filter?: Filter<Certificate>,
  ): Promise<Certificate[]> {
    const user = await getUser(token, this.userRepository);
    return this.certificateRepository.find({where:{user: user}});
    // let key = AES.encrypt(
    //   generateString(25),
    //   process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
    // ).toString();
    // const jwt = sign(
    //   {
    //     id: 28
    //   },
    //   process.env.JWT_SECRET ?? 'shourySecret',
    // );
    // console.log(jwt);
  }

  @patch('/certificates')
  @response(200, {
    description: 'Certificate PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Certificate, {partial: true}),
        },
      },
    })
    certificate: Certificate,
    @param.where(Certificate) where?: Where<Certificate>,
  ): Promise<Count> {
    return this.certificateRepository.updateAll(certificate, where);
  }

  @get('/certificates/{id}')
  @response(200, {
    description: 'Certificate model instance'
  })
  async findById(
    @param.path.number('id') id: number,
    @param.query.string("key") token: string
  ){
    const user = await getCertificateUser(token, this.userRepository);
    if(!user){
      throw new HttpErrors['403']();
    }else{
      const certificate = await this.certificateRepository.findOne({where: {user: user.id, id: id}});
      const rootPath = process.env.isProduction ? '/var/www/uh-training.com/home/uht_ssh_sr/web/uhe-learning/public/media/certificates/' : '';
      const {request, response} = this.requestCtx;
      response.setHeader("Content-Type", "application/pdf");
      response.send(readFileSync( rootPath + certificate?.name + '.pdf'));
      return response;

      // return new Buffer(img.data.toString(), 'binary')
    }
  }

  @patch('/certificates/{id}')
  @response(204, {
    description: 'Certificate PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Certificate, {partial: true}),
        },
      },
    })
    certificate: Certificate,
  ): Promise<void> {
    await this.certificateRepository.updateById(id, certificate);
  }

  @put('/certificates/{id}')
  @response(204, {
    description: 'Certificate PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() certificate: Certificate,
  ): Promise<void> {
    await this.certificateRepository.replaceById(id, certificate);
  }

  @del('/certificates/{id}')
  @response(204, {
    description: 'Certificate DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.certificateRepository.deleteById(id);
  }
}
