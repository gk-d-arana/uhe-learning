/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  Where,
  repository,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {createHash} from 'crypto';
import {deleteImage} from '../common';
import {generateImageFileFromBase64} from '../common/generator';
import { isAdmin, isStudent } from '../common/middleware';
import {StudentTransformer} from '../common/transformers';
import {Student} from '../models';
import {RegisterObjectClass} from '../models/user.model';
import {StudentRepository} from '../repositories';
import {UserRepository} from '../repositories/user.repository';
// import { HttpErrors } from '@loopback/rest';

export class StudentController {
  constructor(
    @repository(StudentRepository)
    public studentRepository: StudentRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  @post('/students')
  @response(200, {
    description: 'Student model instance',
    content: {'application/json': {schema: getModelSchemaRef(Student)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Student, {
            title: 'NewStudent',
            exclude: ['id'],
          }),
        },
      },
    })
    student: Omit<Student, 'id'>,
  ): Promise<Student> {
    return this.studentRepository.create(student);
  }

  @get('/students/count')
  @response(200, {
    description: 'Student model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Student) where?: Where<Student>): Promise<Count> {
    return this.studentRepository.count(where);
  }

  @get('/students')
  @response(200, {
    description: 'Array of Student model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Student, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.filter(Student) filter?: Filter<Student>,
  ): Promise<any> {
    await isAdmin(token, this.userRepository);
    const res = await StudentTransformer(
      await this.studentRepository.find({}),
      this.userRepository,
      this.studentRepository,
    );
    return res;
  }

  @patch('/students')
  @response(200, {
    description: 'Student PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Student, {partial: true}),
        },
      },
    })
    student: Student,
    @param.where(Student) where?: Where<Student>,
  ): Promise<Count> {
    return this.studentRepository.updateAll(student, where);
  }

  @get('/students/{id}')
  @response(200, {
    description: 'Student model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Student, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Student, {exclude: 'where'})
    filter?: FilterExcludingWhere<Student>,
  ): Promise<any> {
    const student = await this.studentRepository.findById(id, filter);
    const user = await this.userRepository.findById(student.user);
    return {
      id: student.id,
      user: user,
      createdAt: student.createdAt,
      updatedAt: student.updatedAt,
    };
  }

  @patch('/students/{id}')
  @response(204, {
    description: 'Student PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Student, {partial: true}),
        },
      },
    })
    student: Student,
  ): Promise<void> {
    await this.studentRepository.updateById(id, student);
  }

  @put('/students/{id}')
  @response(200, {
    description: 'Student PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody()
    student: {
      createdAt: string;
      user: RegisterObjectClass;
    },
  ): Promise<{message: string}> {
    const studentObj = await this.studentRepository.findById(id);
    if (
      student.user.profileImage &&
      student.user.profileImage.indexOf('base64') !== -1
    ) {
      student.user.profileImage = await generateImageFileFromBase64(
        'users',
        student.user.profileImage,
        `${student.user.firstName}_${student.user.lastName}_` +
          Math.floor(Math.random() * 22424),
      );
    }else{
      delete student.user.profileImage;
    }
    if (student.user.password) {
      const hashedPassword = createHash('sha256')
        .update(student.user.password)
        .digest('hex');
        student.user.password = hashedPassword;
    }
    await this.userRepository.updateById(studentObj.user, student.user);
    studentObj.updatedAt = Date();
    await this.updateById(id, studentObj);
    return {message: 'success'};
  }

  @del('/students/{id}')
  @response(204, {
    description: 'Student DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.studentRepository.deleteById(id);
  }

  @del('/student-account')
  @response(200, {})
  async deleteMyAccount(
    @param.header.string('Authorization') token: string,
  ): Promise<{message: string}> {
    const userId = await isStudent(token, this.userRepository);
    const user = await this.userRepository.findById(userId);
    try {
      await deleteImage('./public/' + user.profileImage ?? '');
    } catch (err) {
      console.log(err);
    }
    await this.userRepository.deleteById(userId);
    const student = await this.studentRepository.find({where: {user: userId}});
    await this.studentRepository.delete(student[0]);
    return {
      message: 'success',
    };
  }


}

