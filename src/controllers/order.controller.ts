import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response
} from '@loopback/rest';
import {isInstructor, isStudent} from '../common/middleware';
import {Order} from '../models';
import {OrderCreationClass, OrderNowCreationClass, OrderTestsNowCreationClass} from '../models/order.model';
import {CartItemRepository, GlobalStudentTestRepository, GlobalTestRepository, OrderRepository} from '../repositories';
import {UserRepository} from '../repositories/user.repository';

export class OrderController {
  constructor(
    @repository(OrderRepository)
    public orderRepository: OrderRepository,

    @repository(GlobalStudentTestRepository)
    public globalStudentTestRepository: GlobalStudentTestRepository,


    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,

    @repository(UserRepository)
    public userRepository: UserRepository,

    @repository(CartItemRepository)
    public cartItemRepository: CartItemRepository,
  ) { }

  @post('/orders')
  @response(201, {
    description: 'Order model instance',
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderCreationClass, {
            title: 'NewOrder',
          }),
        },
      },
    })
    order: OrderCreationClass,
  ): Promise<Order | undefined> {
    let userId;
    try {
      userId = await isStudent(token, this.userRepository);
    } catch (err) {
      userId = await isInstructor(token, this.userRepository);
    }
    const cartItems = await this.cartItemRepository.find({where: {user: userId, inCart: true}});
    order.cartItems = cartItems.map(el => el.id || 0);
    return this.orderRepository.checkout(order, userId ?? 0);
  }



  @post('/buy_now')
  @response(201, {
    description: 'Order model instance',
  })
  async buyNow(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderNowCreationClass, {
            title: 'NewOrder',
          }),
        },
      },
    })
    order: OrderNowCreationClass,
  ): Promise<Order | undefined> {
    const userId = await isStudent(token, this.userRepository);
    return this.orderRepository.buyNow(order, userId ?? 0);
  }


  @post('/buy_tests_now')
  @response(201, {
    description: 'Order model instance',
  })
  async buyTestsNow(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(OrderTestsNowCreationClass, {
            title: 'NewOrder',
          }),
        },
      },
    })
    order: OrderTestsNowCreationClass,
  ): Promise<Order | undefined> {
    const userId = await isStudent(token, this.userRepository);
    return this.orderRepository.buyTestsNow(order, userId ?? 0, this.globalTestRepository, this.globalStudentTestRepository);
  }


  @get('/orders/count')
  @response(200, {
    description: 'Order model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Order) where?: Where<Order>): Promise<Count> {
    return this.orderRepository.count(where);
  }

  @get('/orders')
  @response(200, {
    description: 'Array of Order model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Order, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(Order) filter?: Filter<Order>): Promise<Order[]> {
    return this.orderRepository.find(filter);
  }

  @patch('/orders')
  @response(200, {
    description: 'Order PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Order, {partial: true}),
        },
      },
    })
    order: Order,
    @param.where(Order) where?: Where<Order>,
  ): Promise<Count> {
    return this.orderRepository.updateAll(order, where);
  }

  @get('/orders/{id}')
  @response(200, {
    description: 'Order model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Order, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Order, {exclude: 'where'})
    filter?: FilterExcludingWhere<Order>,
  ): Promise<Order> {
    return this.orderRepository.findById(id, filter);
  }

  @patch('/orders/{id}')
  @response(204, {
    description: 'Order PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Order, {partial: true}),
        },
      },
    })
    order: Order,
  ): Promise<void> {
    await this.orderRepository.updateById(id, order);
  }

  @put('/orders/{id}')
  @response(204, {
    description: 'Order PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() order: Order,
  ): Promise<void> {
    await this.orderRepository.replaceById(id, order);
  }

  @del('/orders/{id}')
  @response(204, {
    description: 'Order DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.orderRepository.deleteById(id);
  }
}
