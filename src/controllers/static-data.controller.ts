import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {deleteImage, generateImageFileFromBase64} from '../common';
import {StaticData} from '../models';
import {StaticDataRepository} from '../repositories';

export class StaticDataController {
  constructor(
    @repository(StaticDataRepository)
    public staticDataRepository: StaticDataRepository,
  ) { }

  @post('/static-data')
  @response(200, {
    description: 'StaticData model instance',
    content: {'application/json': {schema: getModelSchemaRef(StaticData)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StaticData, {
            title: 'NewStaticData',
            exclude: ['id'],
          }),
        },
      },
    })
    staticData: StaticData,
  ): Promise<StaticData> {
    let staticDataObjs = await this.staticDataRepository.find({});
    if (staticDataObjs?.[0]) {
      return await this.replaceById(staticDataObjs[0].id ?? 0, staticData);
    } else {
      if (
        staticData.freeGuideFile && staticData.freeGuideFile.indexOf('base64') !== -1
      ) {
        staticData.freeGuideFile = await generateImageFileFromBase64('static-data', staticData.freeGuideFile, 'freeguide');
      }
      if (
        staticData.adminImage && staticData.adminImage.indexOf('base64') !== -1
      ) {
        staticData.adminImage = await generateImageFileFromBase64('static-data', staticData.adminImage, 'freeguide');
      }
      return this.staticDataRepository.create(staticData);
    }
  }

  @get('/static-data/count')
  @response(200, {
    description: 'StaticData model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(StaticData) where?: Where<StaticData>,
  ): Promise<Count> {
    return this.staticDataRepository.count(where);
  }

  @get('/static-data')
  @response(200, {
    description: 'Array of StaticData model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(StaticData, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(StaticData) filter?: Filter<StaticData>,
  ): Promise<StaticData | undefined | null> {
    let std = await this.staticDataRepository.findOne({where: {id: 1}});
    return std;
  }

  @patch('/static-data')
  @response(200, {
    description: 'StaticData PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StaticData, {partial: true}),
        },
      },
    })
    staticData: StaticData,
    @param.where(StaticData) where?: Where<StaticData>,
  ): Promise<Count> {
    return this.staticDataRepository.updateAll(staticData, where);
  }

  @get('/static-data/{id}')
  @response(200, {
    description: 'StaticData model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(StaticData, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(StaticData, {exclude: 'where'}) filter?: FilterExcludingWhere<StaticData>
  ): Promise<StaticData> {
    return this.staticDataRepository.findById(id, filter);
  }

  @patch('/static-data/{id}')
  @response(200, {
    description: 'StaticData PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(StaticData, {partial: true}),
        },
      },
    })
    staticData: StaticData,
  ): Promise<void> {
    await this.staticDataRepository.updateById(id, staticData);
  }

  @put('/static-data/{id}')
  @response(200, {
    description: 'StaticData PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() staticData: StaticData,
  ): Promise<StaticData> {
    let staticDataObj = await this.staticDataRepository.findById(id);
    if (
      staticData.freeGuideFile && staticData.freeGuideFile.indexOf('base64') !== -1
    ) {
      await deleteImage(staticDataObj.freeGuideFile);
      staticData.freeGuideFile = await generateImageFileFromBase64('static-data', staticData.freeGuideFile, 'freeguide');
    }
    if (
      staticData.adminImage && staticData.adminImage.indexOf('base64') !== -1
    ) {
      await deleteImage(staticDataObj.adminImage);
      staticData.adminImage = await generateImageFileFromBase64('static-data', staticData.adminImage, 'freeguide');
    }

    await this.staticDataRepository.replaceById(id, staticData);
    return staticData;
  }

  @del('/static-data/{id}')
  @response(200, {
    description: 'StaticData DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<{message: string}> {

    let staticDataObj = await this.staticDataRepository.findById(id);

    await deleteImage(staticDataObj.freeGuideFile);
    await deleteImage(staticDataObj.adminImage);

    await this.staticDataRepository.deleteById(id);
    return {
      "message": "success"
    }
  }
}
