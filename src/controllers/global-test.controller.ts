import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import { canAddCourse, getUser, isAdmin, isInstructor, isStudent, tryGetUserId } from '../common/middleware';
import { GlobalTestListTransformer, GlobalTestObjTransformer, GlobalTestTransformer } from '../common/transformers';
import { GlobalTest } from '../models';
import { GlobalTestCreationClass } from '../models/global-test.model';
import { GlobalStudentTestRepository, GlobalTestRepository, TestRepository, WishlistRepository } from '../repositories';
import { AdminRepository } from '../repositories/admin.repository';
import { AnswerRepository } from '../repositories/answer.repository';
import { ColumnAnswerRepository } from '../repositories/column-answer.repository';
import { CourseRepository } from '../repositories/course.repository';
import { InstructorRepository } from '../repositories/instructor.repository';
import { UserRepository } from '../repositories/user.repository';
import { deleteGlobalTest } from '../common/deleters';
import { CartItemRepository } from '../repositories/cart-item.repository';

export class GlobalTestController {
  constructor(
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
    @repository(AdminRepository)
    public adminRepository: AdminRepository,
    @repository(InstructorRepository)
    public instructorRepository: InstructorRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(GlobalStudentTestRepository)
    public globalStudentTestRepository: GlobalStudentTestRepository,
    @repository(AnswerRepository)
    public answerRepository: AnswerRepository,
    @repository(ColumnAnswerRepository)
    public columnAnswerRepository: ColumnAnswerRepository,
    @repository(TestRepository)
    public testRepository: TestRepository,
    @repository(CourseRepository)
    public courseRepository: CourseRepository,

    @repository(CartItemRepository)
    public cartItemRepository: CartItemRepository,
    @repository(WishlistRepository)
    public wishlistRepository: WishlistRepository,

  ) { }

  @post('/global-tests')
  @response(200, {
    description: 'GlobalTest model instance',
    content: { 'application/json': { schema: getModelSchemaRef(GlobalTest) } },
  })
  async create(
    @param.header.string("Authorization") token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GlobalTestCreationClass, {
            title: 'NewGlobalTest',
          }),
        },
      },
    })
    globalTest: GlobalTestCreationClass,
  ): Promise<GlobalTest> {
    const id = await canAddCourse(token, this.userRepository);
    if (globalTest.user) {
    } else {
      globalTest.user = id;
    }
    return this.globalTestRepository.create(globalTest);
  }

  @get('/global-tests/count')
  @response(200, {
    description: 'GlobalTest model count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async count(
    @param.where(GlobalTest) where?: Where<GlobalTest>,
  ): Promise<Count> {
    return this.globalTestRepository.count(where);
  }

  @get('/global-tests')
  @response(200, {
    description: 'Array of GlobalTest model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(GlobalTest, { includeRelations: true }),
        },
      },
    },
  })
  async find(
    @param.header.string("Authorization") token: string,
    @param.filter(GlobalTest) filter?: Filter<GlobalTest>,
  ): Promise<any> {
    const userId = await tryGetUserId(token, this.userRepository);
    return await GlobalTestListTransformer(
      userId,
      await this.globalTestRepository.find({ where: { course: 0 } }),
      this.globalStudentTestRepository, 
      this.cartItemRepository,
      this.userRepository,
      this.wishlistRepository,
      this.testRepository
    );
  }


  
  @get('/global-tests-details/{id}')
  @response(200, {
    description: 'Array of GlobalTest model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(GlobalTest, { includeRelations: true }),
        },
      },
    },
  })
  async findByIdDetails(
    @param.path.number("id") id: number,
    @param.header.string("Authorization") token: string
  ): Promise<any> {
    const userId = await tryGetUserId(token, this.userRepository);
    return await GlobalTestObjTransformer(
      userId,
      await this.globalTestRepository.findById(id),
      this.globalStudentTestRepository, 
      this.cartItemRepository,
      this.userRepository,
      this.wishlistRepository,
      this.testRepository
    );
  }

  @patch('/global-tests')
  @response(200, {
    description: 'GlobalTest PATCH success count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GlobalTest, { partial: true }),
        },
      },
    })
    globalTest: GlobalTest,
    @param.where(GlobalTest) where?: Where<GlobalTest>,
  ): Promise<Count> {
    return this.globalTestRepository.updateAll(globalTest, where);
  }

  @get('/global-tests/{id}')
  @response(200, {
    description: 'GlobalTest model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(GlobalTest, { includeRelations: true }),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.header.string("Authorization") token: string,
    @param.filter(GlobalTest, { exclude: 'where' }) filter?: FilterExcludingWhere<GlobalTest>
  ): Promise<GlobalTest | undefined> {
    const globalTest = await this.globalTestRepository.findById(id);
    try {
      const user = await isStudent(token, this.userRepository);
      const globalStudentTestInstance = await this.globalStudentTestRepository.findOne({ where: { user: user } })
      if (globalStudentTestInstance) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        let globalTestRes = await GlobalTestTransformer(globalTest, this.testRepository, this.answerRepository, this.columnAnswerRepository, this.courseRepository);
        //@ts-ignore
        globalTestRes['globalStudentTest'] = globalStudentTestInstance[0].id;

        //@ts-ignore
        return globalTestRes;
      } else {
        throw new HttpErrors[401];
      }
    } catch (err) {
      try {
        await isAdmin(token, this.userRepository);
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        //@ts-ignore
        return GlobalTestTransformer(globalTest, this.testRepository, this.answerRepository, this.columnAnswerRepository, this.courseRepository);
      } catch (err) {
        const user = await isInstructor(token, this.userRepository);
        if (globalTest.user == user) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          //@ts-ignore
          return GlobalTestTransformer(globalTest, this.testRepository, this.answerRepository, this.columnAnswerRepository, this.courseRepository);
        } else {
          throw new HttpErrors[401];
        }

      }
    }

  }

  @patch('/global-tests/{id}')
  @response(204, {
    description: 'GlobalTest PATCH success',
  })
  async updateById(

    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GlobalTest, { partial: true }),
        },
      },
    })
    globalTest: GlobalTest,
  ): Promise<void> {
    await this.globalTestRepository.updateById(id, globalTest);
  }

  @put('/global-tests/{id}')
  @response(200, {
    description: 'GlobalTest PUT success',
  })
  async replaceById(
    @param.header.string("Authorization") token: string,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(GlobalTestCreationClass, {
            title: 'NewVideo',
          }),
        },
      },
    })
    globalTest: GlobalTestCreationClass,
  ) {
    const userId = await canAddCourse(token, this.userRepository);
    if (globalTest.user) {
    } else {
      globalTest.user = userId;
    }
    await this.globalTestRepository.updateById(id, globalTest);
    return { message: 'success' }
  }

  @del('/global-tests/{id}')
  @response(200, {
    description: 'GlobalTest DELETE success',
  })
  async deleteById(@param.path.number('id') id: number, @param.header.string("Authorization") token: string) {
    const globalTest = await this.globalTestRepository.findById(id);
    const userId = await canAddCourse(token, this.userRepository);
    deleteGlobalTest(globalTest, this.globalTestRepository, this.testRepository);
    return { message: 'success' }
  }





  @get('/my-global-tests')
  @response(200, {
    description: 'Array of GlobalTest model instances'
  })
  async getMyGlobaltests(
    @param.header.string("Authorization") token: string,
    @param.query.integer('userId') userQueryId: number
  ): Promise<GlobalTest[]> {
    let userId = 0;
    if (userQueryId) {
      userId = userQueryId
    } else {
      userId = await isInstructor(token, this.userRepository) ?? 0;
    }
    const res = [];

    const globalTests = await this.globalTestRepository.find({ where: { user: userId } });

    for (const globalTest of globalTests) {
      if (globalTest.course !== 0) {
        const course = await this.courseRepository.find({ where: { id: globalTest.course } });
        if (course?.[0]) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          //@ts-ignore
          globalTest.course = {
            id: course[0].id,
            name: course[0].name,
            courseImage: course[0].courseImage,
            createdAt: course[0].createdAt,
          };
        }
      }
      res.push(globalTest);
    }
    return res;
  }



}
