import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
  HttpErrors,
} from '@loopback/rest';
import { UserUseCaseStudy, UserUseCaseStudyCreationClass } from '../models';
import { UseCaseStudyQuestionAnswerRepository, UseCaseStudyQuestionRepository, UseCaseStudyRepository, UserRepository, UserUseCaseAnswerRepository, UserUseCaseStudyRepository } from '../repositories';
import { getUser } from '../common/middleware';

export class UserUseCaseStudyController {
  constructor(
    @repository(UserUseCaseStudyRepository)
    public userUseCaseStudyRepository: UserUseCaseStudyRepository,
    @repository(UserUseCaseAnswerRepository)
    public userUseCaseAnswerRepository: UserUseCaseAnswerRepository,
    @repository(UseCaseStudyRepository)
    public useCaseStudyRepository: UseCaseStudyRepository,
    @repository(UseCaseStudyQuestionRepository)
    public useCaseStudyQuestionRepository: UseCaseStudyQuestionRepository,
    @repository(UseCaseStudyQuestionAnswerRepository)
    public useCaseStudyQuestionAnswerRepository: UseCaseStudyQuestionAnswerRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  @post('/user-use-case-studies')
  @response(200, {
    description: 'UserUseCaseStudy model instance',
    content: { 'application/json': { schema: getModelSchemaRef(UserUseCaseStudy) } },
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {},
      },
    })
    userUseCaseStudy: UserUseCaseStudyCreationClass,
  ) {
    const user = await getUser(token, this.userRepository);
    
    await this.userUseCaseStudyRepository.createUseCase(user, userUseCaseStudy,this.userUseCaseAnswerRepository);
    return { message: 'success' };
  }

  @get('/user-use-case-studies/count')
  @response(200, {
    description: 'UserUseCaseStudy model count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async count(
    @param.where(UserUseCaseStudy) where?: Where<UserUseCaseStudy>,
  ): Promise<Count> {
    return this.userUseCaseStudyRepository.count(where);
  }

  @get('/user-use-case-studies')
  @response(200, {
    description: 'Array of UserUseCaseStudy model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(UserUseCaseStudy, { includeRelations: true }),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.filter(UserUseCaseStudy) filter?: Filter<UserUseCaseStudy>,
  ): Promise<any[]> {
    const user = await getUser(token, this.userRepository);
    const res: any[] = [];
    const userUseCases = await this.userUseCaseStudyRepository.find({where: {user: user}});
    for (const userUseCase of userUseCases) {
      const useCase = await this.useCaseStudyRepository.findById(userUseCase.useCaseStudy);
      res.push({
        id: userUseCase.id,
        title: useCase.name,
        createdAt: userUseCase.createdAt
      })
    }
    return res;
  }

  @patch('/user-use-case-studies')
  @response(200, {
    description: 'UserUseCaseStudy PATCH success count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserUseCaseStudy, { partial: true }),
        },
      },
    })
    userUseCaseStudy: UserUseCaseStudy,
    @param.where(UserUseCaseStudy) where?: Where<UserUseCaseStudy>,
  ): Promise<Count> {
    return this.userUseCaseStudyRepository.updateAll(userUseCaseStudy, where);
  }

  @get('/user-use-case-studies/{id}')
  @response(200, {
    description: 'UserUseCaseStudy model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(UserUseCaseStudy, { includeRelations: true }),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.header.string('Authorization') token: string,
    @param.filter(UserUseCaseStudy, { exclude: 'where' }) filter?: FilterExcludingWhere<UserUseCaseStudy>
  ): Promise<any> {
    const user = await getUser(token, this.userRepository);
    const userUseCase = await this.userUseCaseStudyRepository.findOne({where: {user : user, id : id}});
    if(!userUseCase){
      throw new HttpErrors['404']();
    }
    const useCase = await this.useCaseStudyRepository.findById(userUseCase.useCaseStudy);
    const res: any = {
      title: useCase.name,
      questions: []
    };
    const questions = await this.useCaseStudyQuestionRepository.find({where: {useCaseStudy: useCase.id}})
      for (const question of questions) {
        const questionRes:any = {
          question: question.question,
          createdAt: question.createdAt,
          updatedAt: question.updatedAt,
          answers: []
        }
        const answers = await this.useCaseStudyQuestionAnswerRepository.find({where: {useCaseStudyQuestion: question.id}});
        for (const answer of answers) {
          const userAnswer = await this.userUseCaseAnswerRepository.findOne({where: {
            userUseCaseStudy: userUseCase.id,
            useCaseStudyQuestion: question.id,
            useCaseStudyQuestionAnswer: answer.id
          }})
          let isByUser = false;
          if(userAnswer){
            isByUser = true;
          }
          questionRes.answers.push({
            value: answer.value,
            next: answer.next,
            createdAt: answer.createdAt,
            updatedAt: answer.updatedAt,
            userChosen: isByUser
          })
        }
        res.questions.push(questionRes);
      }
    return res;
  }

  @patch('/user-use-case-studies/{id}')
  @response(204, {
    description: 'UserUseCaseStudy PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserUseCaseStudy, { partial: true }),
        },
      },
    })
    userUseCaseStudy: UserUseCaseStudy,
  ): Promise<void> {
    await this.userUseCaseStudyRepository.updateById(id, userUseCaseStudy);
  }

  @put('/user-use-case-studies/{id}')
  @response(204, {
    description: 'UserUseCaseStudy PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() userUseCaseStudy: UserUseCaseStudy,
  ): Promise<void> {
    await this.userUseCaseStudyRepository.replaceById(id, userUseCaseStudy);
  }

  @del('/user-use-case-studies/{id}')
  @response(204, {
    description: 'UserUseCaseStudy DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userUseCaseStudyRepository.deleteById(id);
  }
}
