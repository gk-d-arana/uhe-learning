/* eslint-disable @typescript-eslint/no-unused-vars */
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {HttpErrors, get, getModelSchemaRef, param, put, response} from '@loopback/rest';
import {isStudent} from '../common/middleware';
import {Enrollment} from '../models';
import {EnrollmentRepository} from '../repositories';
import {CourseRepository} from '../repositories/course.repository';
import {StudentLessonRepository} from '../repositories/student-lesson.repository';
import {UserRepository} from '../repositories/user.repository';
import {EnrollmentResponse} from '../types/responses';

export class EnrollmentController {
  constructor(
    @repository(EnrollmentRepository)
    public enrollmentRepository: EnrollmentRepository,

    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(CourseRepository)
    public courseRepository: CourseRepository,

    @repository(StudentLessonRepository)
    public studentLessonRepository: StudentLessonRepository,
  ) { }

  @get('/enrollments')
  @response(200, {
    description: 'Array of Enrollment model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Enrollment, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.filter(Enrollment) filter?: Filter<Enrollment>,
  ): Promise<EnrollmentResponse[]> {
    const userId = await isStudent(token, this.userRepository);
    const res = this.enrollmentRepository.getMyEnrollments(
      userId,
      this.courseRepository,
    );
    return res;
  }

  @get('/enrollments/{id}')
  @response(200, {
    description: 'Enrollment model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Enrollment, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @param.filter(Enrollment, {exclude: 'where'})
    filter?: FilterExcludingWhere<Enrollment>,
  ) {
    const userId = await isStudent(token, this.userRepository);
    const enrollment = await this.enrollmentRepository.findById(id);
    const {course, enrolled} = await this.courseRepository.findViewCourseById(
      userId,
      enrollment.course,
      filter,
    );
    return course;
  }

  @put('/student-lessons/{id}')
  @response(200, {
    description: 'Course PUT success',
  })
  async replaceById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
  ): Promise<{message: 'success'}> {
    const userId = await isStudent(token, this.userRepository);
    const studentLesson = await this.studentLessonRepository.findOne({where: {lesson: id, user: userId}});
    if (studentLesson) {
      studentLesson.isWatched = true;
      await this.studentLessonRepository.updateById(studentLesson.id, studentLesson);
      return {
        message: 'success',
      };
    } else {
      throw new HttpErrors.NotFound;
    }

  }
}
