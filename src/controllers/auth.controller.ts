/* eslint-disable @typescript-eslint/return-await */
import {repository} from '@loopback/repository';
import {
  HttpErrors,
  get,
  getModelSchemaRef,
  param,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {createHash} from 'crypto';
import {generateImageFileFromBase64} from '../common/generator';
import {getUser, isAdmin, isInstructor, isStudent} from '../common/middleware';
import {RegisterObjectClass} from '../models/user.model';
import {AdminRepository} from '../repositories/admin.repository';
import {CourseRepository} from '../repositories/course.repository';
import {GlobalTestRepository} from '../repositories/global-test.repository';
import {InstructorRepository} from '../repositories/instructor.repository';
import {StudentRepository} from '../repositories/student.repository';
import {UserRepository} from '../repositories/user.repository';
import {RegisterObject, RoleChangeObject} from '../types/auth';
import {InstructorResponse} from '../types/responses';

export class AuthController {
  constructor(
    @repository(UserRepository) public userRepository: UserRepository,
    @repository(StudentRepository) public studentRepository: StudentRepository,
    @repository(CourseRepository) public courseRepository: CourseRepository,
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
    @repository(InstructorRepository)
    public instructorRepository: InstructorRepository,
    @repository(AdminRepository) public adminRepository: AdminRepository,
  ) { }

  //controller to login
  @post('/login')
  async login(@requestBody({}) credentials: {email: string; password: string}) {
    const userRes = await this.userRepository.checkAuth(
      credentials.email,
      credentials.password,
    );
    if (userRes?.user.role === 'student') {
      const student = await this.studentRepository.find({
        where: {user: userRes?.user.id},
      });
      return {
        id: student[0].id,
        user: userRes?.user,
        jwt: userRes?.jwt,
        createdAt: student[0].createdAt,
        updatedAt: student[0].updatedAt,
      };
    } else if (userRes?.user.role === 'instructor') {
      const instructor = await this.instructorRepository.find({
        where: {user: userRes?.user.id},
      });
      return {
        id: instructor[0].id,
        user: userRes?.user,
        jwt: userRes?.jwt,
        createdAt: instructor[0].createdAt,
        updatedAt: instructor[0].updatedAt,
      };
    }
    else if (userRes?.user.role === 'admin') {
      const admin = await this.adminRepository.find({
        where: {user: userRes?.user.id},
      });
      if (admin?.[0]) {
        return {
          id: admin[0].id,
          user: userRes?.user,
          jwt: userRes?.jwt,
          createdAt: admin[0].createdAt,
          updatedAt: admin[0].updatedAt,
        };
      }
    }
  }

  @get('/profile')
  async find(@param.header.string('Authorization') token: string) {
    try {
      const userId = await isStudent(token, this.userRepository);
      const user = await this.userRepository.findById(userId);
      const student = await this.studentRepository.find({
        where: {user: userId},
      });
      if (student?.[0]) {
        return {
          profile: {
            id: student[0].id,
            user: user,
            createdAt: student[0].createdAt,
            updatedAt: student[0].updatedAt,
          }

        };
      } else {
        throw new HttpErrors['403']();
      }
    } catch (err) {
      const userId = await isInstructor(token, this.userRepository);
      const user = await this.userRepository.findById(userId);
      const instructor = await this.instructorRepository.find({
        where: {user: userId},
      });
      if (
        instructor[0].isBlocked &&
        instructor[0].blockEndingLimit &&
        new Date() >= new Date(instructor[0].blockEndingLimit)
      ) {
        instructor[0].isBlocked = false;
        await this.instructorRepository.updateById(
          instructor[0].id,
          instructor[0],
        );
      }
      if (instructor?.[0]) {
        return await this.findById(instructor[0]?.id || 0)
        // return {
        //   id: instructor[0].id,
        //   user: user,
        //   bio: instructor[0].bio,
        //   isApproved: instructor[0].isApproved,
        //   isReviewed: instructor[0].isReviewed,
        //   isBlocked: instructor[0].isBlocked,
        //   blockMessage: instructor[0].blockMessage,
        //   blockEndingLimit: instructor[0].blockEndinigLmit,
        //   education: instructor[0].education,
        //   rating: instructor[0].rating,
        //   coursesEnrollment: instructor[0].coursesEnrollment,
        //   createdAt: instructor[0].createdAt,
        //   updatedAt: instructor[0].updatedAt,
        // };
      } else {
        throw new HttpErrors['403']();
      }
    }
  }

  @post('/approveInstructor/{id}')
  async approveInstructor(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
  ) {
    const userId = await isAdmin(token, this.userRepository);
    if (userId) {
      const instructor = await this.instructorRepository.findById(id);
      instructor.isApproved = !instructor.isApproved;
      instructor.isReviewed = !instructor.isReviewed;
      await this.instructorRepository.updateById(id, instructor);
      return {message: 'success'};
    }
  }

  //controller to register
  @post('/register')
  async register(
    @requestBody({
      required: true,
      content: {
        'application/json': {
          schema: {
            'x-ts-type': RegisterObjectClass,
          },
        },
      },
    })
    userObj: RegisterObject,
    @param.header.string('Authorization') token?: string
  ) {
    let isAdminBool = false;
    try {
      if (token) {
        await isAdmin(token, this.userRepository);
        isAdminBool = true
      }
    } catch (err) {
      console.log(err);
    }

    const hashedPassword = createHash('sha256')
      .update(userObj.password)
      .digest('hex');
    const {jwt, secretCodeGen} = await this.userRepository.registerJWT(
      userObj.email,
      hashedPassword,
      userObj.role,
      true,
    );
    const currDate = new Date();
    const user = {
      firstName: userObj.firstName,
      lastName: userObj.lastName,
      email: userObj.email,
      password: hashedPassword,
      phoneNumber: userObj.phoneNumber ? userObj.phoneNumber : '',
      address: userObj.address ? userObj.address : '',
      age: userObj.age ? userObj.age : 18,
      role: userObj.role,
      createdAt: currDate.toISOString(),
      secretCode: secretCodeGen,
      profileImage: userObj.profileImage ? userObj.profileImage : '',
    };
    if (userObj.profileImage) {
      user.profileImage = await generateImageFileFromBase64(
        'users',
        userObj.profileImage,
        `${userObj.firstName}_${userObj.lastName}_` +
        Math.floor(Math.random() * 22424),
      );
    }
    const userCreated = await this.userRepository.create(user);
    if (user.role === 'student') {
      const student = {
        user: userCreated.id,
        createdAt: currDate.toISOString(),
        updatedAt: currDate.toISOString(),
      };
      const studentCreated = await this.studentRepository.create(student);
      return {
        id: studentCreated.id,
        user: userCreated,
        jwt: jwt,
        createdAt: studentCreated.createdAt,
        updatedAt: studentCreated.updatedAt,
      };
    } else if (user.role === 'instructor') {
      const instructor = {
        user: userCreated.id,
        bio: userObj.bio ? userObj.bio : '',
        education: userObj.education ? userObj.education : '',
        isApproved: isAdminBool && userObj.isApproved ? userObj.isApproved : false,
        isReviewed: isAdminBool && userObj.isReviewed ? userObj.isReviewed : false,
        isPrimary: isAdminBool && userObj.isPrimary ? userObj.isPrimary : false,
        isBlocked: isAdminBool && userObj.isBlocked ? userObj.isBlocked : false,
        blockEndingLimit: isAdminBool && userObj.blockEndingLimit ? userObj.blockEndingLimit : '',
        blockMessage: isAdminBool && userObj.blockMessage ? userObj.blockMessage : '',
        rating: isAdminBool && userObj.rating ? userObj.rating : 0,
        createdAt: currDate.toISOString(),
        updatedAt: currDate.toISOString(),
      };
      const instructorCreated = await this.instructorRepository.create(
        instructor,
      );
      return {
        id: instructorCreated.id,
        user: userCreated,
        isApproved: false,
        bio: instructor.bio,
        isReviewed: false,
        isBlocked: false,
        blockMessage: '',
        blockEndingLimit: '',
        education: instructor.education,
        rating: instructor.rating,
        jwt: jwt,
        createdAt: instructorCreated.createdAt,
        updatedAt: instructorCreated.updatedAt,
      };
    } else if (user.role === 'admin') {
      const admin = {
        user: userCreated.id,
      };
      const adminCreated = await this.adminRepository.create(admin);
      return {
        id: adminCreated.id,
        user: userCreated,
        jwt,
      };
    }
  }

  @put('/change-role')
  async changeRole(
    @param.header.string('Authorization') token: string,
    @requestBody({
      required: false,
      content: {
        'application/json': {
          schema: {
            'x-ts-type': RoleChangeObject,
          },
        },
      },
    })
    userObj: RoleChangeObject,
  ) {
    try {
      const userId = await isStudent(token, this.userRepository);
      const user = await this.userRepository.findById(userId);
      user.role = 'instructor';
      const {jwt, secretCodeGen} = await this.userRepository.registerJWT(
        user.email,
        user.password,
        'instructor',
        false,
      );
      user.secretCode = secretCodeGen;
      await this.userRepository.updateById(user.id, user);
      try {
        const instructor = await this.instructorRepository.find({
          where: {user: userId},
        });
        if (instructor?.[0]) {
          if (userObj?.bio) {
            instructor[0].bio = userObj?.bio;
          }
          if (userObj?.education) {
            instructor[0].education = userObj?.education;
          }
          instructor[0].updatedAt = new Date().toISOString();
          await this.instructorRepository.updateById(
            instructor[0].id,
            instructor[0],
          );

          return {
            id: instructor[0].id,
            user: user,
            bio: instructor[0].bio,
            isApproved: instructor[0].isApproved,
            education: instructor[0].education,
            rating: instructor[0].rating,
            jwt: jwt,
            createdAt: instructor[0].createdAt,
            updatedAt: instructor[0].updatedAt,
          };
        } else {
          const instructorObj = {
            user: user.id,
            bio: userObj?.bio ? userObj.bio : '',
            isApproved: instructor[0].isApproved,
            isReviewed: instructor[0].isReviewed,
            isBlocked: instructor[0].isBlocked,
            blockMessage: instructor[0].blockMessage,
            blockEndingLimit: instructor[0].blockEndinigLmit,
            education: userObj?.education ? userObj.education : '',
            rating: 0,
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString(),
          };
          const instructorCreated = await this.instructorRepository.create(
            instructorObj,
          );
          return {
            id: instructorCreated.id,
            user: user,
            bio: instructorObj.bio,
            education: instructorObj.education,
            rating: instructorObj.rating,
            isApproved: instructor[0].isApproved,
            isReviewed: instructor[0].isReviewed,
            isBlocked: instructor[0].isBlocked,
            blockMessage: instructor[0].blockMessage,
            blockEndingLimit: instructor[0].blockEndinigLmit,
            jwt: jwt,
            createdAt: instructorCreated.createdAt,
            updatedAt: instructorCreated.updatedAt,
          };
        }
      } catch (_err) {
        const instructorObj = {
          user: user.id,
          bio: userObj?.bio ? userObj?.bio : '',
          education: userObj?.education ? userObj?.education : '',
          rating: 0,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
        };
        const instructorCreated = await this.instructorRepository.create(
          instructorObj,
        );
        return {
          id: instructorCreated.id,
          user: user,
          isApproved: instructorCreated.isApproved,
          isReviewed: instructorCreated.isReviewed,
          isBlocked: instructorCreated.isBlocked,
          blockMessage: instructorCreated.blockMessage,
          blockEndingLimit: instructorCreated.blockEndinigLmit,
          bio: instructorObj.bio,
          education: instructorObj.education,
          rating: instructorObj.rating,
          jwt: jwt,
          createdAt: instructorCreated.createdAt,
          updatedAt: instructorCreated.updatedAt,
        };
      }
    } catch (err) {
      const userId = await isInstructor(token, this.userRepository);
      const user = await this.userRepository.findById(userId);
      user.role = 'student';
      const {jwt, secretCodeGen} = await this.userRepository.registerJWT(
        user.email,
        user.password,
        'student',
        false,
      );
      user.secretCode = secretCodeGen;
      await this.userRepository.updateById(user.id, user);
      try {
        const student = await this.studentRepository.find({
          where: {user: userId},
        });
        if (student?.[0]) {
          student[0].updatedAt = new Date().toISOString();
          await this.studentRepository.updateById(student[0].id, student[0]);
          return {
            id: student[0].id,
            user: user,
            jwt: jwt,
            createdAt: student[0].createdAt,
            updatedAt: student[0].updatedAt,
          };
        } else {
          const studentObj = {
            user: user.id,
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString(),
          };
          const studentCreated = await this.studentRepository.create(
            studentObj,
          );
          return {
            id: studentCreated.id,
            user: user,
            jwt: jwt,
            createdAt: studentCreated.createdAt,
            updatedAt: studentCreated.updatedAt,
          };
        }
      } catch (error) {
        const studentObj = {
          user: user.id,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
        };
        const studentCreated = await this.studentRepository.create(studentObj);
        return {
          id: studentCreated.id,
          user: user,
          jwt: jwt,
          createdAt: studentCreated.createdAt,
          updatedAt: studentCreated.updatedAt,
        };
      }
    }
  }

  @get('/instructor/{id}')
  @response(200, {
    description: 'LiveStream model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(InstructorResponse, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
  ): Promise<InstructorResponse> {
    const instructor = await this.instructorRepository.findById(id);
    if (
      instructor.isBlocked &&
      instructor.blockEndingLimit &&
      new Date() >= new Date(instructor.blockEndingLimit)
    ) {
      instructor.isBlocked = false;
      await this.instructorRepository.updateById(instructor.id, instructor);
    }
    const user = await this.userRepository.findById(instructor.user);
    const courses = await this.courseRepository.find({
      where: {instructor: instructor.id},
    });
    const globalTests = await this.globalTestRepository.find({
      where: {user: user.id, course: 0},
    });
    const resProfile = {
      ...instructor,
      user: user,
    };
    return {
      profile: resProfile,
      courses: courses.map(el => {
        return {
          id: el.id,
          name: el.name,
          courseImage: el.courseImage,
          createdAt: el.createdAt,
          webDescription: el.webDescription,
          mobileDescription: el.mobileDescription,
          updatedAt: el.updatedAt,
        };
      }),
      tests: globalTests.map(el => {
        return {
          id: el.id,
          name: el.name,
          createdAt: el.createdAt,
          price: el.price,
        };
      }),
    };
  }



  //update my student profile
  @put('/student/my-account')
  @response(200, {
    description: 'PUT success',
  })
  async updateStudentProfile(
    @param.header.string("Authorization") token: string,
    @requestBody()
    student: RegisterObjectClass
  ): Promise<{message: string}> {
    const user = await isStudent(token, this.userRepository);
    const studentMod = await this.studentRepository.find({where: {user: user}});
    let studentObj;
    if (studentMod?.[0]) {
      studentObj = studentMod[0]
    } else {
      throw new HttpErrors['404']();
    }
    if (
      student.profileImage &&
      student.profileImage.indexOf('media') === -1
    ) {
      student.profileImage = await generateImageFileFromBase64(
        'users',
        student.profileImage,
        `${student.firstName}_${student.lastName}_` +
        Math.floor(Math.random() * 22424),
      );
    }
    if (student.password) {
      const hashedPassword = createHash('sha256')
        .update(student.password)
        .digest('hex');
      student.password = hashedPassword;
    }
    await this.userRepository.updateById(studentObj.user, student);
    studentObj.updatedAt = Date();
    await this.studentRepository.updateById(studentObj.id ?? 0, studentObj);
    return {message: 'success'};
  }


  //update my instructor profile
  @put('/instructor/my-account')
  @response(200, {
    description: 'PUT success',
  })
  async updateInstrcutorProfile(
    @param.header.string("Authorization") token: string,
    @requestBody()
    instructor: RegisterObjectClass
  ): Promise<{message: string}> {
    const user = await isInstructor(token, this.userRepository);
    const instructorMod = await this.instructorRepository.find({where: {user: user}});
    let instructorObj;
    if (instructorMod?.[0]) {
      instructorObj = instructorMod[0]
    } else {
      throw new HttpErrors['404']();
    }
    if (
      instructor.profileImage &&
      instructor.profileImage.indexOf('media') === -1
    ) {
      instructor.profileImage = await generateImageFileFromBase64(
        'users',
        instructor.profileImage,
        `${instructor.firstName}_${instructor.lastName}_` +
        Math.floor(Math.random() * 22424),
      );
    }
    if (instructor.password) {
      const hashedPassword = createHash('sha256')
        .update(instructor.password)
        .digest('hex');
      instructor.password = hashedPassword;
    }
    await this.userRepository.updateById(instructorObj.user, instructor);
    instructorObj.updatedAt = Date();
    await this.instructorRepository.updateById(instructorObj.id ?? 0, instructorObj);
    return {message: 'success'};
  }

  //update my student profile
  @post('/review-instructor')
  @response(200, {
    description: 'POST success',
  })
  async reviewInstructor(
    @param.header.string("Authorization") token: string,
    @requestBody()
    reviewIns: {instructorId: number; isApproved: boolean, isPrimary: boolean}
  ): Promise<{message: string}> {
    await isAdmin(token, this.userRepository);
    const instrcutor = await this.instructorRepository.findById(reviewIns.instructorId);
    instrcutor.isApproved = reviewIns.isApproved;
    instrcutor.isPrimary = reviewIns.isPrimary;
    instrcutor.isReviewed = true;
    await this.instructorRepository.updateById(instrcutor.id, instrcutor);
    return {message: 'success'};
  }

  @get('/profile-data')
  @response(200, {
    description: 'POST success',
  })
  async getProfileData(
    @param.header.string("Authorization") token: string,
  ) {
    const userId = await getUser(token, this.userRepository);
    if (userId) {
      return await this.userRepository.findById(userId);
    } else {
      throw new HttpErrors['404']();
    }
  }
}
