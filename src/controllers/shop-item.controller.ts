import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param, patch, post, put, requestBody,
  response
} from '@loopback/rest';
import {deleteImage} from '../common';
import {generateImageFileFromBase64} from '../common/generator';
import {canAddCourse, isAdmin, isInstructor, isStudent} from '../common/middleware';
import {getShopItemResponse} from '../common/transformers';
import {ShopItem} from '../models/shop-item.model';
import {OrderRepository, ShopItemEnrollmentRepository, ShopItemRepository} from '../repositories';
import {CartItemRepository} from '../repositories/cart-item.repository';
import {UserRepository} from '../repositories/user.repository';
import {WishlistRepository} from '../repositories/wishlist.repository';
import {ShopItemResponse} from '../types/responses';

export class ShopItemController {
  constructor(
    @repository(ShopItemRepository)
    public shopItemRepository: ShopItemRepository,
    @repository(ShopItemEnrollmentRepository)
    public shopItemEnrollmentRepository: ShopItemEnrollmentRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(CartItemRepository)
    public cartItemRepository: CartItemRepository,
    @repository(WishlistRepository)
    public wishlistRepository: WishlistRepository,
    @repository(OrderRepository)
    public orderRepository: OrderRepository,
  ) { }

  @post('/shop-items')
  @response(200, {
    description: 'ShopItem model instance',
    content: {'application/json': {schema: getModelSchemaRef(ShopItem)}},
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShopItem, {
            title: 'NewShopItem',
            exclude: ['id'],
          }),
        },
      },
    })
    shopItem: Omit<ShopItem, 'id'>,
  ): Promise<ShopItem> {
    await isAdmin(token, this.userRepository);
    shopItem.shopItemImage = await generateImageFileFromBase64('shop-items', shopItem.shopItemImage, shopItem.name);
    shopItem.file = await generateImageFileFromBase64('shop-items', shopItem.shopItemImage, shopItem.name + 'file');
    return this.shopItemRepository.create(shopItem);
  }

  @get('/shop-items/count')
  @response(200, {
    description: 'ShopItem model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(ShopItem) where?: Where<ShopItem>): Promise<Count> {
    return this.shopItemRepository.count(where);
  }

  @get('/shop-items')
  @response(200, {
    description: 'Array of ShopItem model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ShopItem, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.filter(ShopItem) filter?: Filter<ShopItem>,
  ): Promise<ShopItemResponse[]> {
    const shopItems = await this.shopItemRepository.find(filter);
    const res = [];
    for (const shopItem of shopItems) {
      try {
        res.push(await this.findById(token, shopItem.id ?? 0));
      } catch (err) {
        //
      }
    }
    return res;
  }

  @patch('/shop-items')
  @response(200, {
    description: 'ShopItem PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShopItem, {partial: true}),
        },
      },
    })
    shopItem: ShopItem,
    @param.where(ShopItem) where?: Where<ShopItem>,
  ): Promise<Count> {
    return this.shopItemRepository.updateAll(shopItem, where);
  }

  @get('/shop-items/{id}')
  @response(200, {
    description: 'ShopItem model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ShopItem, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
  ): Promise<ShopItemResponse> {
    const shopItem = await this.shopItemRepository.findById(id);
    let userId;
    try {
      userId = await isStudent(token, this.userRepository);
    } catch (err) {
      try {
        userId = await canAddCourse(token, this.userRepository);
      } catch (err) {
        //
      }
    }
    return await getShopItemResponse(shopItem, this.cartItemRepository, this.wishlistRepository, this.shopItemEnrollmentRepository, userId);
  }

  @patch('/shop-items/{id}')
  @response(204, {
    description: 'ShopItem PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ShopItem, {partial: true}),
        },
      },
    })
    shopItem: ShopItem,
  ): Promise<void> {
    await this.shopItemRepository.updateById(id, shopItem);
  }

  @put('/shop-items/{id}')
  @response(204, {
    description: 'ShopItem PUT success',
  })
  async replaceById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @requestBody() shopItem: ShopItem,
  ): Promise<void> {
    await isAdmin(token, this.userRepository);
    const shopItemObj = await this.shopItemRepository.findById(id);
    if (
      shopItem.file &&
      shopItem.file.indexOf('base64') !== -1
    ) {
      deleteImage(shopItemObj.file);
      shopItem.file = await generateImageFileFromBase64('shop-items', shopItem.shopItemImage, shopItem.name + ' file');
    }

    if (
      shopItem.file &&
      shopItem.file.indexOf('base64') !== -1
    ) {
      deleteImage(shopItemObj.shopItemImage);
      shopItem.shopItemImage = await generateImageFileFromBase64('shop-items', shopItem.shopItemImage, shopItem.name + ' file');
    }
    await this.shopItemRepository.replaceById(id, shopItem);
  }

  @del('/shop-items/{id}')
  @response(204, {
    description: 'ShopItem DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.shopItemRepository.deleteById(id);
  }

  @get('my-shop-items')
  async getMyItems(
    @param.header.string('Authorization') token: string
  ) {
    let userId;
    try {
      userId = await isStudent(token, this.userRepository);
    } catch (err) {
      userId = await isInstructor(token, this.userRepository);
    }
    let res = [];
    let shopItemEnrollments = await this.shopItemEnrollmentRepository.find({where: {user: userId}});
    for (const shopItemEnrollment of shopItemEnrollments) {
      let shopItem = await this.shopItemRepository.find({where: {id: shopItemEnrollment.shopItem}});
      if (shopItem?.[0]) {
        res.push({
          id: shopItem[0].id,
          file: shopItem[0].file ? Buffer.from(shopItem[0].file).toString('base64') : '',
          shopItemImage: shopItem[0].shopItemImage,
          name: shopItem[0].name,
          bought_at: shopItemEnrollment.createdAt || new Date().toISOString()
        })
      }
    }
    return res;
  }

}
