/* eslint-disable @typescript-eslint/no-unused-vars */
import {Filter, FilterExcludingWhere, repository} from '@loopback/repository';
import {get, getModelSchemaRef, HttpErrors, param, put, response} from '@loopback/rest';
import {isStudent} from '../common/middleware';
import {Enrollment} from '../models';
import {GlobalTestRepository} from '../repositories';
import {GlobalTestEnrollmentRepository} from '../repositories/global-test-enrollment.repository';
import {UserRepository} from '../repositories/user.repository';
import {GlobalTestEnrollmentResponse} from '../types/responses';

export class GlobalTestEnrollmentController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
    @repository(GlobalTestEnrollmentRepository)
    public globalTestEnrollmentRepository: GlobalTestEnrollmentRepository,
  ) { }

  @get('/global-test-enrollments')
  @response(200, {
    description: 'Array of Enrollment model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Enrollment, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.filter(Enrollment) filter?: Filter<Enrollment>,
  ): Promise<GlobalTestEnrollmentResponse[]> {
    const userId = await isStudent(token, this.userRepository);
    const res = this.globalTestEnrollmentRepository.getMyGlobalTestEnrollments(userId, this.globalTestRepository);
    return res;
  }
}
