import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Value} from '../models';
import {ValueRepository} from '../repositories';

export class ValueController {
  constructor(
    @repository(ValueRepository)
    public valueRepository : ValueRepository,
  ) {}

  @post('/values')
  @response(200, {
    description: 'Value model instance',
    content: {'application/json': {schema: getModelSchemaRef(Value)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Value, {
            title: 'NewValue',
            exclude: ['id'],
          }),
        },
      },
    })
    value: Omit<Value, 'id'>,
  ): Promise<Value> {
    return this.valueRepository.create(value);
  }

  @get('/values/count')
  @response(200, {
    description: 'Value model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Value) where?: Where<Value>,
  ): Promise<Count> {
    return this.valueRepository.count(where);
  }

  @get('/values')
  @response(200, {
    description: 'Array of Value model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Value, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Value) filter?: Filter<Value>,
  ): Promise<Value[]> {
    return this.valueRepository.find(filter);
  }

  @patch('/values')
  @response(200, {
    description: 'Value PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Value, {partial: true}),
        },
      },
    })
    value: Value,
    @param.where(Value) where?: Where<Value>,
  ): Promise<Count> {
    return this.valueRepository.updateAll(value, where);
  }

  @get('/values/{id}')
  @response(200, {
    description: 'Value model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Value, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Value, {exclude: 'where'}) filter?: FilterExcludingWhere<Value>
  ): Promise<Value> {
    return this.valueRepository.findById(id, filter);
  }

  @patch('/values/{id}')
  @response(204, {
    description: 'Value PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Value, {partial: true}),
        },
      },
    })
    value: Value,
  ): Promise<void> {
    await this.valueRepository.updateById(id, value);
  }

  @put('/values/{id}')
  @response(200, {
    description: 'Value PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() value: Value,
  ): Promise<{message:string}> {
    await this.valueRepository.replaceById(id, value);
    return {
      "message" : "success"
    }
  }

  @del('/values/{id}')
  @response(200, {
    description: 'Value DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<{message:string}> {
    await this.valueRepository.deleteById(id);
    return {
      "message" : "success"
    }
  }
}
