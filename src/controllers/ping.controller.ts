/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {CourseRepository} from '../repositories/course.repository';
import {get,  response} from '@loopback/rest';
import {repository} from '@loopback/repository';
// import {isAdmin} from '../common/middleware';
import {UserRepository} from '../repositories/user.repository';
import {InstructorRepository} from '../repositories/instructor.repository';
import {StudentRepository} from '../repositories/student.repository';
import {CategoryRepository} from '../repositories/category.repository';
import {LiveStreamRepository} from '../repositories/live-stream.repository';
import {ShopItemRepository} from '../repositories/shop-item.repository';
import {BlogRepository} from '../repositories/blog.repository';
import {VirtualClassroomRepository} from '../repositories/virtual-classroom.repository';
import {TransferPaymentRepository} from '../repositories/transfer-payment.repository';
import {GlobalTestRepository} from '../repositories/global-test.repository';

export class PingController {
  constructor(
    @repository(CourseRepository)
    public courseRepository: CourseRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(InstructorRepository)
    public instructorRepository: InstructorRepository,
    @repository(StudentRepository)
    public studentRepository: StudentRepository,
    @repository(CategoryRepository)
    public categoryRepository: CategoryRepository,
    @repository(LiveStreamRepository)
    public liveStreamRepository: LiveStreamRepository,
    @repository(ShopItemRepository)
    public shopItemRepository: ShopItemRepository,
    @repository(BlogRepository)
    public blogRepository: BlogRepository,
    @repository(VirtualClassroomRepository)
    public virtualClassroomRepository: VirtualClassroomRepository,
    @repository(TransferPaymentRepository)
    public transferPaymentRepository: TransferPaymentRepository,
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
  ) {}

  @get('/dashboard-overview')
  @response(200, {
    description: 'Array of Course model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
        },
      },
    },
  })
  async find(
    // @param.header.string('Authorization') token: string,
  ): Promise<any> {
    // await isAdmin(token, this.userRepository);
    return [
      {
        title: 'Students',
        title_ar: 'الطلاب',
        value: (await this.studentRepository.count()).count,
        link: '/manage/students',
        add_link: '/add/add-student',
      },
      {
        title: 'Instructors',
        title_ar: 'الاساتذة',
        value: (await this.instructorRepository.count()).count,
        link: '/manage/instructors',
        add_link: '/add/add-instructor',
      },
      {
        title: 'Categories',
        title_ar: 'التصنيفات',
        value: (await this.categoryRepository.count()).count,
        link: '/manage/categories',
        add_link: '/add/add-category',
      },
      {
        title: 'Live Stream',
        title_ar: 'البثوت المباشرة',
        value: (await this.liveStreamRepository.count()).count,
        link: '/manage/live-streams',
        add_link: '/add/add-live-streams',
      },
      {
        title: 'Courses',
        title_ar: 'الكورسات',
        value: (await this.courseRepository.count()).count,
        link: '/manage/courses',
        add_link: '/add/add-course',
      },
      {
        title: 'Shop Items',
        title_ar: 'المتجر',
        value: (await this.shopItemRepository.count()).count,
        link: '/manage/shop-items',
        add_link: '/add/add-shop-item',
      },
      {
        title: 'Blogs',
        title_ar: 'المقالات',
        value: (await this.blogRepository.count()).count,
        link: '/manage/blogs',
        add_link: '/add/add-blog',
      },
      {
        title: 'Virtual Classrooms',
        title_ar: 'القاعات الافتراضية',
        value: (await this.virtualClassroomRepository.count()).count,
        link: '/manage/virtual-classrooms',
        add_link: '/add/add-virtual-classroom',
      },
      {
        title: 'Transactions',
        title_ar: 'الحوالات المالية',
        value: (await this.transferPaymentRepository.count()).count,
        link: '/transactions',
        add_link: '/add/add-transaction',
      },
      {
        title: 'Tests',
        title_ar: 'الاختبارات',
        value: (await this.globalTestRepository.find({where: {course: 0}}))
        .length,
        link: '/manage/global-tests',
        add_link: '/add/add-test',
      },
    ];
  }
}
