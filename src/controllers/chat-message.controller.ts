import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {isInstructor, isStudent} from '../common/middleware';
import {ChatMessage} from '../models';
import {ChatGroupRepository, ChatMessagePollAnswerRepository, ChatMessagePollRepository, ChatMessagePollStudentAnswerRepository, ChatMessageRepository, UserRepository} from '../repositories';
import {ChatMessageResponse} from '../types/responses';

export class ChatMessageController {
  constructor(
    @repository(ChatMessageRepository)
    public chatMessageRepository: ChatMessageRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(ChatGroupRepository)
    public chatGroupRepository: ChatGroupRepository,

    @repository(ChatMessagePollRepository)
    public chatMessagePollRepository: ChatMessagePollRepository,

    @repository(ChatMessagePollAnswerRepository)
    public chatMessagePollAnswerRepository: ChatMessagePollAnswerRepository,

    @repository(ChatMessagePollStudentAnswerRepository)
    public chatMessagePollStudentAnswerRepository: ChatMessagePollStudentAnswerRepository,

  ) { }

  @post('/chat-messages')
  @response(200, {
    description: 'ChatMessage model instance',
    content: {'application/json': {schema: getModelSchemaRef(ChatMessage)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ChatMessage, {
            title: 'NewChatMessage',
            exclude: ['id'],
          }),
        },
      },
    })
    chatMessage: Omit<ChatMessage, 'id'>,
  ): Promise<ChatMessage> {
    return this.chatMessageRepository.create(chatMessage);
  }

  @get('/chat-messages/count')
  @response(200, {
    description: 'ChatMessage model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ChatMessage) where?: Where<ChatMessage>,
  ): Promise<Count> {
    return this.chatMessageRepository.count(where);
  }

  @get('/chat-messages')
  @response(200, {
    description: 'Array of ChatMessage model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ChatMessage, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.query.number('chatGroup') chatGroup: number,
    @param.filter(ChatMessage) filter?: Filter<ChatMessage>,
  ): Promise<ChatMessageResponse[]> {
    const res: ChatMessageResponse[] = [];
    try {
      const user = await isStudent(token, this.userRepository);
      const chatGroupObj = await this.chatGroupRepository.execute(`SELECT * FROM ChatGroup WHERE (id = ${chatGroup}) AND (users LIKE '%,${user},%') OR (users LIKE '[${user},%') OR (users LIKE '%,${user}]')`);
      if (Array.isArray(chatGroupObj)) {
        if (chatGroupObj.length > 0) {
          const chatMessages = await this.chatMessageRepository.find({where: {chatGroup: chatGroup}});
          for (const chatMessage of chatMessages) {
            const msgUser = await this.userRepository.findOne({where: {id: chatMessage.user}});
            switch (chatMessage.type) {
              case 'message':
                res.push({
                  id: chatMessage.id,
                  createdAt: chatMessage.createdAt,
                  value: chatMessage.value,
                  type: 'message',
                  user: {
                    firstName: msgUser?.firstName,
                    lastName: msgUser?.lastName,
                    id: msgUser?.id,
                    profileImage: msgUser?.profileImage,
                    role: msgUser?.role
                  }
                })
                break;
              case 'poll':
                const chatMessagePoll = await this.chatMessagePollRepository.findOne({where: {chatMessage: chatMessage.id}});
                const chatMessagePollAnswers = await this.chatMessagePollAnswerRepository.find({where: {chatMessagePoll: chatMessagePoll?.id}});
                const chatMessagePollStudentAnswers = await this.chatMessagePollStudentAnswerRepository.find({where: {chatMessagePoll: chatMessagePoll?.id}});
                const res_obj = {
                  id: chatMessage.id,
                  createdAt: chatMessage.createdAt,
                  type: 'poll',
                  chatMessagePoll: {
                    id: chatMessagePoll?.id,
                    question: chatMessagePoll?.question,
                    answers: chatMessagePollAnswers.map(chatMessagePollAnswer => {
                      return {
                        id: chatMessagePollAnswer.id,
                        value: chatMessagePollAnswer.value,
                        isTrue: chatMessagePollAnswer.isTrue
                      }
                    }),
                    usersAnswers: await Promise.all(chatMessagePollStudentAnswers.map(async (chatMessagePollStudentAnswer) => {
                      const pollAnswerUser = await this.userRepository.findOne({where: {id: chatMessagePollStudentAnswer.user}});
                      return {
                        id: chatMessagePollStudentAnswer.id,
                        value: chatMessagePollAnswers.find(el => el.id == chatMessagePollStudentAnswer.id)?.value,
                        isTrue: chatMessagePollStudentAnswer.isTrue,
                        user: {
                          firstName: pollAnswerUser?.firstName,
                          lastName: pollAnswerUser?.lastName,
                          id: pollAnswerUser?.id,
                          profileImage: pollAnswerUser?.profileImage,
                          role: pollAnswerUser?.role
                        }
                      }
                    }))
                  },
                  user: {
                    firstName: msgUser?.firstName,
                    lastName: msgUser?.lastName,
                    id: msgUser?.id,
                    profileImage: msgUser?.profileImage,
                    role: msgUser?.role
                  }
                };
                res.push(res_obj);
                break;
              default:
                break;
            }
          }

        } else {
          throw new HttpErrors['401']();
        }
      }
    } catch (err) {
      const user = await isInstructor(token, this.userRepository);
      const chatGroupObj = await this.chatGroupRepository.findById(chatGroup);
      if (user == chatGroupObj.user) {
        const chatMessages = await this.chatMessageRepository.find({where: {chatGroup: chatGroup}});

        for (const chatMessage of chatMessages) {
          const msgUser = await this.userRepository.findOne({where: {id: chatMessage.user}});
          switch (chatMessage.type) {
            case 'message':
              res.push({
                id: chatMessage.id,
                createdAt: chatMessage.createdAt,
                value: chatMessage.value,
                type: 'message',
                user: {
                  firstName: msgUser?.firstName,
                  lastName: msgUser?.lastName,
                  id: msgUser?.id,
                  profileImage: msgUser?.profileImage,
                  role: msgUser?.role
                }
              })
              break;
            case 'poll':
              const chatMessagePoll = await this.chatMessagePollRepository.findOne({where: {chatMessage: chatMessage.id}});
              const chatMessagePollAnswers = await this.chatMessagePollAnswerRepository.find({where: {chatMessagePoll: chatMessagePoll?.id}});
              const chatMessagePollStudentAnswers = await this.chatMessagePollStudentAnswerRepository.find({where: {chatMessagePoll: chatMessagePoll?.id}});
              res.push({
                id: chatMessage.id,
                createdAt: chatMessage.createdAt,
                type: 'poll',
                chatMessagePoll: {
                  id: chatMessagePoll?.id,
                  question: chatMessagePoll?.question,
                  answers: chatMessagePollAnswers.map(chatMessagePollAnswer => {
                    return {
                      id: chatMessagePollAnswer.id,
                      value: chatMessagePollAnswer.value,
                      isTrue: chatMessagePollAnswer.isTrue
                    }
                  }),
                  usersAnswers: await Promise.all(chatMessagePollStudentAnswers.map(async (chatMessagePollStudentAnswer) => {
                    const pollAnswerUser = await this.userRepository.findOne({where: {id: chatMessagePollStudentAnswer.user}});
                    return {
                      id: chatMessagePollStudentAnswer.id,
                      value: chatMessagePollAnswers.find(el => el.id == chatMessagePollStudentAnswer.id)?.value,
                      isTrue: chatMessagePollStudentAnswer.isTrue,
                      user: {
                        firstName: pollAnswerUser?.firstName,
                        lastName: pollAnswerUser?.lastName,
                        id: pollAnswerUser?.id,
                        profileImage: pollAnswerUser?.profileImage,
                        role: pollAnswerUser?.role
                      }
                    }
                  }))
                },
                user: {
                  firstName: msgUser?.firstName,
                  lastName: msgUser?.lastName,
                  id: msgUser?.id,
                  profileImage: msgUser?.profileImage,
                  role: msgUser?.role
                }
              })
              break;

            default:
              break;
          }
        }
      } else {
        throw new HttpErrors['401']();
      }
    }
    return res;
  }

  @patch('/chat-messages')
  @response(200, {
    description: 'ChatMessage PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ChatMessage, {partial: true}),
        },
      },
    })
    chatMessage: ChatMessage,
    @param.where(ChatMessage) where?: Where<ChatMessage>,
  ): Promise<Count> {
    return this.chatMessageRepository.updateAll(chatMessage, where);
  }

  @get('/chat-messages/{id}')
  @response(200, {
    description: 'ChatMessage model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ChatMessage, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ChatMessage, {exclude: 'where'}) filter?: FilterExcludingWhere<ChatMessage>
  ): Promise<ChatMessage> {
    return this.chatMessageRepository.findById(id, filter);
  }

  @patch('/chat-messages/{id}')
  @response(204, {
    description: 'ChatMessage PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ChatMessage, {partial: true}),
        },
      },
    })
    chatMessage: ChatMessage,
  ): Promise<void> {
    await this.chatMessageRepository.updateById(id, chatMessage);
  }

  @put('/chat-messages/{id}')
  @response(204, {
    description: 'ChatMessage PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() chatMessage: ChatMessage,
  ): Promise<void> {
    await this.chatMessageRepository.replaceById(id, chatMessage);
  }

  @del('/chat-messages/{id}')
  @response(204, {
    description: 'ChatMessage DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.chatMessageRepository.deleteById(id);
  }
}
