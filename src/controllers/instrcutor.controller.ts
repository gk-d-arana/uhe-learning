// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-nocheck
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Instructor} from '../models';
import {InstructorRepository} from '../repositories';
import { isAdmin, canAddCourse, isInstructor } from '../common/middleware';
import {UserRepository} from '../repositories/user.repository';
import {InstructorProfile} from '../types/responses';
import {AdminRepository} from '../repositories/admin.repository';
import {HttpErrors} from '@loopback/rest';
import { RegisterObject } from '../types/auth';
import { deleteImage } from '../common/helpers';
import { generateImageFileFromBase64 } from '../common/generator';
import {createHash} from 'crypto';

export class InstrcutorController {
  constructor(
    @repository(InstructorRepository)
    public instructorRepository: InstructorRepository,
    @repository(AdminRepository)
    public adminRepository: AdminRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  @post('/instructors')
  @response(200, {
    description: 'Instructor model instance',
    content: {'application/json': {schema: getModelSchemaRef(Instructor)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Instructor, {
            title: 'NewInstructor',
          }),
        },
      },
    })
    instructor: Instructor,
  ): Promise<RegisterObject> {
    return this.instructorRepository.create(instructor);
  }

  @get('/instructors/count')
  @response(200, {
    description: 'Instructor model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Instructor) where?: Where<Instructor>,
  ): Promise<Count> {
    return this.instructorRepository.count(where);
  }

  @get('/instructors')
  @response(200, {
    description: 'Array of Instructor model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Instructor, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.query.string('show') showFilter: string,
    @param.query.string('name') name: string,
    @param.header.string('Authorization') token: string,
    @param.filter(Instructor) filter?: Filter<Instructor>,
  ): Promise<Instructor[]> {
    let res: Instructor[] = [];
    switch (showFilter) {
      case 'top':
        res = await this.instructorRepository.getTopInstrcutors();
        break;
      default:
        res = await this.instructorRepository.getInstrcutors();
    }
    if(name){
      res = await this.instructorRepository.fetchByName(name);
    }
    return res;
  }

  @patch('/instructors')
  @response(200, {
    description: 'Instructor PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Instructor, {partial: true}),
        },
      },
    })
    instructor: Instructor,
    @param.where(Instructor) where?: Where<Instructor>,
  ): Promise<Count> {
    return this.instructorRepository.updateAll(instructor, where);
  }

  @get('/instructors/{id}')
  @response(200, {
    description: 'Instructor model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Instructor, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Instructor, {exclude: 'where'})
    filter?: FilterExcludingWhere<Instructor>,
  ): Promise<Instructor> {
    return this.instructorRepository.findById(id, filter);
  }

  @post('/instructors/{id}/block')
  @response(200, {
    description: 'Instructor Block success',
  })
  async updateById(
    @param.header.string("Authorization") token: string,
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
        },
      },
    })
    data: {blockMessage: string, isBlocked: boolean, blockEndingLimit: string},
  ): Promise<{message: string}> {
    const check = await isAdmin(token, this.userRepository);
    if(check){
      const instrcutor = await this.instructorRepository.findById(id);
      instrcutor.isBlocked = data.isBlocked;
      instrcutor.blockMessage = data.blockMessage;
      instrcutor.blockEndingLimit = new Date(data.blockEndingLimit).toISOString();
      await this.instructorRepository.updateById(id, instrcutor)
      return {message: 'success'};
    }else{
      throw new HttpErrors['403'];
    }
  }

  @put('/instructors/{id}')
  @response(200, {
    description: 'Instructor PUT success',
  })
  async replaceById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @requestBody() instructor: InstructorProfile,
  ): Promise<{message: string}> {
    const userId = await canAddCourse(token, this.userRepository);
    const admin = await this.adminRepository.find({where: {user: userId}});
    const instructorRef = await this.instructorRepository.find({
      where: {user: userId},
    });
    if (
      admin?.[0] ||
      (instructorRef?.[0] && instructorRef[0].user === userId)
    ) {
      if (
        instructor.user.profileImage &&
        instructor.user.profileImage.indexOf('base64') !== -1
      ) {
        await deleteImage(instructor.user.profileImage);
        instructor.user.profileImage = await generateImageFileFromBase64(
          'users',
          instructor.user.profileImage,
          `${instructor.user.firstName}_${instructor.user.lastName}_` +
            Math.floor(Math.random() * 22424),
        );
      }else{
        delete instructor.user.profileImage;
      }
      if (instructor.user.password) {
        const hashedPassword = createHash('sha256')
          .update(instructor.user.password)
          .digest('hex');
          instructor.user.password = hashedPassword;
      }
      await this.userRepository.updateById(instructor.user.id, instructor.user);
      const instructorObj = {
        ...instructor,
        user: instructor.user.id,
      };
      await this.instructorRepository.replaceById(id, instructorObj); //
      return {message: 'success'};
    } else {
      throw new HttpErrors['401']();
    }
  }

  @del('/instructors/{id}')
  @response(204, {
    description: 'Instructor DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.instructorRepository.deleteById(id);
  }

    @del('/instructor-account')
  @response(200, {})
  async deleteMyAccount(
    @param.header.string("Authorization") token: string
  ): Promise<{message: string}> {
    const userId = await isInstructor(token, this.userRepository);
    const user = await this.userRepository.findById(userId);
    try{
      await deleteImage("./public/" + user.profileImage ?? '');
    }
    catch(err){
      console.log(err);
    }
    await this.userRepository.deleteById(userId);
    const student = await this.instructorRepository.find({where:{user:userId}});
    await this.instructorRepository.delete(student[0]);
    return {
      message: 'success',
    };
  }
}
