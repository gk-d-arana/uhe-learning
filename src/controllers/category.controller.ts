import {
  Filter,
  repository,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Category} from '../models';
import {CategoryRepository} from '../repositories';
import { isAdmin } from '../common/middleware';
import { UserRepository } from '../repositories/user.repository';

export class CategoryController {
  constructor(
    @repository(CategoryRepository)
    public categoryRepository : CategoryRepository,

    @repository(UserRepository) public userRepository : UserRepository
  ) {}

  @post('/categories')
  @response(200, {
    description: 'Category model instance',
    content: {'application/json': {schema: getModelSchemaRef(Category)}},
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Category, {
            title: 'NewCategory',
            exclude: ['id'],
          }),
        },
      },
    })
    category: Omit<Category, 'id'>,
  ): Promise<Category> {
    await isAdmin(token, this.userRepository);
    return this.categoryRepository.create(category);
  }



  @get('/categories')
  @response(200, {
    description: 'Array of Category model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Category, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.query.string('show') showFilter: string,
    @param.filter(Category) filter?: Filter<Category>,
  ): Promise<Category[]> {
    let res: Category[] = [];
    switch (showFilter) {
      case 'top':
        res = await this.categoryRepository.getTopCategories();
        break;

      default:
        res = await this.categoryRepository.find(filter);
    }
    return res;
  }

  @get('/categories/{id}')
  @response(200, {
    description: 'Array of Category model instances'
  })
  async findById(
    @param.path.number("id") id: number
  ): Promise<Category> {
    const res = await this.categoryRepository.findById(id);
    return res;
  }

  @put('/categories/{id}')
  @response(200, {
    description: 'Category PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @param.header.string('Authorization') token: string,
    @requestBody() category: Category,
  ): Promise<{message:string}> {
    await isAdmin(token, this.userRepository);
    await this.categoryRepository.replaceById(id, category);
    return {
      message: 'success'
    }
  }

  @del('/categories/{id}')
  @response(200, {
    description: 'Category DELETE success',
  })
  async deleteById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number): Promise<{message:string}> {
    await isAdmin(token, this.userRepository);
    await this.categoryRepository.deleteById(id);
    return {
      message: "success"
    }
  }
}
