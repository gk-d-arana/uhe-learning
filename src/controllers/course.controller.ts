/* eslint-disable @typescript-eslint/no-explicit-any */
import { Filter, FilterExcludingWhere, repository } from '@loopback/repository';
import {
  HttpErrors,
  del,
  get,
  getModelSchemaRef,
  param,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {
  canAddCourse,
  getUser,
  isAdmin,
  isInCart,
  isInWishlist,
  isInstructor,
  isStudent
} from '../common/middleware';
import { Course } from '../models';
import { CourseObjectClass } from '../models/course.model';
import { CategoryRepository, CourseRepository, InstructorRepository } from '../repositories';
import { AdminRepository } from '../repositories/admin.repository';
import { CartItemRepository } from '../repositories/cart-item.repository';
import { EnrollmentRepository } from '../repositories/enrollment.repository';
import { UserRepository } from '../repositories/user.repository';
import { WishlistRepository } from '../repositories/wishlist.repository';

export class CourseController {
  constructor(
    @repository(CourseRepository)
    public courseRepository: CourseRepository,
    @repository(CategoryRepository)
    public categoryRepository: CategoryRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(AdminRepository)
    public adminRepository: AdminRepository,
    @repository(InstructorRepository)
    public instructorRepository: InstructorRepository,
    @repository(CartItemRepository)
    public cartItemRepository: CartItemRepository,
    @repository(EnrollmentRepository)
    public enrollmentRepository: EnrollmentRepository,
    @repository(WishlistRepository)
    public wishlistRepository: WishlistRepository
  ) { }

  @post('/courses')
  @response(201, {
    description: 'Course model instance',
    content: { 'application/json': { schema: getModelSchemaRef(Course) } },
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CourseObjectClass, {
            title: 'NewCourse',
          }),
        },
      },
    })
    course: CourseObjectClass,
  ): Promise<Course> {
    try {
      await isAdmin(token, this.userRepository);
      const instructor = await this.instructorRepository.findById(course.instructor);
      if (instructor) {
        const courseObj = await this.courseRepository.create(course);
        for (let index = 0; index < course.categories.length; index++) {
          let category = await this.categoryRepository.findById(course.categories[index]);
          if (category && category.coursesCount) {
            category.coursesCount += 1;
            await this.categoryRepository.updateById(category.id ?? 0, category);
          }
        }
        return courseObj;
      } else {
        throw new HttpErrors['404']
      }
    }
    catch (err) {
      const userId = await isInstructor(token, this.userRepository);
      const instructor = await this.instructorRepository.find({ where: { user: userId } });
      if (instructor?.[0]) {
        course.instructor = instructor[0].id;
        const courseObj = await this.courseRepository.create(course);
        for (let index = 0; index < course.categories.length; index++) {
          let category = await this.categoryRepository.findById(course.categories[index]);
          if (category && category.coursesCount) {
            category.coursesCount += 1;
            await this.categoryRepository.updateById(category.id ?? 0, category);
          }
        }
        return courseObj;
      } else {
        throw new HttpErrors['403']
      }
    }

  }

  @get('/courses')
  @response(200, {
    description: 'Array of Course model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Course, { includeRelations: true }),
        },
      },
    },
  })
  async find(
    @param.query.string('show') showFilter: string | undefined,
    @param.query.string('value') valueFilter: string | undefined,
    @param.query.string('name') name: string | undefined,
    @param.query.string('sortPrice') sortPrice: string | undefined,
    @param.query.number('minPrice') minPrice: number | undefined,
    @param.query.number('maxPrice') maxPrice: number | undefined,
    @param.header.string('Authorization') token: string | undefined,
    @param.query.number('category') category?: string | undefined,
    @param.filter(Course) filter?: Filter<Course>,
  ): Promise<any[]> {
    let courses = [];
    if (showFilter == 'top') {
      courses = await this.courseRepository.getTopCourses();
    } else {
      let whereClause = '';
      let sortClause = '';

      if (category) {
        whereClause += `((categories LIKE '%,${category},%') OR (categories LIKE '[${category},%') OR (categories LIKE '%,${category}]')) AND `;
      }
      if (maxPrice) {
        whereClause += `price < ${maxPrice} AND `
      }
      if (minPrice) {
        whereClause += `price > ${minPrice} AND `
      }
      if (showFilter == 'uploadTime') {
        let whereTime = this.courseRepository.getCoursesQueryBasedOnTime(valueFilter ?? '');
        if (whereTime != '') {
          whereClause += `${whereTime} AND `
        }
      }
      if (whereClause == '') {
        whereClause = 'id>0 AND '
      }
      whereClause = whereClause.slice(0, whereClause.length - 5);

      if (sortPrice == 'htl') {
        sortClause += 'ORDER BY price DESC'
      }

      if (sortPrice == 'lth') {
        sortClause += 'ORDER BY price ASC'
      }


      let res = await this.courseRepository.execute(`SELECT * FROM Course WHERE ${whereClause} ${sortClause}`);
      if (Array.isArray(res)) {
        if (name) {
          courses = res.filter((el:Course) => el.name.indexOf(name) > -1 || el.mobileDescription.indexOf(name) > -1 || el.webDescription.indexOf(name) > -1)
        } else {
          courses = res;
        }
      }
    }
    if (token && token != undefined && token != 'undefined') {
      const user = await getUser(token, this.userRepository);
      const ress = []
      // eslint-disable-next-line @typescript-eslint/prefer-for-of
      for (let i = 0; i < courses.length; i++) {
        const course = courses[i];
        const wishlists = await this.wishlistRepository.find({ where: { user: user, item: course.id, type: 'course' } });
        let categories = [];
        if (typeof course.categories == 'string') {
          categories = JSON.parse(course.categories);
        } else {
          categories = course.categories;
        }
        delete course.categories;
        if (wishlists?.[0]) {
          ress.push({ ...course, inWishlist: true, categories: categories });
        } else {
          ress.push({ ...course, inWishlist: false, categories: categories });
        }
      }
      return ress;
    } else {
      return courses.map(el => {
        let categories = [];
        if (typeof el.categories == 'string') {
          categories = JSON.parse(el.categories);
        } else {
          categories = el.categories;
        }
        delete el.categories;
        return {
          ...el,
          categories: categories
        }
      });
    }

  }

  @get('/courses/{id}')
  @response(200, {
    description: 'Course model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Course, { includeRelations: true }),
      },
    },
  })
  async findById(
    @param.header.string('Authorization') token: string,
    @param.path.number('id') id: number,
    @param.filter(Course, { exclude: 'where' })
    filter?: FilterExcludingWhere<Course>,
  ): Promise<any> {
    let inCart = false;
    let inWishlist = false;
    let userId;
    try {
      userId = await isStudent(token, this.userRepository);
    } catch (err) {
      try {
        userId = await canAddCourse(token, this.userRepository)
      } catch (errr) {
        //
      }
    };
    const { course, enrolled, progress } = await this.courseRepository.findViewCourseById(userId, id, filter);
    try {
      if (userId) {
        inCart = await isInCart(userId, course.id,'course' ,this.cartItemRepository);
        inWishlist = await isInWishlist(userId, course.id, 'course', this.wishlistRepository);
        return {
          enrolled,
          progress,
          inCart,
          inWishlist,
          course
        };
      } else {
        return {
          enrolled,
          inCart,
          progress,
          inWishlist,
          course
        };
      }
    } catch (err) {
      return {
        enrolled,
        inCart,
        progress,
        inWishlist,
        course
      };
    }
  }

  @put('/courses/{id}')
  @response(200, {
    description: 'Course model instance',
    content: { 'application/json': { schema: getModelSchemaRef(Course) } },
  })
  async updateById(
    @param.path.number('id') id: number,
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CourseObjectClass, {
            title: 'NewCourse',
          }),
        },
      },
    })
    course: CourseObjectClass,
  ): Promise<{ message: string }> {
    const courseDb = await this.courseRepository.findById(id);

    try {
      await isAdmin(token, this.userRepository);
      const instructor = await this.instructorRepository.findById(course.instructor);
      if (instructor) {
        const courseObj = await this.courseRepository.updateCourse(courseDb, course);
        for (const cat of courseDb.categories) {
          let category = await this.categoryRepository.findById(cat);
          if (category && category.coursesCount) {
            category.coursesCount -= 1;
            await this.categoryRepository.updateById(category.id ?? 0, category);
          }
        }
        for (let index = 0; index < course.categories.length; index++) {
          let category = await this.categoryRepository.findById(course.categories[index]);
          if (category && category.coursesCount) {
            category.coursesCount += 1;
            await this.categoryRepository.updateById(category.id ?? 0, category);
          }
        }
        return { message: 'success' };
      } else {
        throw new HttpErrors['404']
      }
    }
    catch (err) {
      const userId = await isInstructor(token, this.userRepository);
      const instructor = await this.instructorRepository.find({ where: { user: userId } });
      if (instructor?.[0]) {
        course.instructor = instructor[0].id;
        const courseObj = await this.courseRepository.updateCourse(courseDb, course);
        for (const cat of courseDb.categories) {
          let category = await this.categoryRepository.findById(cat);
          if (category && category.coursesCount) {
            category.coursesCount -= 1;
            await this.categoryRepository.updateById(category.id ?? 0, category);
          }
        }
        for (let index = 0; index < course.categories.length; index++) {
          let category = await this.categoryRepository.findById(course.categories[index]);
          if (category && category.coursesCount) {
            category.coursesCount += 1;
            await this.categoryRepository.updateById(category.id ?? 0, category);
          }
        }
        return { message: 'success' };
      } else {
        throw new HttpErrors['403']
      }
    }

  }

  @del('/courses/{id}')
  @response(200, {
    description: 'Course DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<{message: 'success'}> {
    await this.courseRepository.deleteCourse(id);
    return {message: 'success'};
  }
}
