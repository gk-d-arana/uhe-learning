
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  requestBody,
  response,
} from '@loopback/rest';
import {LiveStream} from '../models';
import {LiveStreamRepository} from '../repositories';
import { LiveStreamCreation } from '../types/requests';
import { isInstructor } from '../common/middleware';
import { UserRepository } from '../repositories/user.repository';
                                    import { generateImageFileFromBase64 } from '../common/generator';

export class LiveStreamController {
  constructor(
    @repository(LiveStreamRepository)
    public liveStreamRepository : LiveStreamRepository,
    @repository(UserRepository)
    public userRepository : UserRepository,

  ) {}

  @post('/live-streams')
  @response(200, {
    description: 'LiveStream model instance',
    content: {'application/json': {schema: getModelSchemaRef(LiveStream)}},
  })
  async create(
    @param.header.string("Authorization") token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LiveStreamCreation, {
            title: 'NewLiveStream',
          }),
        },
      },
    })
    liveStream: LiveStreamCreation,
  ): Promise<LiveStream> {
    const userId = await isInstructor(token, this.userRepository)
    const user = await this.userRepository.findById(userId);
    liveStream.user = userId;
    let coverImage;
    if(liveStream.coverImage != null){
      coverImage = liveStream.coverImage
    }else{
      coverImage = user.profileImage ?? '';
    }
    const link = await generateImageFileFromBase64(
      'live-streams',
      coverImage,
      `${liveStream.name}_` +
        Math.floor(Math.random() * 22424),
    )
    liveStream.coverImage = link;
    const ls = await this.liveStreamRepository.create(liveStream);
    delete ls.user;
    return ls;
  }

  @get('/live-streams/count')
  @response(200, {
    description: 'LiveStream model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(LiveStream) where?: Where<LiveStream>,
  ): Promise<Count> {
    return this.liveStreamRepository.count(where);
  }

  @get('/live-streams')
  @response(200, {
    description: 'Array of LiveStream model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(LiveStream, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(LiveStream) filter?: Filter<LiveStream>,
  ): Promise<any[]> {
    const res = await this.liveStreamRepository.getHomeLiveStreams();
    const ress = [];
    for (const l of res) {
      const instruc = await this.userRepository.find({where:{id:l.user}});
      if(instruc?.[0]){
        ress.push({...l, instructor: {
          id: l.user,
          email: instruc[0].email,
          firstName: instruc[0].firstName,
          lastName: instruc[0].lastName,
          phoneNumber: instruc[0].phoneNumber,
          profileImage: instruc[0].profileImage
        }})
      }
    }
    return ress;

  }

  @get('/live-streams/{id}')
  @response(200, {
    description: 'LiveStream model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(LiveStream, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(LiveStream, {exclude: 'where'}) filter?: FilterExcludingWhere<LiveStream>
  ): Promise<LiveStream> {
    return this.liveStreamRepository.findById(id, filter);
  }
}
