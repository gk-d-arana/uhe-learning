/* eslint-disable @typescript-eslint/no-explicit-any */
import {Filter, repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {CartItemResponse} from '../types/responses';
import { isStudent, isInstructor } from '../common/middleware';
import {CartItemTransformer} from '../common/transformers';
import {CartItem} from '../models';
import {CartItemClass} from '../models/cart-item.model';
import {CartItemRepository, GlobalTestRepository, ShopItemEnrollmentRepository} from '../repositories';
import {CourseRepository} from '../repositories/course.repository';
import {UserRepository} from '../repositories/user.repository';
import { ShopItemRepository } from '../repositories/shop-item.repository';
import { WishlistRepository } from '../repositories/wishlist.repository';

export class CartItemController {
  constructor(
    @repository(CartItemRepository)
    public cartItemRepository: CartItemRepository,
    @repository(ShopItemEnrollmentRepository)
    public shopItemEnrollmentRepository: ShopItemEnrollmentRepository,
    @repository(WishlistRepository)
    public wishlistRepository: WishlistRepository,
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
    @repository(ShopItemRepository)
    public shopItemRepository: ShopItemRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(CourseRepository)
    public courseRepository: CourseRepository,
  ) {}

  @post('/cart')
  @response(200, {
    description: 'CartItem model instance',
    content: {'application/json': {schema: getModelSchemaRef(CartItem)}},
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(CartItemClass, {
            title: 'NewCartItem',
          }),
        },
      },
    })
    cartItem: CartItemClass,
  ): Promise<any> {
    let userId;
    try{
      userId = await isStudent(token, this.userRepository);
    }catch(err){
      userId = await isInstructor(token, this.userRepository);
      if(cartItem.type !== 'shopItem'){
        throw new HttpErrors.BadRequest('Instructor Can Only Buy Shop Items');
      }
    }
    const res = await this.cartItemRepository.addItemToCart(cartItem, userId);
    if (res === false) {
      return {
        message: 'removed item from cart',
      };
    } else if (cartItem.type === 'shopItem') {
      const shopItem = await this.shopItemRepository.findById(cartItem.item);
      return {
        id: res.id,
        quantity: res.quantity,
        shopItem: {
          name: shopItem.name,
          price: shopItem.price,
          courseImage: shopItem.shopItemImage,
        },
      };
    } else if (cartItem.type === 'course') {
      const course = await this.courseRepository.findById(cartItem.item);
      return {
        id: res.id,
        quantity: res.quantity,
        course: {
          name: course.name,
          price: course.price,
          courseImage: course.courseImage,
        },
      };
    }
    else if (cartItem.type === 'globalTest') {
      const globalTest = await this.globalTestRepository.findById(cartItem.item);
      return {
        id: res.id,
        quantity: res.quantity,
        globalTest: {
          name: globalTest.name,
          price: globalTest.price,
          courseImage: "",
        },
      };
    }
  }

  @get('/cart')
  @response(200, {
    description: 'Array of CartItem model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(CartItem, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.filter(CartItem) filter?: Filter<CartItem>,
  ): Promise<CartItemResponse[]> {
    let userId;
    try{
      userId = await isStudent(token, this.userRepository);
    }catch(err){
      userId = await isInstructor(token, this.userRepository);
    }
    const carts = await this.cartItemRepository.find({
      where: {user: userId, inCart: true},
    });
    return CartItemTransformer(userId, carts, this.courseRepository, this.shopItemRepository, this.wishlistRepository, this.cartItemRepository,  this.globalTestRepository, this.shopItemEnrollmentRepository);
  }
}
