import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import { Section } from '../models';
import { AudioRepository, GlobalTestRepository, LessonAssetRepository, LessonRepository, SectionRepository, TestRepository, VideoRepository } from '../repositories';
import { SectionObjectClass } from '../models/section.model';
import { deleteSection } from '../common/deleters';

export class SectionController {
  constructor(
    @repository(SectionRepository)
    public sectionRepository: SectionRepository,
    @repository(LessonRepository)
    public lessonRepository: LessonRepository,
    @repository(VideoRepository)
    public videoRepository: VideoRepository,
    @repository(AudioRepository)
    public audioRepository: AudioRepository,
    @repository(LessonAssetRepository)
    public lessonAssetRepository: LessonAssetRepository,
    @repository(TestRepository)
    public testRepository: TestRepository,
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
  ) { }

  @post('/sections')
  @response(200, {
    description: 'Section model instance',
    content: { 'application/json': { schema: getModelSchemaRef(Section) } },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(SectionObjectClass, {
            title: 'NewSection',
          }),
        },
      },
    })
    section: SectionObjectClass,
  ): Promise<Section> {
    const creationDate = new Date();
    section.createdAt = creationDate.toISOString();
    section.updatedAt = creationDate.toISOString();
    return this.sectionRepository.create(section);
  }

  @get('/sections/count')
  @response(200, {
    description: 'Section model count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async count(
    @param.where(Section) where?: Where<Section>,
  ): Promise<Count> {
    return this.sectionRepository.count(where);
  }

  @get('/sections')
  @response(200, {
    description: 'Array of Section model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Section, { includeRelations: true }),
        },
      },
    },
  })
  async find(
    @param.filter(Section) filter?: Filter<Section>,
  ): Promise<Section[]> {
    return this.sectionRepository.find(filter);
  }

  @patch('/sections')
  @response(200, {
    description: 'Section PATCH success count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Section, { partial: true }),
        },
      },
    })
    section: Section,
    @param.where(Section) where?: Where<Section>,
  ): Promise<Count> {
    return this.sectionRepository.updateAll(section, where);
  }

  @get('/sections/{id}')
  @response(200, {
    description: 'Section model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Section, { includeRelations: true }),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Section, { exclude: 'where' }) filter?: FilterExcludingWhere<Section>
  ): Promise<Section> {
    return this.sectionRepository.findById(id, filter);
  }

  @patch('/sections/{id}')
  @response(204, {
    description: 'Section PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Section, { partial: true }),
        },
      },
    })
    section: Section,
  ): Promise<void> {
    await this.sectionRepository.updateById(id, section);
  }

  @put('/sections/{id}')
  @response(200, {
    description: 'Section PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() section: Section,
  ): Promise<{ message: string }> {
    await this.sectionRepository.replaceById(id, section);
    return { message: 'success' };
  }

  @del('/sections/{id}')
  @response(200, {
    description: 'Section DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<{ message: 'success' }> {
    const section = await this.findById(id);
    await deleteSection(section, this.sectionRepository, this.lessonRepository,
      this.videoRepository,
      this.audioRepository,
      this.lessonAssetRepository,
      this.testRepository,
      this.globalTestRepository);
    return { message: 'success' };
  }
}
