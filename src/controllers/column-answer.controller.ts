import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {canAddCourse} from '../common/middleware';
import {ColumnAnswer} from '../models';
import {ColumnAnswerCreationClass} from '../models/column-answer.model';
import {ColumnAnswerRepository} from '../repositories';
import {AdminRepository} from '../repositories/admin.repository';
import {InstructorRepository} from '../repositories/instructor.repository';
import { UserRepository } from '../repositories/user.repository';

export class ColumnAnswerController {
  constructor(
    @repository(ColumnAnswerRepository)
    public columnAnswerRepository: ColumnAnswerRepository,
    @repository(AdminRepository)
    public adminRepository: AdminRepository,
    @repository(InstructorRepository)
    public instructorRepository: InstructorRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) {}

  @post('/column-answers')
  @response(200, {
    description: 'ColumnAnswer model instance',
    content: {'application/json': {schema: getModelSchemaRef(ColumnAnswer)}},
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ColumnAnswerCreationClass, {
            title: 'NewColumnAnswer',
          }),
        },
      },
    })
    columnAnswer: ColumnAnswerCreationClass,
  ): Promise<ColumnAnswer> {
    await canAddCourse(token, this.userRepository);

    return this.columnAnswerRepository.createOnCondition(columnAnswer);
  }

  @get('/column-answers/count')
  @response(200, {
    description: 'ColumnAnswer model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(ColumnAnswer) where?: Where<ColumnAnswer>,
  ): Promise<Count> {
    return this.columnAnswerRepository.count(where);
  }

  @get('/column-answers')
  @response(200, {
    description: 'Array of ColumnAnswer model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(ColumnAnswer, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(ColumnAnswer) filter?: Filter<ColumnAnswer>,
  ): Promise<ColumnAnswer[]> {
    return this.columnAnswerRepository.find(filter);
  }

  @patch('/column-answers')
  @response(200, {
    description: 'ColumnAnswer PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ColumnAnswer, {partial: true}),
        },
      },
    })
    columnAnswer: ColumnAnswer,
    @param.where(ColumnAnswer) where?: Where<ColumnAnswer>,
  ): Promise<Count> {
    return this.columnAnswerRepository.updateAll(columnAnswer, where);
  }

  @get('/column-answers/{id}')
  @response(200, {
    description: 'ColumnAnswer model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(ColumnAnswer, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ColumnAnswer, {exclude: 'where'})
    filter?: FilterExcludingWhere<ColumnAnswer>,
  ): Promise<ColumnAnswer> {
    return this.columnAnswerRepository.findById(id, filter);
  }

  @patch('/column-answers/{id}')
  @response(204, {
    description: 'ColumnAnswer PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ColumnAnswer, {partial: true}),
        },
      },
    })
    columnAnswer: ColumnAnswer,
  ): Promise<void> {
    await this.columnAnswerRepository.updateById(id, columnAnswer);
  }

  @put('/column-answers/{id}')
  @response(200, {
    description: 'ColumnAnswer PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() columnAnswer: ColumnAnswer,
  ) {
    await this.columnAnswerRepository.replaceById(id, columnAnswer);
    return {message: 'success'};
  }

  @del('/column-answers/{id}')
  @response(204, {
    description: 'ColumnAnswer DELETE success',
  })
  async deleteById(@param.path.number('id') id: number){
    await this.columnAnswerRepository.deleteById(id);
    return {message: 'success'};
  }
}
