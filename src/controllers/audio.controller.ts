import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {Audio} from '../models';
import {AudioObjectClass} from '../models/audio.model';
import {AudioRepository} from '../repositories';
import { deleteAudio } from '../common/deleters';

export class AudioController {
  constructor(
    @repository(AudioRepository)
    public audioRepository: AudioRepository,
  ) {}

  @post('/audio')
  @response(200, {
    description: 'Audio model instance',
    content: {'application/json': {schema: getModelSchemaRef(Audio)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AudioObjectClass, {
            title: 'NewAudio',
          }),
        },
      },
    })
    audio: Audio,
  ): Promise<Audio> {
    return this.audioRepository.create(audio);
  }

  @get('/audio/count')
  @response(200, {
    description: 'Audio model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Audio) where?: Where<Audio>): Promise<Count> {
    return this.audioRepository.count(where);
  }

  @get('/audio')
  @response(200, {
    description: 'Array of Audio model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Audio, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(Audio) filter?: Filter<Audio>): Promise<Audio[]> {
    return this.audioRepository.find(filter);
  }

  @patch('/audio')
  @response(200, {
    description: 'Audio PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Audio, {partial: true}),
        },
      },
    })
    audio: Audio,
    @param.where(Audio) where?: Where<Audio>,
  ): Promise<Count> {
    return this.audioRepository.updateAll(audio, where);
  }

  @get('/audio/{id}')
  @response(200, {
    description: 'Audio model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Audio, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Audio, {exclude: 'where'})
    filter?: FilterExcludingWhere<Audio>,
  ): Promise<Audio> {
    return this.audioRepository.findById(id, filter);
  }

  @patch('/audio/{id}')
  @response(204, {
    description: 'Audio PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Audio, {partial: true}),
        },
      },
    })
    audio: Audio,
  ): Promise<void> {
    await this.audioRepository.updateById(id, audio);
  }

  @put('/audio/{id}')
  @response(200, {
    description: 'Audio PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AudioObjectClass, {
            title: 'NewVideo',
          }),
        },
      },
    })
    audio: AudioObjectClass,
  ){
    const audioDb = await this.audioRepository.findById(id);
    await this.audioRepository.updateAudio(audioDb, audio);
    return {message: 'success'}
  }

  @del('/audio/{id}')
  @response(200, {
    description: 'Audio DELETE success',
  })
  async deleteById(@param.path.number('id') id: number){
    const audioDb = await this.audioRepository.findById(id);
    deleteAudio(audioDb, this.audioRepository);
    return {message: 'success'}
  }
}
