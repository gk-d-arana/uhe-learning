/* eslint-disable @typescript-eslint/no-explicit-any */
import {Filter, repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  param,
  post,
  requestBody,
  response,
} from '@loopback/rest';
import {isInstructor, isStudent} from '../common/middleware';
import {CartItemTransformer} from '../common/transformers';
import {Wishlist} from '../models';
import {WishlistItemClass} from '../models/wishlist.model';
import {CartItemRepository, GlobalTestRepository, ShopItemEnrollmentRepository, WishlistRepository} from '../repositories';
import {CourseRepository} from '../repositories/course.repository';
import {ShopItemRepository} from '../repositories/shop-item.repository';
import {UserRepository} from '../repositories/user.repository';
import {WishlistResponse} from '../types/responses';

export class WishlistController {
  constructor(
    @repository(WishlistRepository)
    public wishlistRepository: WishlistRepository,
    @repository(ShopItemRepository)
    public shopItemRepository: ShopItemRepository,
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(CourseRepository)
    public courseRepository: CourseRepository,
    @repository(CartItemRepository)
    public cartItemRepository: CartItemRepository,
    @repository(ShopItemEnrollmentRepository)
    public shopItemEnrollmentRepository: ShopItemEnrollmentRepository,
  ) { }

  @post('/wishlist')
  @response(200, {
    description: 'Wishlist model instance',
    content: {'application/json': {schema: getModelSchemaRef(Wishlist)}},
  })
  async create(
    @param.header.string('Authorization') token: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(WishlistItemClass, {
            title: 'NewWishlist',
          }),
        },
      },
    })
    wishlist: WishlistItemClass,
  ): Promise<any> {
    let userId;
    try {
      userId = await isStudent(token, this.userRepository);
    } catch (err) {
      userId = await isInstructor(token, this.userRepository);
    }
    const res = await this.wishlistRepository.addItemToWishlist(wishlist, userId);
    if (res === false) {
      return {
        message: 'removed item from wishlist',
      };
    } else if (wishlist.type === 'shopItem') {
      const shopItem = await this.shopItemRepository.findById(wishlist.item);
      return {
        id: res.id,
        quantity: res.quantity,
        shopItem: {
          name: shopItem.name,
          price: shopItem.price,
          courseImage: shopItem.shopItemImage,
        },
      };
    } else if (wishlist.type === 'course') {
      const course = await this.courseRepository.findById(wishlist.item);
      return {
        id: res.id,
        quantity: res.quantity,
        course: {
          name: course.name,
          price: course.price,
          courseImage: course.courseImage,
        },
      };
    }
    else if (wishlist.type === 'globalTest') {
      const globalTest = await this.globalTestRepository.findById(wishlist.item);
      return {
        id: res.id,
        quantity: res.quantity,
        globalTest: {
          name: globalTest.name,
          price: globalTest.price,
          courseImage: '',
        },
      };
    }
  }

  @get('/wishlist')
  @response(200, {
    description: 'Array of Wishlist model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Wishlist, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.header.string('Authorization') token: string,
    @param.filter(Wishlist) filter?: Filter<Wishlist>,
  ): Promise<WishlistResponse[]> {
    let userId;
    try {
      userId = await isStudent(token, this.userRepository);
    } catch (err) {
      userId = await isInstructor(token, this.userRepository);
    }
    const wishlists = await this.wishlistRepository.find({
      where: {user: userId, inWishlist: true},
    });
    return CartItemTransformer(
      userId,
      wishlists,
      this.courseRepository,
      this.shopItemRepository,
      this.wishlistRepository,
      this.cartItemRepository,
      this.globalTestRepository,
      this.shopItemEnrollmentRepository
    );
  }
}
