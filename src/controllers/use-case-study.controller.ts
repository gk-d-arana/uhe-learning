import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {UseCaseStudy, UseCaseStudyQuestion} from '../models';
import {UseCaseStudyQuestionAnswerRepository, UseCaseStudyQuestionRepository, UseCaseStudyRepository} from '../repositories';
import { UseCaseStudyResonse } from '../types/responses';

export class UseCaseStudyController {
  constructor(
    @repository(UseCaseStudyRepository)
    public useCaseStudyRepository : UseCaseStudyRepository,
    @repository(UseCaseStudyQuestionRepository)
    public useCaseStudyQuestionRepository : UseCaseStudyQuestionRepository,
    @repository(UseCaseStudyQuestionAnswerRepository)
    public useCaseStudyQuestionAnswerRepository : UseCaseStudyQuestionAnswerRepository,
  ) {}

  @post('/use-case-studies')
  @response(200, {
    description: 'UseCaseStudy model instance',
    content: {'application/json': {schema: getModelSchemaRef(UseCaseStudy)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UseCaseStudy, {
            title: 'NewUseCaseStudy',
            exclude: ['id'],
          }),
        },
      },
    })
    useCaseStudy: Omit<UseCaseStudy, 'id'>,
  ): Promise<UseCaseStudy> {
    return this.useCaseStudyRepository.create(useCaseStudy);
  }

  @get('/use-case-studies/count')
  @response(200, {
    description: 'UseCaseStudy model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(UseCaseStudy) where?: Where<UseCaseStudy>,
  ): Promise<Count> {
    return this.useCaseStudyRepository.count(where);
  }

  @get('/use-case-studies')
  @response(200, {
    description: 'Array of UseCaseStudy model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(UseCaseStudy, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(UseCaseStudy) filter?: Filter<UseCaseStudy>,
  ): Promise<UseCaseStudy[]> {
    return this.useCaseStudyRepository.find(filter);
  }

  @patch('/use-case-studies')
  @response(200, {
    description: 'UseCaseStudy PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UseCaseStudy, {partial: true}),
        },
      },
    })
    useCaseStudy: UseCaseStudy,
    @param.where(UseCaseStudy) where?: Where<UseCaseStudy>,
  ): Promise<Count> {
    return this.useCaseStudyRepository.updateAll(useCaseStudy, where);
  }

  @get('/use-case-studies/{id}')
  @response(200, {
    description: 'UseCaseStudy model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(UseCaseStudy, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(UseCaseStudy, {exclude: 'where'}) filter?: FilterExcludingWhere<UseCaseStudy>
  ): Promise<UseCaseStudyResonse|undefined> {
    const useCase =  await this.useCaseStudyRepository.findById(id, filter);
    if(useCase){
      const res : UseCaseStudyResonse = {useCase: useCase, questions: []};
      const questions = await this.useCaseStudyQuestionRepository.find({where: {useCaseStudy: (await useCase).id}})
      for (const question of questions) {
        const questionRes:any = {
          question: question.question,
          createdAt: question.createdAt,
          updatedAt: question.updatedAt,
          answers: []
        }
        const answers = await this.useCaseStudyQuestionAnswerRepository.find({where: {useCaseStudyQuestion: question.id}});
        for (const answer of answers) {
          questionRes.answers.push({
            value: answer.value,
            next: answer.next,
            createdAt: answer.createdAt,
            updatedAt: answer.updatedAt
          })
        }
        res.questions.push(questionRes);
      }
      return res;
    }
    return;
  }

  @patch('/use-case-studies/{id}')
  @response(204, {
    description: 'UseCaseStudy PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UseCaseStudy, {partial: true}),
        },
      },
    })
    useCaseStudy: UseCaseStudy,
  ): Promise<void> {
    await this.useCaseStudyRepository.updateById(id, useCaseStudy);
  }

  @put('/use-case-studies/{id}')
  @response(204, {
    description: 'UseCaseStudy PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() useCaseStudy: UseCaseStudy,
  ): Promise<void> {
    await this.useCaseStudyRepository.replaceById(id, useCaseStudy);
  }

  @del('/use-case-studies/{id}')
  @response(204, {
    description: 'UseCaseStudy DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.useCaseStudyRepository.deleteById(id);
  }
}
