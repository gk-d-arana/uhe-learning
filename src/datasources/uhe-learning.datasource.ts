import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'UHELearning',
  connector: 'mysql',
  host: 'clients.aigate.me',
  port: 3306,
  user: "c4_db_uht_dev_u",
  password: 'eUzYHU75!',
  database: 'c4_uht_dev'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class UheLearningDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'UHELearning';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.UHELearning', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
