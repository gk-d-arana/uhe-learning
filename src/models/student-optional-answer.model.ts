import {Entity, model, property} from '@loopback/repository';

@model({})
export class StudentOptionalAnswer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  studentTest: number;

  @property({
    type: 'number',
    required: true,
    default: 0
  })
  answerId: number;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  @property({
    type: 'boolean',
    required: true,
  })
  isTrue: boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StudentOptionalAnswer>) {
    super(data);
  }
}

export interface StudentOptionalAnswerRelations {
  // describe navigational properties here
}

export type StudentOptionalAnswerWithRelations = StudentOptionalAnswer & StudentOptionalAnswerRelations;
