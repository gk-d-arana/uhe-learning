import {Entity, model, property} from '@loopback/repository';

@model({})
export class Wishlist extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'number',
    required: true,
  })
  item: number;

  @property({
    type: 'number',
    required: true,
  })
  quantity: number;

  @property({
    type: 'string',
    required: true,
  })
  type: 'course' | 'shopItem' | 'globalTest';

  @property({
    type: 'boolean',
    default: true,
  })
  inWishlist?: boolean;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Wishlist>) {
    super(data);
  }
}

export interface WishlistRelations {
  // describe navigational properties here
}

export class WishlistItemClass {
  item: number;
  type: 'course' | 'shopItem' | 'globalTest';
  quantity: number;
  user?: number;
  createdAt?: string;
  updatedAt: string;
}

export type WishlistWithRelations = Wishlist & WishlistRelations;
