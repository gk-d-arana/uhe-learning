import {Entity, model, property} from '@loopback/repository';

@model({})
export class User extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  firstName: string;

  @property({
    type: 'string',
    required: true,
  })
  lastName: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  phoneNumber: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'string',
  })
  address?: string;

  @property({
    type: 'string',
    required: true,
    hidden: true,
  })
  password: string;

  @property({
    type: 'number',
    default: 20,
  })
  age?: number;

  @property({
    type: 'string',
    default: 'student',
  })
  role?: string;

  @property({
    type: 'string',
    required: false,
    default: "",
  })
  profileImage?: string;

  @property({
    type: 'string',
    required: false,
    hidden: true,
  })
  secretCode?: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}
export class RegisterObjectClass {
  email: string;
  password?: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address?: string;
  age?: number;
  role: 'student' | 'instructor' | 'admin';
  profileImage?: string;
  bio?: string;
  education?: string;
}


export type UserWithRelations = User & UserRelations;
