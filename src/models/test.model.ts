import { Entity, model, property } from '@loopback/repository';

@model({})
export class Test extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'boolean',
    default: false,
    hidden: true,
    required: false,
  })
  isDeleted: boolean;

  @property({
    type: 'number',
    required: true, // if globalTest == 0 then this is inside a course
  })
  globalTest: number;

  @property({
    type: 'string',
    required: true,
  })
  question: string;

  @property({
    type: 'string',
    required: true,
  })
  type: 'written' | 'optional' | 'match';

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Test>) {
    super(data);
  }
}

export interface TestRelations {
  // describe navigational properties here
}

export class TestCreationClass {
  name: string;
  globalTest: number; // if globalTest == 0 then this is inside a course
  question: string;
  type: 'written' | 'optional' | 'match';
}

export type TestWithRelations = Test & TestRelations;
