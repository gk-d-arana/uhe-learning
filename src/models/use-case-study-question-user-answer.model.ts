import {Entity, model, property} from '@loopback/repository';

@model({})
export class UseCaseStudyQuestionUserAnswer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  useCaseStudyQuestionAnswer: number;

  @property({
    type: 'number',
    required: true,
  })
  userUseCaseStudyQuestion: number;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<UseCaseStudyQuestionUserAnswer>) {
    super(data);
  }
}

export interface UseCaseStudyQuestionUserAnswerRelations {
  // describe navigational properties here
}

export type UseCaseStudyQuestionUserAnswerWithRelations = UseCaseStudyQuestionUserAnswer & UseCaseStudyQuestionUserAnswerRelations;
