import {Entity, model, property} from '@loopback/repository';

@model({})
export class UserUseCaseAnswer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  userUseCaseStudy: number;

  @property({
    type: 'number',
    required: true,
  })
  useCaseStudyQuestion: number;

  @property({
    type: 'number',
    required: true,
  })
  useCaseStudyQuestionAnswer: number;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<UserUseCaseAnswer>) {
    super(data);
  }
}

export interface UserUseCaseAnswerRelations {
  // describe navigational properties here
}

export type UserUseCaseAnswerWithRelations = UserUseCaseAnswer & UserUseCaseAnswerRelations;
