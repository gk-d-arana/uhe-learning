import { Entity, model, property } from '@loopback/repository';

@model({})
export class Video extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  description: string;

  @property({
    type: 'string',
    required: true,
  })
  videoUrl: string;

  @property({
    type: 'number',
    required: true,
  })
  lesson: number;

  @property({
    type: 'number',
    required: true,
  })
  videoDuration: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Video>) {
    super(data);
  }
}

export interface VideoRelations {
  // describe navigational properties here
}

export class VideoObjectClass {
  name: string;
  description: string;
  videoUrl: string;
  lesson: number;
  extension?: string;
  videoDuration: number;
}

export type VideoWithRelations = Video & VideoRelations;
