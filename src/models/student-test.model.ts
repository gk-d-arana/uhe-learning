import { Entity, model, property } from '@loopback/repository';

@model({})
export class StudentTest extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;


  @property({
    type: 'number',
    required: true
  })
  globalStudentTest: number;

  @property({
    type: 'number',
    required: true
  })
  test: number;

  @property({
    type: 'boolean',
    default: false
  })
  isChecked: boolean;


  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StudentTest>) {
    super(data);
  }
}

export interface StudentTestRelations {
  // describe navigational properties here
}

export type StudentTestWithRelations = StudentTest & StudentTestRelations;
