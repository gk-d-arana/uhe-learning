import {Entity, model, property} from '@loopback/repository';

@model({})
export class UseCaseStudy extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<UseCaseStudy>) {
    super(data);
  }
}

export interface UseCaseStudyRelations {
  // describe navigational properties here
}

export type UseCaseStudyWithRelations = UseCaseStudy & UseCaseStudyRelations;
