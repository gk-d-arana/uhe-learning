import {Entity, model, property} from '@loopback/repository';
import {TransferPaymentCreationClass} from './transfer-payment.model';

@model({})
export class LocalPayment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'boolean',
    required: true,
  })
  isEpay: boolean;

  @property({
    type: 'number',
    default: 0,
  })
  transferPayment?: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LocalPayment>) {
    super(data);
  }
}

export interface LocalPaymentRelations {
  // describe navigational properties here
}

export class LocalPaymentCreationClass {
  isEpay: boolean;
  transferPayment?: TransferPaymentCreationClass;
}

export type LocalPaymentWithRelations = LocalPayment & LocalPaymentRelations;
