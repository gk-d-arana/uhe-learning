import {Entity, model, property} from '@loopback/repository';

@model({})
export class ShopItemEnrollment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'number',
    required: true,
  })
  shopItem: number;

  @property({
    type: 'string',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ShopItemEnrollment>) {
    super(data);
  }
}

export interface ShopItemEnrollmentRelations {
  // describe navigational properties here
}

export type ShopItemEnrollmentWithRelations = ShopItemEnrollment & ShopItemEnrollmentRelations;
