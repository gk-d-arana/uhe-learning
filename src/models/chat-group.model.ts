import {Entity, model, property} from '@loopback/repository';

@model({})
export class ChatGroup extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'array',
    itemType: 'number',
    required: true,
  })
  users: number[];

  @property({
    type: 'number',
    required: true,
    default: 0
  })
  user: number;

  @property({
    type: 'array',
    itemType: 'number',
  })
  messages?: number[];

  @property({
    type: 'string',
    default: "",
  })
  name?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ChatGroup>) {
    super(data);
  }
}

export interface ChatGroupRelations {
  // describe navigational properties here
}

export type ChatGroupWithRelations = ChatGroup & ChatGroupRelations;
