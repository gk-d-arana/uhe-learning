import { Entity, model, property } from '@loopback/repository';

@model({})
export class Answer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  value: string;

  @property({
    type: 'number',
    required: true,
  })
  test: number;

  @property({
    type: 'boolean',
    required: true,
  })
  isTrue: boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Answer>) {
    super(data);
  }
}

export interface AnswerRelations {
  // describe navigational properties here
}


export class AnswerCreationClass{
  value: string;
  test: number;
  isTrue: boolean;
}

export type AnswerWithRelations = Answer & AnswerRelations;
