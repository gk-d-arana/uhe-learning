import {Entity, model, property} from '@loopback/repository';

@model()
export class StudentLesson extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'number',
    required: true,
  })
  lesson: number;

  @property({
    type: 'number',
    required: true,
  })
  section: number;

  @property({
    type: 'number',
    required: true,
  })
  course: number;

  @property({
    type: 'string',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'string',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;


  @property({
    type: 'boolean',
    default: false,
  })
  isWatched: boolean;

  constructor(data?: Partial<StudentLesson>) {
    super(data);
  }
}

export interface StudentLessonRelations {
  // describe navigational properties here
}

export type StudentLessonWithRelations = StudentLesson & StudentLessonRelations;
