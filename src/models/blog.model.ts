import {Entity, model, property} from '@loopback/repository';

@model({})
export class Blog extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  description: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;

  @property({
    type: 'string',
    required: true,
  })
  blogImage: string;

  @property({
    type: 'string',
    default: '',
  })
  blogVideo?: string;

  @property({
    type: 'array',
    itemType: 'number',
    default: [],
  })
  categories?: number[];

  @property({
    type: 'boolean',
    default: false
  })
  isPrimary?: boolean;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Blog>) {
    super(data);
  }
}

export interface BlogRelations {
  // describe navigational properties here
}

export type BlogWithRelations = Blog & BlogRelations;
