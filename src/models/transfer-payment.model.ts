import { Entity, model, property } from '@loopback/repository';

@model({})
export class TransferPayment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  companyName: string;

  @property({
    type: 'string',
    required: true,
  })
  phoneNumber: string;

  @property({
    type: 'string',
    required: true,
  })
  idNumber: string;

  @property({
    type: 'string',
    required: true,
  })
  proofImage: string;


  @property({
    type: 'boolean',
    default: true,
  })
  isLocal?: boolean;



  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<TransferPayment>) {
    super(data);
  }
}

export interface TransferPaymentRelations {
  // describe navigational properties here
}

export class TransferPaymentCreationClass {
  companyName: string;
  phoneNumber: string;
  idNumber: string;
  proofImage: string;
  isLocal?: boolean
}

export type TransferPaymentWithRelations = TransferPayment &
  TransferPaymentRelations;
