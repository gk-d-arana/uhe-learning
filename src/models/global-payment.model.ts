import { Entity, model, property } from '@loopback/repository';
import { CardPaymentCreationClass } from './card-payment.model';
import { TransferPaymentCreationClass } from './transfer-payment.model';

@model({})
export class GlobalPayment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'boolean',
    required: true,
  })
  isPaypal: boolean;

  @property({
    type: 'number',
    required: true,
  })
  cardPayment: number;

  @property({
    type: 'number',
    required: true,
  })
  transferPayment: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<GlobalPayment>) {
    super(data);
  }
}

export interface GlobalPaymentRelations {
  // describe navigational properties here
}

export class GlobalPaymentCreationClass {
  isPaypal: boolean;
  cardPayment?: CardPaymentCreationClass;
  transferPayment?: TransferPaymentCreationClass;
}

export type GlobalPaymentWithRelations = GlobalPayment & GlobalPaymentRelations;
