import {Entity, model, property} from '@loopback/repository';

@model({})
export class Instructor extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'date',
    required: false,
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    required: false,
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  @property({
    type: 'string',
    default: "",
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  bio?: string;

  @property({
    type: 'string',
  })
  education?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isApproved?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isReviewed?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isPrimary?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isBlocked?: boolean;

  @property({
    type: 'string',
    default: '',
  })
  blockEndingLimit?: string;

  @property({
    type: 'string',
    default: '',
  })
  blockMessage?: string;

  @property({
    type: 'number',
    required: false,
    default: 0,
  })
  rating: number;

  @property({
    type: 'number',
    required: false,
    default: 0,
  })
  coursesEnrollment?: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Instructor>) {
    super(data);
  }
}

export interface InstructorRelations {
  // describe navigational properties here
}
