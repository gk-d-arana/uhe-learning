import {Entity, model, property} from '@loopback/repository';

@model({})
export class Course extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'LONGTEXT'
    },
    required: true,
  })
  mobileDescription: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    },
  })
  webDescription: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;

  @property({
    type: 'array',
    itemType: 'number',
    required: true,
  })
  categories: number[];

  @property({
    type: 'number',
    required: true,
  })
  instructor: number;

  @property({
    type: 'string',
    default: '',
  })
  courseImage?: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isApproved?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isReviewed?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isPrimary?: boolean;

  @property({
    type: 'number',
    required: true,
  })
  price: number;

  @property({
    type: 'string',
    default: '',
  })
  overviewImage?: string;

  @property({
    type: 'number',
    default: 0,
  })
  rating: number;

  @property({
    type: 'string',
    default: '',
  })
  descriptionVideo?: string;

  @property({
    type: 'string',
    default: '',
  })
  descriptionImage?: string;

  @property({
    type: 'number',
    default: 0,
  })
  enrollmentNumber: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;
  inWishlist?:  boolean;

  constructor(data?: Partial<Course>) {
    super(data);
  }
}

export interface CourseRelations {
  // describe navigational properties here
}

export class CourseObjectClass {
  name: string;
  mobileDescription: string;
  webDescription: string;
  categories: number[];
  instructor?: number;
  price: number;
  courseImage?: string;
  overviewImage?: string;
  descriptionVideo?: string;
  descriptionImage?: string;
  createdAt?: string;
  updatedAt: string;
}
