import {Entity, model, property} from '@loopback/repository';

@model({})
export class WrittenTestAnswer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  studentTest: number;

  @property({
    type: 'string',
    required: true,
  })
  value: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isTrue?: boolean;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<WrittenTestAnswer>) {
    super(data);
  }
}

export interface WrittenTestAnswerRelations {
  // describe navigational properties here
}

export type WrittenTestAnswerWithRelations = WrittenTestAnswer & WrittenTestAnswerRelations;
