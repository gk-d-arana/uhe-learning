import {Entity, model, property} from '@loopback/repository';

@model({})
export class UseCaseStudyQuestionAnswer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  value: string;

  @property({
    type: 'number',
    required: true,
  })
  useCaseStudyQuestion: number;

  @property({
    type: 'number',
    required: true,
  })
  next: number;
  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<UseCaseStudyQuestionAnswer>) {
    super(data);
  }
}

export interface UseCaseStudyQuestionAnswerRelations {
  // describe navigational properties here
}

export type UseCaseStudyQuestionAnswerWithRelations = UseCaseStudyQuestionAnswer & UseCaseStudyQuestionAnswerRelations;
