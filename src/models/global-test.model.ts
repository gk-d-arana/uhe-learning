import {Entity, model, property} from '@loopback/repository';

@model({})
export class GlobalTest extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  description: string;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'number',
    required: true,
  })
  price: number;

  @property({
    type: 'number',
    required: true,
  })
  course: number;

  @property({
    type: 'number',
    required: true,
  })
  lesson: number;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;


  @property({
    type: 'boolean',
    default: false,
    hidden: true,
    required: false
  })
  isDeleted: boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<GlobalTest>) {
    super(data);
  }
}

export interface GlobalTestRelations {
  // describe navigational properties here
}

export class GlobalTestCreationClass {
  name: string;
  description: string;
  user?: number;
  price: number;
  course: number;
  lesson: number;
}

export type GlobalTestWithRelations = GlobalTest & GlobalTestRelations;
