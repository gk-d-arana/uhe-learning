import {Entity, model, property} from '@loopback/repository';

@model({})
export class ChatMessagePollStudentAnswer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  chatMessagePoll: number;

  @property({
    type: 'boolean',
    required: true,
  })
  isTrue: boolean;


  @property({
    type: 'number',
    required: true,
  })
  chatMessagePollAnswer: number;


  @property({
    type: 'number',
    required: true,
  })
  user: number;


  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ChatMessagePollStudentAnswer>) {
    super(data);
  }
}

export interface ChatMessagePollStudentAnswerRelations {
  // describe navigational properties here
}

export type ChatMessagePollStudentAnswerWithRelations = ChatMessagePollStudentAnswer & ChatMessagePollStudentAnswerRelations;
