import {Entity, model, property} from '@loopback/repository';

@model({})
export class UserUseCaseStudy extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  useCaseStudy: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<UserUseCaseStudy>) {
    super(data);
  }
}

export interface UserUseCaseStudyRelations {
  // describe navigational properties here
}

export interface UserUseCaseStudyCreationClass {
  useCaseStudy: number;
  answers:{
    question : number;
    answer: number
  }[]
}

export type UserUseCaseStudyWithRelations = UserUseCaseStudy & UserUseCaseStudyRelations;
