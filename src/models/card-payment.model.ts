import { Entity, model, property } from '@loopback/repository';

@model({})
export class CardPayment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  firstName: string;

  @property({
    type: 'string',
    default: '',
  })
  lastName?: string;

  @property({
    type: 'string',
    required: true,
  })
  addressLine1: string;

  @property({
    type: 'string',
    default: '',
  })
  addressLine2?: string;

  @property({
    type: 'string',
    required: true,
  })
  zipCode: string;

  @property({
    type: 'string',
    required: true,
  })
  country: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CardPayment>) {
    super(data);
  }
}

export interface CardPaymentRelations {
  // describe navigational properties here
}

export class CardPaymentCreationClass {
  firstName: string;
  lastName?: string;
  addressLine1: string;
  addressLine2?: string;
  zipCode: string;
  country: string;
}

export type CardPaymentWithRelations = CardPayment & CardPaymentRelations;
