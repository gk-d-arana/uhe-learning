import { Entity, model, property } from '@loopback/repository';

@model({})
export class Audio extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  description: string;

  @property({
    type: 'string',
    required: true,
  })
  audioUrl: string;

  @property({
    type: 'number',
    required: true,
  })
  lesson: number;

  @property({
    type: 'number',
    required: true,
  })
  audioDuration: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Audio>) {
    super(data);
  }
}

export interface AudioRelations {
  // describe navigational properties here
}

export class AudioObjectClass {
  name: string;
  description: string;
  audioUrl: string;
  lesson: number;
  extension?: string;
  audioDuration: number;
}

export type AudioWithRelations = Audio & AudioRelations;
