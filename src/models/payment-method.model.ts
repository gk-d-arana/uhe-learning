import {Entity, model, property} from '@loopback/repository';
import {GlobalPaymentCreationClass} from './global-payment.model';
import {LocalPaymentCreationClass} from './local-payment.model';

@model({})
export class PaymentMethod extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  globalPayment: number;

  @property({
    type: 'number',
    required: true,
  })
  localPayment: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<PaymentMethod>) {
    super(data);
  }
}

export interface PaymentMethodRelations {
  // describe navigational properties here
}

export class PaymentMethodCreationClass {
  globalPayment: GlobalPaymentCreationClass;
  localPayment?: LocalPaymentCreationClass;
}

export type PaymentMethodWithRelations = PaymentMethod & PaymentMethodRelations;
