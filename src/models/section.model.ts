import {Entity, model, property} from '@loopback/repository';

@model({})
export class Section extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  description?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;

  @property({
    type: 'boolean',
    required: true,
  })
  isFree: boolean;

  @property({
    type: 'number',
    required: true,
  })
  course?: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Section>) {
    super(data);
  }
}

export interface SectionRelations {
  // describe navigational properties here
}

export class SectionObjectClass {
  name: string;
  description?: string;
  createdAt?: string;
  updatedAt?: string;
  isFree: boolean;
  course?: number;
}

export type SectionWithRelations = Section & SectionRelations;
