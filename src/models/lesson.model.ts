import {Entity, model, property} from '@loopback/repository';

@model({})
export class Lesson extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  section: number;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;

  @property({
    type: 'string',
    required: true,
  })
  type: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Lesson>) {
    super(data);
  }
}

export interface LessonRelations {
  // describe navigational properties here
}

export class LessonCreationClass {
  section: number;
  type: 'video' | 'audio' | 'asset' | 'test';
}

export type LessonWithRelations = Lesson & LessonRelations;
