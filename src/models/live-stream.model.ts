import {Entity, model, property} from '@loopback/repository';

@model({})
export class LiveStream extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'date',
    required: true,
  })
  startDate: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'number',
  })
  user?: number;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  description: string;

  @property({
    type: 'boolean',
    required: true,
  })
  isPrivate: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isLive?: boolean;

  @property({
    type: 'boolean',
    default: false,
  })
  isFromAdmin?: boolean;

  @property({
    type: 'string',
    default: '',
  })
  password?: string;

  @property({
    type: 'string',
    default: '',
  })
  coverImage?: string;


  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LiveStream>) {
    super(data);
  }
}

export interface LiveStreamRelations {
  // describe navigational properties here
}

export type LiveStreamWithRelations = LiveStream & LiveStreamRelations;
