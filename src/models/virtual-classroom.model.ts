import {Entity, model, property} from '@loopback/repository';
import {VRCInstanceMembersResponse} from '../types/responses';

@model({})
export class VirtualClassroom extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  description: string;

  @property({
    type: 'string',
    default: ""
  })
  coverImage: string

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'number',
    default: 0
  })
  chatGroup?: number;

  @property({
    type: 'array',
    itemType: 'number',
  })
  files?: number[];

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<VirtualClassroom>) {
    super(data);
  }
}

export class VirtualClassroomCreationClass {
  user?: number;
  coverImage: string;
  name: string
  description: string;
  chatGroup?: number;
}

export class VRCResponse {
  id: number | undefined;
  members: VRCInstanceMembersResponse[];
  name: string;
  coverImage: string;
  instructor: VRCInstanceMembersResponse;
  chatGroupId: number;
}

export interface VirtualClassroomRelations {
  // describe navigational properties here
}



export type VirtualClassroomWithRelations = VirtualClassroom &
  VirtualClassroomRelations;
