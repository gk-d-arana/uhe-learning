import { Entity, model, property } from '@loopback/repository';

@model({})
export class ColumnAnswer extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  test: number;

  @property({
    type: 'string',
    required: true,
  })
  value: string;

  @property({
    type: 'string',
    required: true,
  })
  oppositeValue: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<ColumnAnswer>) {
    super(data);
  }
}

export interface ColumnAnswerRelations {
  // describe navigational properties here
}

export class ColumnAnswerCreationClass {
  test: number;
  value: string;
  oppositeValue: string;
}

export type ColumnAnswerWithRelations = ColumnAnswer & ColumnAnswerRelations;
