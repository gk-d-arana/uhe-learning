import {Entity, model, property} from '@loopback/repository';

@model({})
export class GlobalTestEnrollment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'number',
    required: true
  })
  globalTest: number;

  @property({
    type: 'string',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;


  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<GlobalTestEnrollment>) {
    super(data);
  }
}

export interface GlobalTestEnrollmentRelations {
  // describe navigational properties here
}

export type GlobalTestEnrollmentWithRelations = GlobalTestEnrollment & GlobalTestEnrollmentRelations;



