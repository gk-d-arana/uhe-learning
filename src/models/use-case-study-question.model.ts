import {Entity, model, property} from '@loopback/repository';

@model({})
export class UseCaseStudyQuestion extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  question: string;

  @property({
    type: 'number',
    required: true,
  })
  useCaseStudy: number;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<UseCaseStudyQuestion>) {
    super(data);
  }
}

export interface UseCaseStudyQuestionRelations {
  // describe navigational properties here
}

export type UseCaseStudyQuestionWithRelations = UseCaseStudyQuestion & UseCaseStudyQuestionRelations;
