import {Entity, model, property} from '@loopback/repository';
import {PaymentMethodCreationClass} from './payment-method.model';

@model({})
export class Order extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'array',
    itemType: 'number',
    required: true,
  })
  cartItems: number[];

  @property({
    type: 'number',
    required: true,
  })
  paymentMethod: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'boolean',
    default: false,
  })
  paymentApproved?: boolean;


  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Order>) {
    super(data);
  }
}

export interface OrderRelations {
  // describe navigational properties here
}
export class OrderCreationClass {
  cartItems: number[];
  paymentMethod: PaymentMethodCreationClass;
  paymentApproved?: boolean;
}

export class OrderNowCreationClass{
  item: number;
  type: 'course' | 'shopItem' | 'globalTest'
  paymentMethod: PaymentMethodCreationClass;
  paymentApproved?: boolean;
}

export class OrderTestsNowCreationClass{
  globalTestsIds: number[];
  paymentMethod: PaymentMethodCreationClass;
  paymentApproved?: boolean;
}

export type OrderWithRelations = Order & OrderRelations;
