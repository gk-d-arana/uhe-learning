import { Entity, model, property } from '@loopback/repository';

@model({})
export class LessonAsset extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  lesson: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  description: string;

  @property({
    type: 'string',
    required: true,
  })
  fileUrl: string;

  @property({
    type: 'string',
    required: true,
  })
  type: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LessonAsset>) {
    super(data);
  }
}

export interface LessonAssetRelations {
  // describe navigational properties here
}

export class LessonAssetCreationCleass {
  lesson: number;
  name: string;
  description: string;
  fileUrl: string;
  type: 'pdf' | 'img' | 'doc';
}

export type LessonAssetWithRelations = LessonAsset & LessonAssetRelations;
