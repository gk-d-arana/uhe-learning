import {Entity, model, property} from '@loopback/repository';

@model({})
export class StaticData extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    default: "",
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  introDescription?: string;

  @property({
    type: 'string',
    default: "",
  })
  freeGuideFile?: string;

  @property({
    type: 'string',
    default: "",
  })
  introVideo?: string;

  @property({
    type: 'number',
    default: 0,
  })
  registeredEnrolls?: number;

  @property({
    type: 'number',
    default: 0,
  })
  finishedSessions?: number;

  @property({
    type: 'number',
    default: 0,
  })
  satisfactionRate?: number;

  @property({
    type: 'string',
    default: "",
  })
  adminImage?: string;


  @property({
    type: 'string',
    default: "",
    mysql: {
      dataType: 'LONGTEXT'
    }
  })
  adminDescription?: string;

  @property({
    type: 'number',
    default: 0,
  })
  yearsInService?: number;



  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<StaticData>) {
    super(data);
  }
}

export interface StaticDataRelations {
  // describe navigational properties here
}

export type StaticDataWithRelations = StaticData & StaticDataRelations;
