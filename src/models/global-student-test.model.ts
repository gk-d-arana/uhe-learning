import { Entity, model, property } from '@loopback/repository';

@model({})
export class GlobalStudentTest extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  globalTest: number;

  @property({
    type: 'boolean',
    default: false,
    required: false
  })
  isAnswered?: boolean;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    required: false,
  })
  updatedAt?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<GlobalStudentTest>) {
    super(data);
  }
}

export interface GlobalStudentTestRelations {
  // describe navigational properties here
}

export type GlobalStudentTestWithRelations = GlobalStudentTest & GlobalStudentTestRelations;
