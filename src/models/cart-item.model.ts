import {Entity, model, property} from '@loopback/repository';

@model({})
export class CartItem extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'number',
    required: true,
  })
  item: number;

  @property({
    type: 'string',
    required: true,
  })
  type: 'course' | 'shopItem' | 'globalTest';

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;

  @property({
    type: 'number',
    default: 1,
  })
  quantity: number;

  @property({
    type: 'boolean',
    default: true,
  })
  inCart?: boolean;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<CartItem>) {
    super(data);
  }
}

export interface CartItemRelations {
  // describe navigational properties here
}

export class CartItemClass {
  item: number;
  type: 'course' | 'shopItem' | 'globalTest';
  quantity: number;
  createdAt?: string;
  updatedAt: string;
  user?: number;
}

export type CartItemWithRelations = CartItem & CartItemRelations;
