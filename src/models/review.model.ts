import {Entity, model, property} from '@loopback/repository';

@model({})
export class Review extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  user: number;

  @property({
    type: 'number',
    required: true,
  })
  course: number;

  @property({
    type: 'number',
    required: true,
  })
  rating: number;

  @property({
    type: 'string',
    default: '',
  })
  textContent?: string;

  @property({
    type: 'string',
    default: '',
  })
  voiceContent?: string;

  @property({
    type: 'string',
    default: '',
  })
  videoContent?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  createdAt?: string;

  @property({
    type: 'date',
    default: `${new Date().toISOString()}`,
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Review>) {
    super(data);
  }
}

export interface ReviewRelations {
  // describe navigational properties here
}

export class ReviewObjectClass {
  id?: number;
  user?: number;
  course: number;
  rating: number;
  extension?: string;
  textContent?: string;
  voiceContent?: string;
  videoContent?: string;
  createdAt?: string;
  updatedAt?: string;
}

export type ReviewWithRelations = Review & ReviewRelations;
