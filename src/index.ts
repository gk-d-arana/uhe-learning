import {config as ENVCONFIG} from 'dotenv';
import {readFileSync} from 'fs-extra';
import path from 'path';
import {ApplicationConfig, UheApplication} from './application';
export * from './application';
ENVCONFIG({path: path.resolve(__dirname, '../.env'), debug: true})


export async function main(options: ApplicationConfig = {}) {
  const app = new UheApplication(options);
  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}

if (require.main === module) {
  // Run the application
  const config = {

    json: {limit: "5GB"},
    urlencoded: {limit: "5GB", extended: true},
    rest: {
      protocol: 'http',
      // protocol: 'https',
      // key: readFileSync('/etc/letsencrypt/live/uh-training.com/privkey.pem'),
      // cert: readFileSync('/etc/letsencrypt/live/uh-training.com/fullchain.pem'),
      port: +(process.env.PORT ?? 5000),
      host: process.env.HOST,
      // The `gracePeriodForClose` provides a graceful close for http/https
      // servers with keep-alive clients. The default value is `Infinity`
      // (don't force-close). If you want to immediately destroy all sockets
      // upon stop, set its value to `0`.
      // See https://www.npmjs.com/package/stoppable
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
      cors: {
        origin: '*',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        preflightContinue: false,
        optionsSuccessStatus: 204,
        credentials: true,
      },
    },
  };
  main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
