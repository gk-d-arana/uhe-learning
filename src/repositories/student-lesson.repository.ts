import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {StudentLesson, StudentLessonRelations} from '../models';

export class StudentLessonRepository extends DefaultCrudRepository<
  StudentLesson,
  typeof StudentLesson.prototype.id,
  StudentLessonRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(StudentLesson, dataSource);
  }
}
