import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {Category, CategoryRelations} from '../models';

export class CategoryRepository extends DefaultCrudRepository<
  Category,
  typeof Category.prototype.id,
  CategoryRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Category, dataSource);
  }

  async getTopCategories() {
    const res = await this.find({
      order: ['coursesCount DESC', 'coursesEnrollment DESC'],
      limit: 10,
    });
    return res;
  }
}
