// @ts-nocheck
import {inject} from '@loopback/core';
import {DefaultCrudRepository, repository, WhereBuilder} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {Instructor, InstructorRelations} from '../models';
import {UserRepository} from './user.repository';

export class InstructorRepository extends DefaultCrudRepository<
  Instructor,
  typeof Instructor.prototype.id,
  InstructorRelations
> {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Instructor, dataSource);
  }

  async getTopInstrcutors() {
    const res = await this.find({
      order: ['rating DESC', 'coursesEnrollment DESC'],
      limit: 10,
    });
    const response = [];
    for (const el of res) {
      const user = await this.userRepository.findById(el.user);
      const resInstructor = el;
      if(resInstructor.isBlocked && resInstructor.blockEndingLimit && new Date() >= new Date(resInstructor.blockEndingLimit)){
        resInstructor.isBlocked = false;
        await this.updateById(resInstructor.id, resInstructor);
      }
      resInstructor.user = user;
      response.push(resInstructor);
    }
    return response;
  }


  async getInstrcutors() {
    const res = await this.find({order: ['createdAt DESC', 'isReviewed ASC']});
    const response = [];
    for (const el of res) {
      try{
        const user = await this.userRepository.findById(el.user);
        const resInstructor = el;
        if(resInstructor.isBlocked && resInstructor.blockEndingLimit && new Date() >= new Date(resInstructor.blockEndingLimit)){
          resInstructor.isBlocked = false;
          await this.updateById(resInstructor.id, resInstructor);
        }
        resInstructor.user = user;
        response.push(resInstructor);
      }
      catch(error){
        console.log(error)
      }
    }
    return response;
  }

  async fetchByName(name: string){
    const res = [];
    const where = new WhereBuilder().or({firstName: {like: `%${name}%`}}, {lastName: {like: `%${name}%`}}).build();
    const users = await this.userRepository.find({where: where, order: ['createdAt DESC']});
    for (const user of users) {
      const instructor = await this.find({where: {user: user.id}})
      res.push({
        ...instructor[0],
        user: user
      });
    }
    return res;
  }
}
