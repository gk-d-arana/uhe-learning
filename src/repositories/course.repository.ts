// @ts-nocheck
import { inject } from '@loopback/core';
import {
  AnyObject,
  DefaultCrudRepository,
  FilterExcludingWhere,
  repository,
} from '@loopback/repository';
import { CourseResponse } from '../@types/responses';
import { generateImageFileFromBase64 } from '../common/generator';
import { isAdmin, studentIsEnrolled } from '../common/middleware';
import { UheLearningDataSource } from '../datasources';
import { Course, CourseRelations } from '../models';
import { CourseObjectClass } from '../models/course.model';
import { AnswerRepository } from './answer.repository';
import { AudioRepository } from './audio.repository';
import { CategoryRepository } from './category.repository';
import { ColumnAnswerRepository } from './column-answer.repository';
import { EnrollmentRepository } from './enrollment.repository';
import { GlobalStudentTestRepository } from './global-student-test.repository';
import { GlobalTestRepository } from './global-test.repository';
import { InstructorRepository } from './instructor.repository';
import { LessonAssetRepository } from './lesson-asset.repository';
import { LessonRepository } from './lesson.repository';
import { SectionRepository } from './section.repository';
import { StudentLessonRepository } from './student-lesson.repository';
import { StudentRepository } from './student.repository';
import { TestRepository } from './test.repository';
import { UserRepository } from './user.repository';
import { VideoRepository } from './video.repository';
import { deleteImage } from '../common';
import { courseDeleter } from '../common/deleters';

export class CourseRepository extends DefaultCrudRepository<
  Course,
  typeof Course.prototype.id,
  CourseRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
    @repository(CategoryRepository)
    public categoryRepository: CategoryRepository,
    @repository(StudentRepository)
    public studentRepository: StudentRepository,
    @repository(InstructorRepository)
    public instructorRepository: InstructorRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(SectionRepository)
    public sectionRepository: SectionRepository,
    @repository(LessonRepository)
    public lessonRepository: LessonRepository,
    @repository(VideoRepository)
    public videoRepository: VideoRepository,
    @repository(AudioRepository)
    public audioRepository: AudioRepository,
    @repository(LessonAssetRepository)
    public lessonAssetRepository: LessonAssetRepository,
    @repository(TestRepository)
    public testRepository: TestRepository,
    @repository(AnswerRepository)
    public answerRepository: AnswerRepository,
    @repository(ColumnAnswerRepository)
    public columnAnswerRepository: ColumnAnswerRepository,
    @repository(EnrollmentRepository)
    public enrollmentRepository: EnrollmentRepository,
    @repository(StudentLessonRepository)
    public studentLessonRepository: StudentLessonRepository,
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
    @repository(GlobalStudentTestRepository)
    public globalStudentTestRepository: GlobalStudentTestRepository,
  ) {
    super(Course, dataSource);
  }

  async create(
    entity: CourseObjectClass,
    options?: AnyObject | undefined,
  ): Promise<Course> {
    if (entity.courseImage) {
      entity.courseImage = await generateImageFileFromBase64(
        'courses/images',
        entity.courseImage,
        `${entity.name?.replaceAll(' ', '')}_course_image_` +
        Math.floor(Math.random() * 22424),
      );
    }
    if (entity.overviewImage) {
      entity.overviewImage = await generateImageFileFromBase64(
        'courses/overview_images',
        entity.overviewImage,
        `${entity.name?.replaceAll(' ', '')}_overview_image` +
        Math.floor(Math.random() * 22424),
      );
    }
    if (entity.descriptionVideo) {
      entity.descriptionVideo = await generateImageFileFromBase64(
        'courses/description_videos',
        entity.descriptionVideo,
        `${entity.name?.replaceAll(' ', '')}_description_video` +
        Math.floor(Math.random() * 22424),
      );
    }
    if (entity.descriptionImage) {
      entity.descriptionImage = await generateImageFileFromBase64(
        'courses/description_images',
        entity.descriptionImage,
        `${entity.name?.replaceAll(' ', '')}_description_image` +
        Math.floor(Math.random() * 22424),
      );
    }
    const creationDate = new Date();
    entity.createdAt = creationDate.toISOString();
    entity.updatedAt = creationDate.toISOString();
    const res = await super.create(entity);
    return res;
  }


  async updateCourse(
    course: Course,
    entity: CourseObjectClass,
    options?: AnyObject | undefined,
  ) {
    if (entity.courseImage?.indexOf('base64') != -1) {
      if (course.courseImage) {
        deleteImage(course.courseImage);
      }
      entity.courseImage = await generateImageFileFromBase64(
        'courses/images',
        entity.courseImage,
        `${entity.name?.replaceAll(' ', '')}_course_image_` +
        Math.floor(Math.random() * 22424),
      );
    } else {
      entity.courseImage = course.courseImage;
    }
    if (entity.overviewImage?.indexOf('base64') != -1) {
      if (course.overviewImage) {
        deleteImage(course.overviewImage);
      }
      entity.overviewImage = await generateImageFileFromBase64(
        'courses/overview_images',
        entity.overviewImage,
        `${entity.name?.replaceAll(' ', '')}_overview_image` +
        Math.floor(Math.random() * 22424),
      );
    }
    else {
      entity.overviewImage = course.overviewImage;
    }
    if (entity.descriptionVideo?.indexOf('base64') != -1) {
      if (course.descriptionVideo) {
        deleteImage(course.descriptionVideo);
      }
      entity.descriptionVideo = await generateImageFileFromBase64(
        'courses/description_videos',
        entity.descriptionVideo,
        `${entity.name?.replaceAll(' ', '')}_description_video` +
        Math.floor(Math.random() * 22424),
      );
    }
    else {
      entity.descriptionVideo = course.descriptionVideo;
    }
    if (entity.descriptionImage?.indexOf('base64') != -1) {
      if (course.descriptionImage) {
        deleteImage(course.descriptionImage);
      }
      entity.descriptionImage = await generateImageFileFromBase64(
        'courses/description_images',
        entity.descriptionImage,
        `${entity.name?.replaceAll(' ', '')}_description_image` +
        Math.floor(Math.random() * 22424),
      );
    }
    else {
      entity.descriptionImage = course.descriptionImage;
    }
    const creationDate = new Date();
    entity.createdAt = course.createdAt
    entity.updatedAt = creationDate.toISOString();
    await this.updateById(course.id, entity);
  }

  async deleteCourse(
    id: number,
  ) {
    const course = await this.findById(id);
    await courseDeleter(course,
      this,
      this.sectionRepository,
      this.lessonRepository,
      this.videoRepository,
      this.audioRepository,
      this.lessonAssetRepository,
      this.testRepository,
      this.globalTestRepository);
  }

  async getTopCourses() {
    const res = await this.find({
      order: ['rating DESC', 'enrollmentNumber DESC'],
      limit: 10,
    });
    return res;
  }

  getCoursesQueryBasedOnTime(valueFilter: string) {
    switch (valueFilter) {
      case 'today':
        return `createdAt BETWEEN NOW() - INTERVAL 1 DAY AND NOW()`;
      case 'month':
        return `createdAt BETWEEN NOW() - INTERVAL 30 DAY AND NOW()`;
      case 'year':
        return `createdAt BETWEEN NOW() - INTERVAL 365 DAY AND NOW()`;
      default:
        return '';
    }
  }

  async findViewCourseById(
    userId: number | undefined,
    id: number,
    filter?: FilterExcludingWhere<Course>,
  ) {
    const course = await this.findById(id);
    let enrolled = false;
    const userObj = await this.userRepository.findById(userId);
    let  student;
    if(userObj.role == 'student'){
      student = await this.studentRepository.findOne({ where: { user: userId } });
    }
    if (userId) {
      if (student) {
        enrolled = await studentIsEnrolled(userId, id, this.enrollmentRepository);
      } else {
        try {
          const adminUserId = await isAdmin(userId, this.userRepository);
          if (adminUserId) {
            enrolled = true;
          } else {
            enrolled = false;
          }
        } catch (error) {
          const instrcutorReq = await this.instructorRepository.find({ user: userId });
          if (instrcutorReq?.[0] && course.instructor == instrcutorReq[0].id) {
            enrolled = true;
          } else {
            enrolled = false;
          }
        }
      }
    }

    const instrcutor = await this.instructorRepository.find({ user: course.instructor });
    let user;
    if (instrcutor?.[0]) {
      let userOfInstructor = await this.userRepository.find({ id: instrcutor[0].user });
      if (userOfInstructor?.[0]) { }
      user = {
        ...instrcutor,
        user: userOfInstructor[0]
      }
    }
    const finalTransformedRes: CourseResponse = {
      id: course.id ?? 0,
      name: course.name ?? '',
      mobileDescription: course.mobileDescription ?? '',
      webDescription: course.webDescription ?? '',
      createdAt: course.createdAt ?? '',
      updatedAt: course.updatedAt ?? '',
      instructor: user ?? course.instructor,
      categories: [],
      courseImage: course.courseImage ?? '',
      price: course.price ?? 0,
      overviewImage: course.overviewImage ?? '',
      rating: course.rating ?? 0,
      descriptionVideo: course.descriptionVideo ?? '',
      descriptionImage: course.descriptionImage ?? '',
      enrollmentNumber: course.enrollmentNumber ?? 0,
      sections: [],
    };

    const res = await this.findById(id, filter);
    for (const categoryId of res.categories) {
      try {
        const cat = await this.categoryRepository.findById(categoryId);
        finalTransformedRes.categories.push(cat);
      } catch (err) {
        const courseWithoutCat = course;
        courseWithoutCat.categories = courseWithoutCat.categories.filter(
          el => el !== categoryId,
        );
        await this.updateById(id, courseWithoutCat);
        console.log(err);
      }
    }

    try {
      finalTransformedRes.instructor = await this.instructorRepository.findById(
        course.instructor,
      );
      if (finalTransformedRes.instructor) {
        finalTransformedRes.instructor.user =
          await this.userRepository.findById(
            finalTransformedRes.instructor.user,
          );
      }
    } catch (err) {
      console.log(err);
    }

    const sections = await this.sectionRepository.find({
      where: { course: course.id },
    });
    for (const section of sections) {
      const sectionRes = {
        id: section.id,
        name: section.name,
        description: section.description,
        createdAt: section.createdAt,
        updatedAt: section.updatedAt,
        isFree: section.isFree,
        lessons: [],
      };
      const lessons = await this.lessonRepository.find({
        where: { section: section.id },
      });
      for (const lesson of lessons) {
        const lessonRes = {
          id: lesson.id,
          createdAt: lesson.createdAt,
          updatedAt: lesson.updatedAt,
          type: lesson.type,
          isWatched: false
        };
        if (enrolled) {
          const studentLesson = await this.studentLessonRepository.findOne({
            where: { lesson: lesson.id, user: userId },
          });
          if (studentLesson) {
            lessonRes.isWatched = studentLesson.isWatched;
          }
        }
        if (lesson.type === 'video') {
          const videoRes = await this.videoRepository.find({
            where: { lesson: lesson.id },
          });
          if (videoRes?.[0]) {
            lessonRes.video = {
              id: videoRes[0].id,
              name: videoRes[0].name,
              description: videoRes[0].description,
            };
            if (section.isFree || enrolled) {
              lessonRes.video.videoUrl = videoRes[0].videoUrl;
              lessonRes.video.videoDuration = videoRes[0].videoDuration;
            } else {
              lessonRes.video.videoUrl = '';
              lessonRes.video.videoDuration = '';
            }
          }
        } else if (lesson.type === 'audio') {
          const audioRes = await this.audioRepository.find({
            where: { lesson: lesson.id },
          });
          lessonRes.audio = {
            id: audioRes[0].id,
            name: audioRes[0].name,
            description: audioRes[0].description,
          };
          if (section.isFree || enrolled) {
            lessonRes.audio.audioUrl = audioRes[0].audioUrl;
            lessonRes.audio.audioDuration = audioRes[0].audioDuration;
          } else {
            lessonRes.audio.audioUrl = '';
            lessonRes.audio.audioDuration = '';
          }
        } else if (lesson.type === 'asset') {
          const lessonAssetRes = await this.lessonAssetRepository.find({
            where: { lesson: lesson.id },
          });
          if (lessonAssetRes?.[0]) {
            lessonRes.lessonAsset = {
              id: lessonAssetRes[0].id,
              name: lessonAssetRes[0].name,
              description: lessonAssetRes[0].description,
            };
          } else {
            lessonRes.lessonAsset = {};
          }
          if (section.isFree || enrolled) {
            lessonRes.lessonAsset.fileUrl = lessonAssetRes[0].fileUrl;
            lessonRes.lessonAsset.type = lessonAssetRes[0].type;
          } else {
            lessonRes.lessonAsset.fileUrl = '';
            lessonRes.lessonAsset.type = '';
          }
        } else if (lesson.type === 'test') {
          const globalTest = await this.globalTestRepository.find({
            where: { course: course.id, lesson: lesson.id },
          });
          if (globalTest?.[0] !== undefined) {
            delete globalTest[0].course;
            delete globalTest[0].user;
            lessonRes.testData = globalTest[0];
            const globalStudentTestInstance = await this.globalStudentTestRepository.find({ where: { globalTest: globalTest[0].id, user: userId } });
            if (globalStudentTestInstance?.[0]) {
              //@ts-ignore
              lessonRes.testData['globalStudentTest'] = globalStudentTestInstance[0].id;
            }
            const tests = await this.testRepository.find({
              where: {
                globalTest: globalTest[0].id,
              },
            });
            const resTests = [];
            if (section.isFree || enrolled) {
              for (const test of tests) {
                if (test.type === 'written') {
                  resTests.push({
                    id: test.id,
                    name: test.name,
                    question: test.question,
                    type: test.type,
                  });
                } else if (test.type === 'optional') {
                  const testOptional = {
                    id: test.id,
                    name: test.name,
                    question: test.question,
                    type: test.type,
                    answers: [],
                  };

                  const answers = await this.answerRepository.find({
                    where: { test: test.id },
                  });
                  for (const answer of answers) {
                    testOptional.answers.push({
                      id: answer.id,
                      value: answer.value,
                      isTrue: answer.isTrue,
                    });
                  }
                  resTests.push(testOptional);
                } else if (test.type === 'match') {
                  const testMatch = {
                    id: test.id,
                    name: test.name,
                    question: test.question,
                    type: test.type,
                    columnAnswers: [],
                  };

                  const columnAnswers = await this.columnAnswerRepository.find({
                    where: { test: test.id },
                  });
                  for (const columnAnswer of columnAnswers) {
                    testMatch.columnAnswers.push({
                      id: columnAnswer.id,
                      value: columnAnswer.value,
                      oppositeValue: columnAnswer.oppositeValue,
                    });
                  }
                  resTests.push(testMatch);
                }
              }
              lessonRes.tests = resTests;
            } else {
              lessonRes.tests = resTests.length;
            }
          }
        }
        sectionRes.lessons.push(lessonRes);
      }
      finalTransformedRes.sections.push(sectionRes);
    }
    let progress = 0;
    if (enrolled) {
      const studentLessons = await this.studentLessonRepository.find({ where: { course: course.id, user: userId ?? 0 } });
      const watchedLessons = studentLessons.filter(el => el.isWatched == true);
      progress = Math.floor((100 * watchedLessons.length) / studentLessons.length);
    }
    return { course: finalTransformedRes, enrolled: enrolled, progress: progress };
  }
}
