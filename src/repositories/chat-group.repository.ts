import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {ChatGroup, ChatGroupRelations} from '../models';

export class ChatGroupRepository extends DefaultCrudRepository<
  ChatGroup,
  typeof ChatGroup.prototype.id,
  ChatGroupRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(ChatGroup, dataSource);
  }
}
