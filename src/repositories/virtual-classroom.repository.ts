import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {VirtualClassroom, VirtualClassroomRelations} from '../models';

export class VirtualClassroomRepository extends DefaultCrudRepository<
  VirtualClassroom,
  typeof VirtualClassroom.prototype.id,
  VirtualClassroomRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(VirtualClassroom, dataSource);
  }

  

}
