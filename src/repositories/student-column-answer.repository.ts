import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {StudentColumnAnswer, StudentColumnAnswerRelations} from '../models';

export class StudentColumnAnswerRepository extends DefaultCrudRepository<
  StudentColumnAnswer,
  typeof StudentColumnAnswer.prototype.id,
  StudentColumnAnswerRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(StudentColumnAnswer, dataSource);
  }
}
