import {inject} from '@loopback/core';
import {AnyObject, DefaultCrudRepository} from '@loopback/repository';
import {generateImageFileFromBase64} from '../common/generator';
import {UheLearningDataSource} from '../datasources';
import {Video, VideoRelations} from '../models';
import {VideoObjectClass} from '../models/video.model';
import { deleteImage } from '../common';

export class VideoRepository extends DefaultCrudRepository<
  Video,
  typeof Video.prototype.id,
  VideoRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Video, dataSource);
  }

  async create(
    entity: VideoObjectClass,
    options?: AnyObject | undefined,
  ): Promise<Video> {
    if (entity.videoUrl) {
      entity.videoUrl = await generateImageFileFromBase64(
        'courses/videos',
        entity.videoUrl,
        `${entity.name?.replaceAll(' ', '')}_video_url` +
          Math.floor(Math.random() * 22424),
          entity.extension
      );
    }
    delete entity.extension;
    const res = await super.create(entity);
    return res;
  }

  async updateVideo(
    video: Video,
    entity: VideoObjectClass,
    options?: AnyObject | undefined,
  ) {
    if (entity.videoUrl.indexOf('base64') != -1) {
      if(video.videoUrl){
        await deleteImage(video.videoUrl);
      }
      entity.videoUrl = await generateImageFileFromBase64(
        'courses/videos',
        entity.videoUrl,
        `${entity.name?.replaceAll(' ', '')}_video_url` +
          Math.floor(Math.random() * 22424),
          entity.extension
      );
    }else{
      entity.videoUrl = video.videoUrl;
    }
    delete entity.extension;
    await this.updateById(video.id, entity);
  }

}
