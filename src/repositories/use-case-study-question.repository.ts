import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {UseCaseStudyQuestion, UseCaseStudyQuestionRelations} from '../models';

export class UseCaseStudyQuestionRepository extends DefaultCrudRepository<
  UseCaseStudyQuestion,
  typeof UseCaseStudyQuestion.prototype.id,
  UseCaseStudyQuestionRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(UseCaseStudyQuestion, dataSource);
  }
}
