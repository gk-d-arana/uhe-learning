import {inject} from '@loopback/core';
import {DefaultCrudRepository, repository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {generateImageFileFromBase64} from '../common/generator';
import {studentIsEnrolled} from '../common/middleware';
import {UheLearningDataSource} from '../datasources';
import {Review, ReviewRelations} from '../models';
import {ReviewObjectClass} from '../models/review.model';
import {CourseReview} from '../types/responses';
import {EnrollmentRepository} from './enrollment.repository';
import {UserRepository} from './user.repository';

export class ReviewRepository extends DefaultCrudRepository<
  Review,
  typeof Review.prototype.id,
  ReviewRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
    @repository(EnrollmentRepository)
    public enrollmentRepository: EnrollmentRepository,
  ) {
    super(Review, dataSource);
  }

  async findCourseReviews(courseId: number, userRepository: UserRepository): Promise<CourseReview[]> {
    const reviews = await this.find({where: {course: courseId}});
    const reviewRes = [];
    for (const review of reviews) {
      const user = await userRepository.find({where: {id: review.user}});
      if (user?.[0]) {
        reviewRes.push({
          id: review.id,
          user: {
            id: user[0].id,
            firstName: user[0].firstName,
            lastName: user[0].lastName,
            profileImage: user[0].profileImage
          },
          rating: review.rating,
          textContent: review.textContent,
          voiceContent: review.voiceContent,
          videoContent: review.videoContent,
          createdAt: review.createdAt,
          updatedAt: review.updatedAt
        })
      }
    }
    return reviewRes;
  }

  async addReview(entity: ReviewObjectClass, userId: number | undefined) {
    if (!await studentIsEnrolled(userId, entity.course, this.enrollmentRepository)) {
      throw new HttpErrors.Unauthorized(
        'Must be enrolled to add your review on this course',
      );
      return;
    };
    const creationDate = new Date();
    entity.createdAt = creationDate.toISOString();
    entity.updatedAt = creationDate.toISOString();
    entity.user = userId;

    if (entity.videoContent) {

      entity.videoContent = await generateImageFileFromBase64(
        'reviews/videos',
        entity.videoContent,
        `review_video` + Math.floor(Math.random() * 22424),
      );
    }
    if (entity.voiceContent) {
      entity.voiceContent = await generateImageFileFromBase64(
        'reviews/voices',
        entity.voiceContent,
        `review_voice` + Math.floor(Math.random() * 22424),
      );
    }

    const res = await this.create(entity);
    return res;
  }

  async updateComment(entity: ReviewObjectClass) {
    const creationDate = new Date();
    entity.updatedAt = creationDate.toISOString();
    if (entity.videoContent) {
      entity.videoContent = await generateImageFileFromBase64(
        'reviews/videos',
        entity.videoContent,
        `review_video` + Math.floor(Math.random() * 22424),
      );
      entity.voiceContent = '';
      entity.textContent = '';
    }
    if (entity.voiceContent) {
      entity.voiceContent = await generateImageFileFromBase64(
        'reviews/voices',
        entity.voiceContent,
        `review_voice` + Math.floor(Math.random() * 22424),
      );
      entity.videoContent = '';
      entity.textContent = '';
    } else {
      entity.voiceContent = '';
      entity.videoContent = '';
    }
    await this.updateById(entity.id, entity);
    return 'success'
  }
}
