import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {Lesson, LessonRelations} from '../models';

export class LessonRepository extends DefaultCrudRepository<
  Lesson,
  typeof Lesson.prototype.id,
  LessonRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Lesson, dataSource);
  }
}
