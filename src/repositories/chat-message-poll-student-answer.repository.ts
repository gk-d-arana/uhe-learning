import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {ChatMessagePollStudentAnswer, ChatMessagePollStudentAnswerRelations} from '../models';

export class ChatMessagePollStudentAnswerRepository extends DefaultCrudRepository<
  ChatMessagePollStudentAnswer,
  typeof ChatMessagePollStudentAnswer.prototype.id,
  ChatMessagePollStudentAnswerRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(ChatMessagePollStudentAnswer, dataSource);
  }
}
