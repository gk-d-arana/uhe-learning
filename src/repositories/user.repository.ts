import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {createHash} from 'crypto';
import {AES} from 'crypto-js';
import {sign} from 'jsonwebtoken';
import {generateString} from '../common';
import {UheLearningDataSource} from '../datasources';
import {User, UserRelations} from '../models';

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(User, dataSource);
  }

  async checkAuth(email: string, password: string) {
    const user = await this.findOne({where: {email: email}});
    if (!user) {
      throw new HttpErrors.Unauthorized('Invalid credentials');
      return;
    }

    const hashedPassword = createHash('sha256').update(password).digest('hex');
    if (user.password === hashedPassword) {
      const secretCodeGen = generateString(25);
      const encryptedAES = AES.encrypt(
        secretCodeGen,
        process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
      );
      user.secretCode = secretCodeGen;
      await this.updateById(user.id, user);
      return {
        user,
        jwt: sign(
          {
            email: user.email,
            role: user.role,
            secretCode: encryptedAES.toString(),
          },
          process.env.JWT_SECRET ?? 'shourySecret',
        ),
      };
    } else {
      throw new HttpErrors.Unauthorized('Invalid credentials');
    }
  }

  async registerJWT(email: string, hashedPassword: string, role: string, isNew:boolean) {
    const user = await this.findOne({where: {email: email}});
    if (user && isNew) {
      throw new HttpErrors.Conflict('Email already exists');
    }
    const secretCodeGen = generateString(25);
    const encryptedAES = AES.encrypt(
      secretCodeGen,
      process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
    );
    const jwt = sign(
      {
        email: email,
        role: role,
        secretCode: encryptedAES.toString(),
      },
      process.env.JWT_SECRET ?? 'shourySecret',
    );
    return {jwt, secretCodeGen};
  }
}
