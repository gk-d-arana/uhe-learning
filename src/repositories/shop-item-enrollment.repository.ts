import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {ShopItemEnrollment, ShopItemEnrollmentRelations} from '../models';

export class ShopItemEnrollmentRepository extends DefaultCrudRepository<
  ShopItemEnrollment,
  typeof ShopItemEnrollment.prototype.id,
  ShopItemEnrollmentRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(ShopItemEnrollment, dataSource);
  }
}
