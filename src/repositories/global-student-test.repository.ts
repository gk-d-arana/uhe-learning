import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {GlobalStudentTest, GlobalStudentTestRelations} from '../models';

export class GlobalStudentTestRepository extends DefaultCrudRepository<
  GlobalStudentTest,
  typeof GlobalStudentTest.prototype.id,
  GlobalStudentTestRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(GlobalStudentTest, dataSource);
  }
}
