import {inject} from '@loopback/core';
import {
  AnyObject,
  DefaultCrudRepository,
  repository,
} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {UheLearningDataSource} from '../datasources';
import {Answer, AnswerRelations} from '../models';
import {AnswerCreationClass} from '../models/answer.model';
import {TestRepository} from './test.repository';

export class AnswerRepository extends DefaultCrudRepository<
  Answer,
  typeof Answer.prototype.id,
  AnswerRelations
> {
  constructor(
    @repository(TestRepository) public testRepository: TestRepository,
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Answer, dataSource);
  }

  async createOnCondition(
    entity: AnswerCreationClass,
    options?: AnyObject | undefined,
  ): Promise<Answer> {
    const test = await this.testRepository.findById(entity.test);
    if (test.type === 'optional') {
      return super.create(entity);
    } else {
      throw new HttpErrors.BadRequest('Test must be optional to add answers');
    }
  }
}
