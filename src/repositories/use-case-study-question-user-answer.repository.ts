import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {UseCaseStudyQuestionUserAnswer, UseCaseStudyQuestionUserAnswerRelations} from '../models';

export class UseCaseStudyQuestionUserAnswerRepository extends DefaultCrudRepository<
  UseCaseStudyQuestionUserAnswer,
  typeof UseCaseStudyQuestionUserAnswer.prototype.id,
  UseCaseStudyQuestionUserAnswerRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(UseCaseStudyQuestionUserAnswer, dataSource);
  }
}
