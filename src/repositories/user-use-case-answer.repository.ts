import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {UserUseCaseAnswer, UserUseCaseAnswerRelations} from '../models';

export class UserUseCaseAnswerRepository extends DefaultCrudRepository<
  UserUseCaseAnswer,
  typeof UserUseCaseAnswer.prototype.id,
  UserUseCaseAnswerRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(UserUseCaseAnswer, dataSource);
  }
}
