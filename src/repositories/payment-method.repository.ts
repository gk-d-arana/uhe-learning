import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {PaymentMethod, PaymentMethodRelations} from '../models';

export class PaymentMethodRepository extends DefaultCrudRepository<
  PaymentMethod,
  typeof PaymentMethod.prototype.id,
  PaymentMethodRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(PaymentMethod, dataSource);
  }
}
