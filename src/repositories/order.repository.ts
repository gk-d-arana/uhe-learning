import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {repository} from '@loopback/repository/dist/decorators';
import {HttpErrors} from '@loopback/rest';
import {generateImageFileFromBase64} from '../common';
import {UheLearningDataSource} from '../datasources';
import {Order, OrderRelations} from '../models';
import {OrderCreationClass, OrderNowCreationClass, OrderTestsNowCreationClass} from '../models/order.model';
import {CardPaymentRepository} from './card-payment.repository';
import {CartItemRepository} from './cart-item.repository';
import {CourseRepository} from './course.repository';
import {EnrollmentRepository} from './enrollment.repository';
import {GlobalPaymentRepository} from './global-payment.repository';
import {GlobalStudentTestRepository} from './global-student-test.repository';
import {GlobalTestRepository} from './global-test.repository';
import {LessonRepository} from './lesson.repository';
import {LocalPaymentRepository} from './local-payment.repository';
import {PaymentMethodRepository} from './payment-method.repository';
import {SectionRepository} from './section.repository';
import {ShopItemEnrollmentRepository} from './shop-item-enrollment.repository';
import {StudentLessonRepository} from './student-lesson.repository';
import {TransferPaymentRepository} from './transfer-payment.repository';

export class OrderRepository extends DefaultCrudRepository<
  Order,
  typeof Order.prototype.id,
  OrderRelations
> {
  constructor(
    @repository(PaymentMethodRepository)
    public paymentMethodRepository: PaymentMethodRepository,

    @repository(GlobalPaymentRepository)
    public globalPaymentRepository: GlobalPaymentRepository,

    @repository(ShopItemEnrollmentRepository)
    public shopItemEnrollmentRepository: ShopItemEnrollmentRepository,

    @repository(LocalPaymentRepository)
    public localPaymentRepository: LocalPaymentRepository,

    @repository(TransferPaymentRepository)
    public transferPaymentRepository: TransferPaymentRepository,

    @repository(GlobalStudentTestRepository)
    public globalStudentTestRepository: GlobalStudentTestRepository,

    @repository(CardPaymentRepository)
    public cardPaymentRepository: CardPaymentRepository,

    @repository(CartItemRepository)
    public cartItemRepository: CartItemRepository,

    @repository(CourseRepository)
    public courseRepository: CourseRepository,

    @repository(SectionRepository)
    public sectionRepository: SectionRepository,

    @repository(LessonRepository)
    public lessonRepository: LessonRepository,

    @repository(StudentLessonRepository)
    public studentLessonRepository: StudentLessonRepository,

    @repository(EnrollmentRepository)
    public enrollmentRepository: EnrollmentRepository,

    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Order, dataSource);
  }

  checkout = async (order: OrderCreationClass, userId: number) => {
    let globalPayment = 0;
    let localPayment = 0;
    let cardPayment = 0;
    let transferPayment = 0;
    const cartItems = [];
    const approvement =
      order.paymentApproved &&
      (order.paymentMethod.globalPayment?.isPaypal ||
        order.paymentMethod.globalPayment?.cardPayment != null);
    for (const cartItem of order.cartItems) {
      try {
        const cartItemObj = await this.cartItemRepository.find({
          where: {
            id: cartItem,
            user: userId,
          },
        });
        if (cartItemObj.length > 0) {
          cartItems.push(cartItemObj[0]);
        } else {
          throw new HttpErrors.NotFound('Cart Item Not Found');
          return;
        }
      } catch (err) {
        throw new HttpErrors.NotFound('Cart Item Not Found');
        return;
      }
    }
    if (order.paymentMethod.globalPayment) {
      if (order.paymentMethod.globalPayment.cardPayment) {
        const cardPaymentObj = await this.cardPaymentRepository.create(
          order.paymentMethod.globalPayment.cardPayment,
        );
        cardPayment = cardPaymentObj.id ?? 0;
      } else if (order.paymentMethod.globalPayment.transferPayment) {
        order.paymentMethod.globalPayment.transferPayment.proofImage =
          await generateImageFileFromBase64('orders', order.paymentMethod.globalPayment.transferPayment.proofImage, 'proof' + (new Date()).toISOString());
        const transferPaymentObj = await this.transferPaymentRepository.create(
          order.paymentMethod.globalPayment.transferPayment,
        );
        transferPayment = transferPaymentObj.id ?? 0;
      }
      const globalPaymentObj = await this.globalPaymentRepository.create({
        isPaypal: order.paymentMethod.globalPayment.isPaypal,
        cardPayment: cardPayment,
        transferPayment: transferPayment,
      });
      globalPayment = globalPaymentObj.id ?? 0;
    } else {
      if (order.paymentMethod.localPayment?.transferPayment) {
        order.paymentMethod.localPayment.transferPayment.proofImage =
          await generateImageFileFromBase64('orders', order.paymentMethod.localPayment?.transferPayment.proofImage, 'proof' + (new Date()).toISOString());
        const transferPaymentObj = await this.transferPaymentRepository.create(
          order.paymentMethod.localPayment.transferPayment,
        );
        transferPayment = transferPaymentObj.id ?? 0;
      }
      const localPaymentObj = await this.localPaymentRepository.create({
        isEpay: order.paymentMethod.localPayment?.isEpay,
        transferPayment: transferPayment,
      });
      localPayment = localPaymentObj.id ?? 0;
    }
    const paymentMethodObj = await this.paymentMethodRepository.create({
      globalPayment: globalPayment,
      localPayment: localPayment,
    });
    const orderObjResponse = this.create({
      user: userId,
      cartItems: order.cartItems,
      paymentMethod: paymentMethodObj.id ?? 0,
      paymentApproved: approvement,
    });
    for (const cartItem of cartItems) {
      cartItem.inCart = false;
      await this.cartItemRepository.updateById(cartItem.id, cartItem);
      if (approvement) {
        if (cartItem.type == 'course') {
          const enrollment = await this.enrollmentRepository.create({
            user: userId,
            course: cartItem.item,
          });
          if (enrollment) {
            const sections = await this.sectionRepository.find({
              where: {course: cartItem.item},
            });
            for (const section of sections) {
              const lessons = await this.lessonRepository.find({
                where: {section: section.id},
              });
              for (const lesson of lessons) {
                await this.studentLessonRepository.create({
                  user: userId,
                  lesson: lesson.id,
                  section: section.id,
                  course: cartItem.item,
                  createdAt: `${new Date().toISOString()}`,
                  updatedAt: `${new Date().toISOString()}`,
                });
              }
            }
          }
        }
        else if (cartItem.type == 'globalTest') {
          await this.globalStudentTestRepository.create({
            user: userId,
            globalTest: cartItem.item
          })
        }
        else if (cartItem.type == 'shopItem') {
          await this.shopItemEnrollmentRepository.create({
            user: userId,
            shopItem: cartItem.item
          })
        }
      }
    }
    return orderObjResponse;
  };

  buyNow = async (
    orderNowCreationClass: OrderNowCreationClass,
    userId: number,
  ) => {
    let cartItem = await this.cartItemRepository.findOne({
      where: {
        item: orderNowCreationClass.item,
        type: orderNowCreationClass.type,
        user: userId
      }
    })
    if (cartItem) {
      cartItem.updatedAt = new Date().toISOString();
      cartItem.quantity = 1;
      await this.cartItemRepository.updateById(cartItem.id, cartItem);
    } else {
      cartItem = await this.cartItemRepository.create({
        item: orderNowCreationClass.item,
        type: orderNowCreationClass.type,
        createdAt: new Date().toISOString(),
        quantity: 1,
        updatedAt: new Date().toISOString(),
        user: userId,
      });
    }
    const order: OrderCreationClass = {
      paymentApproved: orderNowCreationClass.paymentApproved,
      paymentMethod: orderNowCreationClass.paymentMethod,
      cartItems: [cartItem.id ?? 0]
    };
    const res = await this.checkout(order, userId);
    return res;
  };

  buyTestsNow = async (
    orderNowCreationClass: OrderTestsNowCreationClass,
    userId: number,
    globalTestRepository: GlobalTestRepository,
    globalStudentTestRepository: GlobalStudentTestRepository
  ) => {

    let cartItems = [];
    for (const globalTestId of orderNowCreationClass.globalTestsIds) {
      let globalTest = await globalTestRepository.findById(globalTestId);
      cartItems.push((await this.cartItemRepository.create({
        item: globalTest.id,
        type: 'globalTest',
        quantity: 1,
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
        user: userId,
      })).id ?? 0);
      if(orderNowCreationClass.paymentApproved){
        await globalStudentTestRepository.create({
          user: userId,
          globalTest: globalTestId
        });
      }
    }

    const order: OrderCreationClass = {
      paymentApproved: orderNowCreationClass.paymentApproved,
      paymentMethod: orderNowCreationClass.paymentMethod,
      cartItems: cartItems
    };
    const res = await this.checkout(order, userId);
    return res;
  };
}
