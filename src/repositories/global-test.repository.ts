import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {GlobalTest, GlobalTestRelations} from '../models';

export class GlobalTestRepository extends DefaultCrudRepository<
  GlobalTest,
  typeof GlobalTest.prototype.id,
  GlobalTestRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(GlobalTest, dataSource);
  }
}
