import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {LiveStream, LiveStreamRelations} from '../models';

export class LiveStreamRepository extends DefaultCrudRepository<
  LiveStream,
  typeof LiveStream.prototype.id,
  LiveStreamRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(LiveStream, dataSource);
  }

  getHomeLiveStreams = async () => {
    const todayDate = new Date();
    const tomorrowDate = new Date(
      new Date().setDate(todayDate.getDate() + 1),
    ).toISOString();
    const yesterdayDate = new Date(
      new Date().setDate(todayDate.getDate() - 1),
    ).toISOString();
    const adminLiveStreams = await this.find({where:{isFromAdmin:true}});
      const liveStreams = await this.find({
        where: {
          or: [
            {
              and: [
                {isLive: true},
                {isFromAdmin: false}
              ]
            },
            {
              and: [
                {startDate: {gte: yesterdayDate}},
                {startDate: {lte: tomorrowDate}},
                {isFromAdmin: false}
              ],
            },
          ],
        },
        limit: 10 - adminLiveStreams.length
      });
    return [...adminLiveStreams.concat(liveStreams)];
  };



}
