import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {Value, ValueRelations} from '../models';

export class ValueRepository extends DefaultCrudRepository<
  Value,
  typeof Value.prototype.id,
  ValueRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Value, dataSource);
  }
}
