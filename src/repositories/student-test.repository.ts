/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/prefer-for-of */
// @ts-nocheck
import { inject } from '@loopback/core';
import { DefaultCrudRepository } from '@loopback/repository';
import { repository } from '@loopback/repository/dist/decorators/repository.decorator';
import { HttpErrors } from '@loopback/rest';
import { UheLearningDataSource } from '../datasources';
import { StudentTest, StudentTestRelations } from '../models';
import { StudentTestCreation } from '../types/requests';
import { TestListResponse } from '../types/responses';
import { AnswerRepository } from './answer.repository';
import { ColumnAnswerRepository } from './column-answer.repository';
import { CourseRepository } from './course.repository';
import { GlobalStudentTestRepository } from './global-student-test.repository';
import { GlobalTestRepository } from './global-test.repository';
import { StudentColumnAnswerRepository } from './student-column-answer.repository';
import { StudentOptionalAnswerRepository } from './student-optional-answer.repository';
import { TestRepository } from './test.repository';
import { WrittenTestAnswerRepository } from './written-test-answer.repository';

export class StudentTestRepository extends DefaultCrudRepository<
  StudentTest,
  typeof StudentTest.prototype.id,
  StudentTestRelations
> {
  constructor(
    @repository(CourseRepository)
    public courseRepository: CourseRepository,
    @repository(GlobalTestRepository)
    public globalTestRepository: GlobalTestRepository,
    @repository(GlobalStudentTestRepository)
    public globalStudentTestRepository: GlobalStudentTestRepository,
    @repository(TestRepository) public testRepository: TestRepository,
    @repository(StudentColumnAnswerRepository)
    public studentColumnAnswerRepository: StudentColumnAnswerRepository,
    @repository(StudentOptionalAnswerRepository)
    public studentOptionalAnswerRepository: StudentOptionalAnswerRepository,
    @repository(AnswerRepository)
    public answerRepository: AnswerRepository,
    @repository(ColumnAnswerRepository)
    public columnAnswerRepository: ColumnAnswerRepository,
    @repository(WrittenTestAnswerRepository)
    public writtenTestAnswerRepository: WrittenTestAnswerRepository,

    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(StudentTest, dataSource);
  }

  createMyTest = async (
    studentTest: StudentTestCreation,
  ): Promise<{ message: string; id: number | undefined }> => {
    let test;
    try {
      const globalStudentTest = await this.globalStudentTestRepository.findById(
        studentTest.globalStudentTestId,
      );
      test = await this.testRepository.findById(studentTest.test);
      if (globalStudentTest && test) {
        if (!globalStudentTest.isAnswered) {
          globalStudentTest.isAnswered = true;
          globalStudentTest.updatedAt = `${new Date().toISOString()}`;
          await this.globalStudentTestRepository.updateById(studentTest.globalStudentTestId, globalStudentTest);
        }
        let studentTestCreated;
        studentTestCreated = await this.findOne({
          where: {
            globalStudentTest: globalStudentTest.id,
            test: studentTest.test,
          }
        })
        if(!studentTestCreated)
        studentTestCreated = await this.create({
          globalStudentTest: globalStudentTest.id,
          test: studentTest.test,
        });
        switch (test.type) {
          case 'match':
            if (studentTest.columnAnswers) {
              for (let i = 0; i < studentTest.columnAnswers.length; i++) {
                await this.studentColumnAnswerRepository.create({
                  studentTest: studentTestCreated.id,
                  value: studentTest.columnAnswers[i].value,
                  oppositeValue: studentTest.columnAnswers[i].oppositeValue,
                  isTrue: studentTest.columnAnswers[i].isTrue,
                });
              }
            }
            break;
          case 'optional':
            if (studentTest.optionalAnswer) {
              await this.studentOptionalAnswerRepository.create({
                studentTest: studentTestCreated.id,
                answerId: studentTest.optionalAnswer.answerId,
                isTrue: studentTest.optionalAnswer.isTrue,
              });
            }
            break;
          case 'written':
            if (studentTest.writtenAnswer) {
              console.log('in')
              await this.writtenTestAnswerRepository.create({
                studentTest: studentTestCreated.id,
                value: studentTest.writtenAnswer.value,
              });
            }
            break;
          default:
            break;
        }
        return { message: 'success', id: studentTestCreated.id };
      } else {
        throw new HttpErrors['404']();
      }
    } catch (err) {
      throw new HttpErrors['404']();
    }
  };

  getTests = async (
    userId: number | undefined,
  ): Promise<TestListResponse[]> => {
    const myGlobalTests = await this.globalStudentTestRepository.find({
      where: { user: userId },
    });
    const resTests: TestListResponse[] | PromiseLike<TestListResponse[]> = [];
    for (const globalTest of myGlobalTests) {
      const originalGlobalTest = await this.globalTestRepository.findById(
        globalTest.globalTest,
      );
      let testName = 'Test ' + originalGlobalTest.name;
      if (originalGlobalTest.course !== 0) {
        const course = await this.courseRepository.findById(
          originalGlobalTest.course,
        );
        if (course) {
          testName += ` ,Course ${course.name}`;
        }
      }
      resTests.push({
        id: globalTest.id,
        name: testName,
        createdAt: globalTest.createdAt ?? new Date().toISOString(),
      });
    }
    return resTests;
  };

  getTestSolves = async (
    id: number | undefined,
    userId: number | undefined,
  ): Promise<any> => {
    const resTests = [];
    const globalStudentTest = await this.globalStudentTestRepository.find({
      where: { user: userId, id: id },
    });
    if (globalStudentTest?.[0]) {
      const tests = await this.testRepository.find({
        where: {
          globalTest: globalStudentTest[0].globalTest
        }
      });

      for (const test of tests) {
        const studentTest = await this.findOne({
          where: { globalStudentTest: id, test: test.id },
        });
        if (test.type === 'written') {
          const testResObj: any = {
            id: test.id,
            studentTestId: studentTest ? studentTest.id : null,
            name: test.name,
            testId: test.id,
            question: test.question,
            type: test.type,
            isChecked: studentTest ? studentTest.isChecked : false,
          }
          const writtenAnswer: any = await this.writtenTestAnswerRepository.findOne({ where: { studentTest: studentTest ? studentTest.id : 0 } })
          if (writtenAnswer) {
            delete writtenAnswer.studentTest;
            testResObj.studentAnswer = writtenAnswer;
          } else {
            testResObj.studentAnswer = null;
          }
          resTests.push(testResObj);
        } else if (test.type === 'optional') {
          const testOptional = {
            id: test.id,
            studentTestId: studentTest ? studentTest.id : null,
            name: test.name,
            testId: test.id,
            question: test.question,
            type: test.type,
            isChecked: studentTest ? studentTest.isChecked : false,
            answers: [],
          };
          const answers = await this.answerRepository.find({
            where: { test: test.id },
          });
          for (const answer of answers) {
            testOptional.answers.push({
              id: answer.id,
              value: answer.value,
              isTrue: answer.isTrue,
            });
          }

          const optionalAnswer = await this.studentOptionalAnswerRepository.findOne({ where: { studentTest: studentTest ? studentTest.id : 0 } })
          if (optionalAnswer) {
            delete optionalAnswer.studentTest;
            testOptional.studentAnswer = optionalAnswer;
          } else {
            testOptional.studentAnswer = null;
          }
          resTests.push(testOptional);
        } else if (test.type === 'match') {
          const testMatch = {
            id: test.id,
            studentTestId: studentTest ? studentTest.id : null,
            name: test.name,
            testId: test.id,
            question: test.question,
            type: test.type,
            isChecked: studentTest ? studentTest.isChecked : false,
            columnAnswers: [],
            studentAnswers: []
          };

          const columnAnswers = await this.columnAnswerRepository.find({
            where: { test: test.id },
          });
          for (const columnAnswer of columnAnswers) {
            testMatch.columnAnswers.push({
              id: columnAnswer.id,
              value: columnAnswer.value,
              oppositeValue: columnAnswer.oppositeValue,
            });
          }
          const matchAnswers = await this.studentColumnAnswerRepository.find({ where: { studentTest: studentTest ? studentTest.id : 0 } })
          if (matchAnswers?.length > 0) {
            for (const matchAnswer of matchAnswers) {
              delete matchAnswer.studentTest;
              testMatch.studentAnswers.push(matchAnswer)
            }
          } else {
            testMatch.studentAnswers = null;
          }
          resTests.push(testMatch);
        }
      }
      return resTests;
    } else {
      return [];
    }
  };

  updateTestCheck = async (id: number, data: { isTrue: boolean }): Promise<{ message: string; }> => {
    const studentTest = await this.findById(id);

    if (studentTest) {
      studentTest.isChecked = true;
      await this.updateById(id, studentTest);
      const writtenAnswer = await this.writtenTestAnswerRepository.find({ where: { studentTest: id } })
      if (writtenAnswer?.[0]) {
        writtenAnswer[0].isTrue = data.isTrue;
        await this.writtenTestAnswerRepository.updateById(writtenAnswer[0].id, writtenAnswer[0]);
        return { message: "success" }
      }
      return { message: "success" }
    } else {
      throw new HttpErrors['404'];
    }
  }
}
