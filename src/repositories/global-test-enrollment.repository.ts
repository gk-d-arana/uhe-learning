import {inject} from '@loopback/core';
import {DefaultCrudRepository, repository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {GlobalTestEnrollment, GlobalTestEnrollmentRelations} from '../models';
import {GlobalTestEnrollmentResponse} from '../types/responses';
import {GlobalTestRepository} from './global-test.repository'

export class GlobalTestEnrollmentRepository extends DefaultCrudRepository<
  GlobalTestEnrollment,
  typeof GlobalTestEnrollment.prototype.id,
  GlobalTestEnrollmentRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,

  ) {
    super(GlobalTestEnrollment, dataSource);
  }

  getMyGlobalTestEnrollments = async (userId: number | undefined, globalTestRepository: GlobalTestRepository): Promise<GlobalTestEnrollmentResponse[]> => {
    const enrollments = await this.find({where: {user: userId}});
    const data: GlobalTestEnrollmentResponse[] = [];
    for (const enrollment of enrollments) {
      const globalTest = await globalTestRepository.findById(enrollment.globalTest);
      data.push({
        id: enrollment.id,
        globalTest:{
          id: globalTest.id ?? 0,
          name: globalTest.name,
          createdAt: enrollment.createdAt
        }
      })
    }
    return data;
  };
}
