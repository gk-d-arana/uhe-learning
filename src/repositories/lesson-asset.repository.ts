import {inject} from '@loopback/core';
import {AnyObject, DefaultCrudRepository} from '@loopback/repository';
import {generateImageFileFromBase64} from '../common/generator';
import {UheLearningDataSource} from '../datasources';
import {LessonAsset, LessonAssetRelations} from '../models';
import {LessonAssetCreationCleass} from '../models/lesson-asset.model';
import { deleteImage } from '../common';

export class LessonAssetRepository extends DefaultCrudRepository<
  LessonAsset,
  typeof LessonAsset.prototype.id,
  LessonAssetRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(LessonAsset, dataSource);
  }

  async create(
    entity: LessonAssetCreationCleass,
    options?: AnyObject | undefined,
  ): Promise<LessonAsset> {
    if (entity.fileUrl) {
      entity.fileUrl = await generateImageFileFromBase64(
        'courses/assets',
        entity.fileUrl,
        `${entity.name?.replaceAll(' ', '')}_lesson_asset_url` +
          Math.floor(Math.random() * 22424),
          entity.type
      );
    }
    const res = await super.create(entity);
    return res;
  }
  async updateLessonAsset(
    lessonAsset: LessonAsset,
    entity: LessonAssetCreationCleass,
    options?: AnyObject | undefined,
  ) {
    if (entity.fileUrl.indexOf('base64') != -1) {
      if(lessonAsset.fileUrl){
        await deleteImage(lessonAsset.fileUrl);
      }
      entity.fileUrl = await generateImageFileFromBase64(
        'courses/lessonAssets',
        entity.fileUrl,
        `${entity.name?.replaceAll(' ', '')}_lesson_asset_url` +
          Math.floor(Math.random() * 22424),
          entity.type
      );
    }else{
      entity.fileUrl = lessonAsset.fileUrl;
    }
    await this.updateById(lessonAsset.id, entity);
  }
}
