import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {GlobalPayment, GlobalPaymentRelations} from '../models';

export class GlobalPaymentRepository extends DefaultCrudRepository<
  GlobalPayment,
  typeof GlobalPayment.prototype.id,
  GlobalPaymentRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(GlobalPayment, dataSource);
  }
}
