import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {LocalPayment, LocalPaymentRelations} from '../models';

export class LocalPaymentRepository extends DefaultCrudRepository<
  LocalPayment,
  typeof LocalPayment.prototype.id,
  LocalPaymentRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(LocalPayment, dataSource);
  }
}
