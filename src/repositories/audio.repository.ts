import {inject} from '@loopback/core';
import {AnyObject, DefaultCrudRepository} from '@loopback/repository';
import {generateImageFileFromBase64} from '../common/generator';
import {UheLearningDataSource} from '../datasources';
import {Audio, AudioRelations} from '../models';
import {AudioObjectClass} from '../models/audio.model';
import { deleteImage } from '../common';

export class AudioRepository extends DefaultCrudRepository<
  Audio,
  typeof Audio.prototype.id,
  AudioRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Audio, dataSource);
  }

  async create(
    entity: AudioObjectClass,
    options?: AnyObject | undefined,
  ): Promise<Audio> {
    if (entity.audioUrl) {
      entity.audioUrl = await generateImageFileFromBase64(
        'courses/audio',
        entity.audioUrl,
        `${entity.name?.replaceAll(' ', '')}_audio_url` +
          Math.floor(Math.random() * 22424),
          entity.extension
      );
    }
    delete entity.extension;
    const res = await super.create(entity);
    return res;
  }


  async updateAudio(
    audio: Audio,
    entity: AudioObjectClass,
    options?: AnyObject | undefined,
  ) {
    if (entity.audioUrl.indexOf('base64') != -1) {
      if(audio.audioUrl){
        await deleteImage(audio.audioUrl);
      }
      entity.audioUrl = await generateImageFileFromBase64(
        'courses/audios',
        entity.audioUrl,
        `${entity.name?.replaceAll(' ', '')}_audio_url` +
          Math.floor(Math.random() * 22424),
          entity.extension
      );
    }else{
      entity.audioUrl = audio.audioUrl;
    }
    delete entity.extension;
    await this.updateById(audio.id, entity);
  }
}
