import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {StudentOptionalAnswer, StudentOptionalAnswerRelations} from '../models';

export class StudentOptionalAnswerRepository extends DefaultCrudRepository<
  StudentOptionalAnswer,
  typeof StudentOptionalAnswer.prototype.id,
  StudentOptionalAnswerRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(StudentOptionalAnswer, dataSource);
  }
}
