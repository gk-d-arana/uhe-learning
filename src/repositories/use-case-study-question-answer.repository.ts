import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {UseCaseStudyQuestionAnswer, UseCaseStudyQuestionAnswerRelations} from '../models';

export class UseCaseStudyQuestionAnswerRepository extends DefaultCrudRepository<
  UseCaseStudyQuestionAnswer,
  typeof UseCaseStudyQuestionAnswer.prototype.id,
  UseCaseStudyQuestionAnswerRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(UseCaseStudyQuestionAnswer, dataSource);
  }
}
