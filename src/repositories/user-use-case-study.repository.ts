import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {User, UserUseCaseStudy, UserUseCaseStudyCreationClass, UserUseCaseStudyRelations} from '../models';
import { UserUseCaseAnswerRepository } from './user-use-case-answer.repository';

export class UserUseCaseStudyRepository extends DefaultCrudRepository<
  UserUseCaseStudy,
  typeof UserUseCaseStudy.prototype.id,
  UserUseCaseStudyRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(UserUseCaseStudy, dataSource);
  }

  createUseCase = async (
    user: number | undefined,
    userUseCase: UserUseCaseStudyCreationClass,
    userUseCaseAnswerRepository : UserUseCaseAnswerRepository
  ) => {
    const userUseCaseObj = await this.create({
      useCaseStudy: userUseCase.useCaseStudy,
      user: user ?? 0
    })

    for (const answer of userUseCase.answers) {
      await userUseCaseAnswerRepository.create({
        useCaseStudyQuestion: answer.question,
        useCaseStudyQuestionAnswer: answer.answer,
        userUseCaseStudy: userUseCaseObj.id
      })
    }

  }
}
