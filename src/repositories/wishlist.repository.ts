import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {Wishlist, WishlistRelations} from '../models';
import {WishlistItemClass} from '../models/wishlist.model';

export class WishlistRepository extends DefaultCrudRepository<
  Wishlist,
  typeof Wishlist.prototype.id,
  WishlistRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(Wishlist, dataSource);
  }
  async addItemToWishlist(wishlistItem: WishlistItemClass, userId: number | undefined) {

    const creationDate = new Date();
    wishlistItem.createdAt = creationDate.toISOString();
    wishlistItem.updatedAt = creationDate.toISOString();
    wishlistItem.user = userId;

    const wishlistExisting = await this.findOne({
      where: {
        user: userId,
        item: wishlistItem.item,
        type: wishlistItem.type
      }
    })
    if (wishlistExisting) {
      wishlistExisting.quantity += wishlistItem.quantity;
      if (wishlistExisting.quantity > 0) {
        await this.updateById(wishlistExisting.id, wishlistExisting)
        return wishlistExisting;
      }
      else {
        await this.delete(wishlistExisting);
        return false;
      }
    } else {
      const res = await this.create(wishlistItem);
      return res;
    }
  }
}
