import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {ShopItem, ShopItemRelations} from '../models';

export class ShopItemRepository extends DefaultCrudRepository<
  ShopItem,
  typeof ShopItem.prototype.id,
  ShopItemRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(ShopItem, dataSource);
  }
}
