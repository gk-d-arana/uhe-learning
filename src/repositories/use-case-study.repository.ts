import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {UseCaseStudy, UseCaseStudyRelations} from '../models';

export class UseCaseStudyRepository extends DefaultCrudRepository<
  UseCaseStudy,
  typeof UseCaseStudy.prototype.id,
  UseCaseStudyRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(UseCaseStudy, dataSource);
  }
}
