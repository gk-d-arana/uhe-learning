import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {ChatMessagePoll, ChatMessagePollRelations} from '../models';

export class ChatMessagePollRepository extends DefaultCrudRepository<
  ChatMessagePoll,
  typeof ChatMessagePoll.prototype.id,
  ChatMessagePollRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(ChatMessagePoll, dataSource);
  }
}
