import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {ChatMessagePollAnswer, ChatMessagePollAnswerRelations} from '../models';

export class ChatMessagePollAnswerRepository extends DefaultCrudRepository<
  ChatMessagePollAnswer,
  typeof ChatMessagePollAnswer.prototype.id,
  ChatMessagePollAnswerRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(ChatMessagePollAnswer, dataSource);
  }
}
