import {inject} from '@loopback/core';
import {
  AnyObject,
  DefaultCrudRepository,
  repository,
} from '@loopback/repository';
import {HttpErrors} from '@loopback/rest';
import {UheLearningDataSource} from '../datasources';
import {ColumnAnswer, ColumnAnswerRelations} from '../models';
import {ColumnAnswerCreationClass} from '../models/column-answer.model';
import {TestRepository} from './test.repository';

export class ColumnAnswerRepository extends DefaultCrudRepository<
  ColumnAnswer,
  typeof ColumnAnswer.prototype.id,
  ColumnAnswerRelations
> {
  constructor(
    @repository(TestRepository) public testRepository: TestRepository,
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(ColumnAnswer, dataSource);
  }
  async createOnCondition(
    entity: ColumnAnswerCreationClass,
    options?: AnyObject | undefined,
  ): Promise<ColumnAnswer> {
    const test = await this.testRepository.findById(entity.test);
    if (test.type === 'match') {
      return super.create(entity);
    } else {
      throw new HttpErrors.BadRequest('Test must be Match to add answers');
    }
  }
}
