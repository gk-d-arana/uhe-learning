import {inject} from '@loopback/core';
import {DefaultCrudRepository, repository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {Enrollment, EnrollmentRelations} from '../models';
import {EnrollmentResponse} from '../types/responses';
import {CourseRepository} from './course.repository';
import {StudentLessonRepository} from './student-lesson.repository';

export class EnrollmentRepository extends DefaultCrudRepository<
  Enrollment,
  typeof Enrollment.prototype.id,
  EnrollmentRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
    @repository(StudentLessonRepository)
    public studentLessonRepository: StudentLessonRepository,

  ) {
    super(Enrollment, dataSource);
  }

  getMyEnrollments = async (userId: number | undefined, courseRepository: CourseRepository): Promise<EnrollmentResponse[]> => {
    const enrollments = await this.find({where: {user: userId}});
    const data: EnrollmentResponse[] = [];
    for (const enrollment of enrollments) {
      const course = await courseRepository.findOne({where: {id: enrollment.course}});
      if(course){
        let query = "select l.* from Lesson l join Section  s on l.section = s.id join Course c on s.course = c.id where c.id = " + course.id
        const lessons = await courseRepository?.execute(query);
        const testLessons = lessons.filter((e: any) => e['type'] == 'test');
        const studentLessons = await this.studentLessonRepository.find({where: {course: course.id, user: userId ?? 0}});
        const watchedLessons = studentLessons.filter(el => el.isWatched == true);
        data.push({
          id: enrollment.id,
          course: {
            id: course.id,
            name: course.name,
            courseImage: course.courseImage
          },
          lessonsCount: lessons.length,
          testsCount: testLessons ? testLessons.length : 0,
          progress: Math.floor((100 * watchedLessons.length) / studentLessons.length)
        })
      }
      
    }
    return data;
  };
}
