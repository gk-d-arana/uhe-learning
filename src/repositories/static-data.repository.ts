import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {StaticData, StaticDataRelations} from '../models';

export class StaticDataRepository extends DefaultCrudRepository<
  StaticData,
  typeof StaticData.prototype.id,
  StaticDataRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(StaticData, dataSource);
  }
}
