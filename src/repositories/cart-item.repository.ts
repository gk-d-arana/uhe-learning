/* eslint-disable @typescript-eslint/no-explicit-any */
import {inject} from '@loopback/core';
import { DefaultCrudRepository } from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {CartItem, CartItemRelations} from '../models';
import { CartItemClass } from '../models/cart-item.model';

export class CartItemRepository extends DefaultCrudRepository<
  CartItem,
  typeof CartItem.prototype.id,
  CartItemRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(CartItem, dataSource);
  }


  async addItemToCart(cartItem: CartItemClass, userId: number | undefined){

    const creationDate = new Date();
    cartItem.createdAt = creationDate.toISOString();
    cartItem.updatedAt = creationDate.toISOString();
    cartItem.user = userId;

    const cartExisting = await this.findOne({
      where : {
        user: userId,
        item: cartItem.item,
        type: cartItem.type,
      }
    })
    if(cartExisting){
      cartExisting.inCart = true;
      cartExisting.quantity += cartItem.quantity;
      if(cartExisting.quantity > 0 ){
        await this.updateById(cartExisting.id, cartExisting)
        return cartExisting;
      }
      else{
        await this.delete(cartExisting);
        return false;
      }
    }else{
      const res = await this.create(cartItem);
      return res;
    }
  }
}

