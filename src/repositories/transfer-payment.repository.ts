import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {TransferPayment, TransferPaymentRelations} from '../models';

export class TransferPaymentRepository extends DefaultCrudRepository<
  TransferPayment,
  typeof TransferPayment.prototype.id,
  TransferPaymentRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(TransferPayment, dataSource);
  }
}
