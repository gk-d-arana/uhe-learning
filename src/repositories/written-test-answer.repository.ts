import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {WrittenTestAnswer, WrittenTestAnswerRelations} from '../models';

export class WrittenTestAnswerRepository extends DefaultCrudRepository<
  WrittenTestAnswer,
  typeof WrittenTestAnswer.prototype.id,
  WrittenTestAnswerRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(WrittenTestAnswer, dataSource);
  }
}
