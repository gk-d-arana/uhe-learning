import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {UheLearningDataSource} from '../datasources';
import {CardPayment, CardPaymentRelations} from '../models';

export class CardPaymentRepository extends DefaultCrudRepository<
  CardPayment,
  typeof CardPayment.prototype.id,
  CardPaymentRelations
> {
  constructor(
    @inject('datasources.UHELearning') dataSource: UheLearningDataSource,
  ) {
    super(CardPayment, dataSource);
  }
}
