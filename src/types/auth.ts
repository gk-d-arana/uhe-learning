export class RegisterObject{
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address?: string;
  age?: number;
  isApproved? :boolean;
  isReviewed?: boolean;
  isPrimary?: boolean;
  isBlocked?: boolean;
  blockEndingLimit?: string;
  blockMessage?: string;
  rating?: number;
  role: "student" | "instructor" | "admin";
  profileImage?: string;
  bio?: string;
  coursesEnrollment?: number;
  education?: string;
}

export class RoleChangeObject{
  bio?: string;
  education?:string;
}
