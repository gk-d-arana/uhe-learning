export class StudentWrittenAnswerCreation {
  value: string;
}

export class StudentOptionalAnswerCreation {
  isTrue: boolean;
  answerId: number|undefined;
}

export class StudentColumnAnswerCreation {
  value: string;
  oppositeValue: string;
  isTrue: boolean;
}

export class FullStudentTestCreation {
  globalStudentTestId: number;
  answers: {
    test: number;
    writtenAnswer?: StudentWrittenAnswerCreation;
    optionalAnswer?: StudentOptionalAnswerCreation;
    columnAnswers?: StudentColumnAnswerCreation[];
  }[]
}

export class StudentTestCreation {
  globalStudentTestId: number;
  test: number;
  writtenAnswer?: StudentWrittenAnswerCreation;
  optionalAnswer?: StudentOptionalAnswerCreation;
  columnAnswers?: StudentColumnAnswerCreation[];
}

export class LiveStreamCreation {
  name: string;
  user?: number;
  description: string;
  startDate: string;
  isPrivate: boolean;
  coverImage?: string;
  isLive?: boolean;
  password: string;
}

export class Option {
  id: number;
  image: string;
  step: number;
}

export class Step {
  id: number;
  question: string;
  options: number[];
}
export class StudentStepAnswer {
  step: number;
  option: number
}
