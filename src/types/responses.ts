import { UseCaseStudy, UseCaseStudyQuestion } from '../models';
import {Category} from '../models/category.model';
import {Instructor} from '../models/instructor.model';
import {ShopItem} from '../models/shop-item.model';
import {User, UserRelations} from '../models/user.model';

export class LessonResponse {
  id: number;
  section: number;
  createdAt?: string;
  updatedAt: string;
  type: 'asset' | 'vido' | 'audio' | 'test';
  isWatched: boolean;
}

export class SectionResponse {
  id?: number;
  name?: string;
  description?: string;
  createdAt?: string;
  updatedAt?: string;
  isFree?: boolean;
  lessons?: LessonResponse[];
}

export class CourseResponse {
  id: number;
  name: string;
  mobileDescription: string;
  webDescription: string;
  createdAt?: string;
  updatedAt: string;
  categories: Category[];
  instructor?: Instructor;
  courseImage: string;
  price: number;
  overviewImage: string;
  rating: number;
  descriptionVideo: string;
  descriptionImage: string;
  enrollmentNumber: number;
  sections?: SectionResponse[];
}

export class CartItemObjectResponse {
  id: number;
  name: string;
  price: number;
  image: string;
}

export class CartItemResponse {
  id: number;
  user: number;
  item: CartItemObjectResponse;
  type: 'course' | 'shopItem' | 'globalTest';
  createdAt?: string;
  updatedAt: string;
  quantity: number;
  inCart: boolean;
}

export class WishlistResponse {
  id: number;
  user: number;
  item: CartItemObjectResponse;
  type: 'course' | 'shopItem' | 'globalTest';
  createdAt?: string;
  updatedAt: string;
  quantity: number;
  inCart: boolean;
}

class EnrollmentCourseOuterResponse {
  id: number | undefined;
  name: string;
  courseImage?: string;
  createdAt?: string;
}

class GlobalTestEnrollmentOuterResponse {
  id: number | undefined;
  name: string;
  createdAt?: string;
}

export class EnrollmentResponse {
  id: number | undefined;
  course: EnrollmentCourseOuterResponse;
  progress: number;
  lessonsCount?: number | undefined;
  testsCount?: number | undefined;
}


export class GlobalTestEnrollmentResponse {
  id: number | undefined;
  globalTest: GlobalTestEnrollmentOuterResponse;
}



export class TestListResponse {
  id: number | undefined;
  name: string;
  createdAt?: string;
}

export class StudentTestListResponse {
  id: number | undefined;
  name: string;
  createdAt?: string;
  course: string
  answeredAt?: string
  globalTest: number;
  isAnswered: boolean;
}

export class UserTests {
  userId: number | undefined;
  firstName: string;
  lastName: string;
  profileImage: string | undefined;
  testsCount: number;
}

export class Answer {
  id: number;
  value: string;
  isTrue: boolean;
}

export class ColumnAnswer {
  id: number;
  value: string;
  oppositeValue: string;
}

export class WrittenStudentAnswer {
  id: number;
  value: string;
  isTrue: boolean;
  createdAt?: string;
  updatedAt: string;
}

export class OptionalStudentAnswer {
  id: number;
  isTrue: boolean;
  createdAt?: string;
  updatedAt: string;
}

export class MatchStudentAnswer {
  id: number;
  value: string;
  oppositeValue: string;
  isTrue: boolean;
  createdAt?: string;
  updatedAt: string;
}

export class TestSolvingResponse {
  id: number;
  name: string;
  question: string;
  type: string;
  answers?: Answer[];
  columnAnswers?: ColumnAnswer[];
  studentAnswers?: MatchStudentAnswer[];
  studentAnswer?: WrittenStudentAnswer | OptionalStudentAnswer;
}

export class CourseReview {
  id: number | undefined;
  user: {
    id: number | undefined;
    firstName: string;
    lastName: string;
    profileImage: string | undefined;
  };
  rating: number;
  textContent: string | undefined;
  voiceContent: string | undefined;
  videoContent: string | undefined;
  createdAt?: string;
  updatedAt: string;
}

export class InstructorProfile {
  user: User & UserRelations;
  id?: number | undefined;
  createdAt?: string;
  isApproved?: boolean;
  isReviewed?: boolean;
  isPrimary?: boolean;
  updatedAt?: string | undefined;
  bio?: string | undefined;
  education?: string | undefined;
  rating: number;
  coursesEnrollment?: number | undefined;
}

export class InstructorTest {
  id: number | undefined;
  name: string;
  createdAt?: string;
  price: number;
}

export class InstructorResponse {
  profile: InstructorProfile;
  courses: EnrollmentCourseOuterResponse[];
  tests: InstructorTest[];
}

export class ShopItemResponse {
  item: ShopItem;
  inCart: boolean;
  inWishlist: boolean;
  isBought: boolean;
}

export class VRCInstanceMembersResponse {
  id: number | undefined;
  firstName: string;
  lastName: string;
  role?: string;
  phoneNumber: string;
  profileImage: string | undefined;
  email: string;
}

export class VRCInstanceResponse {
  id: number | undefined;
  name: string;
  chatGroup?: number;
  description: string;
  coverImage: string;
  createdAt: string | undefined;
  updatedAt: string | undefined;
  members: VRCInstanceMembersResponse[]
}


export class TransferPaymentResponse {
  id?: number;
  items: {
    name: string;
    quantity: number;
    type: string;
  }[];
  proof_image?: string;
  is_approved: boolean;
}


export class ChatMessageResponse {
  id: number | undefined;
  createdAt: string | undefined;
  value?: string | undefined;
  type: string;
  chatMessagePoll?: {
    id: number | undefined;
    question: string | undefined;
    answers: {
      id: number | undefined;
      value: string;
      isTrue: boolean;
    }[];
    usersAnswers: {
      id: number | undefined;
      value: string | undefined;
      isTrue: boolean;
      user: {
        firstName: string | undefined;
        lastName: string | undefined;
        id: number | undefined;
        profileImage: string | undefined;
        role: string | undefined;
      }
    }[]
  };
  user: {
    firstName: string | undefined;
    lastName: string | undefined;
    id: number | undefined;
    profileImage: string | undefined;
    role: string | undefined;
  }
}


export class UseCaseStudyResonse{
  useCase: UseCaseStudy;
  questions: {
    question: string;
    createdAt: string;
    updatedAt: string;
    answers:{
      value: string;
      next: number;
      createdAt: string;
      updatedAt: string;
    }[]
  }[]
}