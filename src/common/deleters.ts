import { Audio, Course, GlobalTest, Lesson, LessonAsset, Test, Video } from '../models';
import { VideoRepository } from '../repositories/video.repository';
import { deleteImage } from './helpers';
import { AudioRepository } from '../repositories/audio.repository';
import { LessonAssetRepository } from '../repositories/lesson-asset.repository';
import { LessonRepository } from '../repositories/lesson.repository';
import { TestRepository } from '../repositories/test.repository';
import { GlobalTestRepository } from '../repositories';
import { Section } from '../models/section.model';
import { SectionRepository } from '../repositories/section.repository';
import { CourseRepository } from '../repositories/course.repository';


export const deleteTest = async (
    test: Test,
    testRepository: TestRepository
) => {
    test.isDeleted = true;
    await testRepository.updateById(test.id, test);
}

export const deleteGlobalTest = async (
    globalTest: GlobalTest,
    globalTestRepository: GlobalTestRepository,
    testRepository: TestRepository
) => {
    globalTest.isDeleted = true;
    await globalTestRepository.updateById(globalTest.id, globalTest);
    const tests = await testRepository.find({ where: { globalTest: globalTest.id } });
    for (const test of tests) {
        deleteTest(test, testRepository);
    }
}

export const deleteVideo = async (
    video: Video,
    videoRepository: VideoRepository
) => {
    await deleteImage(video.videoUrl);
    await videoRepository.deleteById(video.id);
}

export const deleteAudio = async (
    audio: Audio,
    audioRepository: AudioRepository
) => {
    await deleteImage(audio.audioUrl);
    await audioRepository.deleteById(audio.id);
}

export const deleteAsset = async (
    lessonAsset: LessonAsset,
    lessonAssetRepository: LessonAssetRepository
) => {
    await deleteImage(lessonAsset.fileUrl);
    await lessonAssetRepository.deleteById(lessonAsset.id);
}


export const deleteLesson = async (
    lesson: Lesson,
    lessonRepository: LessonRepository,
    videoRepository: VideoRepository,
    audioRepository: AudioRepository,
    lessonAssetRepository: LessonAssetRepository,
    testRepository: TestRepository,
    globalTestRepository: GlobalTestRepository
) => {
    try{
        switch (lesson.type) {
            case 'video':
                const video = await videoRepository.findOne({ where: { lesson: lesson.id } })
                if (video) {
                    await deleteVideo(video, videoRepository);
                }
                break;
            case 'audio':
                const audio = await audioRepository.findOne({ where: { lesson: lesson.id } })
                if (audio) {
                    await deleteAudio(audio, audioRepository);
                }
                break;
            case 'asset':
                const asset = await lessonAssetRepository.findOne({ where: { lesson: lesson.id } })
                if (asset) {
                    await deleteAsset(asset, lessonAssetRepository);
                }
                break;
            case 'test':
                const globalTest = await globalTestRepository.findOne({ where: { lesson: lesson.id } })
                if (globalTest) {
                    await deleteGlobalTest(globalTest, globalTestRepository, testRepository);
                }
                break;
            default:
                break;
        }
    }catch(err){
        //
    }
    
    await lessonRepository.deleteById(lesson.id)
}

export const deleteSection = async (
    section: Section,
    sectionRepository: SectionRepository,
    lessonRepository: LessonRepository,
    videoRepository: VideoRepository,
    audioRepository: AudioRepository,
    lessonAssetRepository: LessonAssetRepository,
    testRepository: TestRepository,
    globalTestRepository: GlobalTestRepository
) => {
    const lessons = await lessonRepository.find({ where: { section: section.id } });
    for (const lesson of lessons) {
        await deleteLesson(lesson, lessonRepository,
            videoRepository,
            audioRepository,
            lessonAssetRepository,
            testRepository,
            globalTestRepository);
    }

    await sectionRepository.deleteById(section.id);
}


export const courseDeleter = async (
    course: Course,
    courseRepository: CourseRepository,
    sectionRepository: SectionRepository,
    lessonRepository: LessonRepository,
    videoRepository: VideoRepository,
    audioRepository: AudioRepository,
    lessonAssetRepository: LessonAssetRepository,
    testRepository: TestRepository,
    globalTestRepository: GlobalTestRepository
) => {
    const sections = await sectionRepository.find({ where: { course: course.id } });
    for (const section of sections) {
        await deleteSection(section, sectionRepository, lessonRepository,
            videoRepository,
            audioRepository,
            lessonAssetRepository,
            testRepository,
            globalTestRepository);
    }
    if(course.courseImage){
        deleteImage(course.courseImage);
      }
      if(course.overviewImage){
        deleteImage(course.overviewImage);
      }
      if(course.descriptionVideo){
        deleteImage(course.descriptionVideo);
      }
      if(course.descriptionImage){
        deleteImage(course.descriptionImage);
      }
    await courseRepository.deleteById(course.id);
}