import {HttpErrors} from '@loopback/rest';
import {AES, enc} from 'crypto-js';
import {decode} from 'jsonwebtoken';
import {ShopItemEnrollmentRepository, WishlistRepository} from '../repositories';
import {CartItemRepository} from '../repositories/cart-item.repository';
import {EnrollmentRepository} from '../repositories/enrollment.repository';
import {UserRepository} from '../repositories/user.repository';
import { GlobalStudentTestRepository } from '../repositories/global-student-test.repository';

export const isAdmin = async (
  token: string,
  userRepository: UserRepository,
) => {
  const decodedToken = decode(token, {json: true});
  if (!token) {
    throw new HttpErrors.Unauthorized('Admin Privilege Required');
    return;
  }
  const decryptedBytes = AES.decrypt(
    decodedToken?.secretCode,
    process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
  );
  const secretCode = decryptedBytes.toString(enc.Utf8);
  const admin = await userRepository.findOne({
    where: {
      email: decodedToken?.email,
      role: 'admin',
      secretCode: secretCode,
    },
  });
  if (!admin) {
    throw new HttpErrors.Unauthorized('Admin Privilege Required');
    return;
  }
  return true;
};

export const isInstructor = async (
  token: string,
  userRepository: UserRepository,
) => {
  const decodedToken = decode(token, {json: true});
  if (!token) {
    throw new HttpErrors.Unauthorized('Instructor Privilege Required');
    return;
  }
  const decryptedBytes = AES.decrypt(
    decodedToken?.secretCode,
    process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
  );
  const secretCode = decryptedBytes.toString(enc.Utf8);
  const instructorUser = await userRepository.findOne({
    where: {
      email: decodedToken?.email,
      role: 'instructor',
      secretCode: secretCode,
    },
  });
  if (instructorUser) {
    return instructorUser.id;
  } else {
    throw new HttpErrors.Unauthorized('Instructor Privilege Required');
    return;
  }
};

export const isStudent = async (
  token: string,
  userRepository: UserRepository,
) => {
  const decodedToken = decode(token, {json: true});
  if (!decodedToken) {
    throw new HttpErrors.Unauthorized('Student Privilege Required');
    return;
  }
  const decryptedBytes = AES.decrypt(
    decodedToken?.secretCode,
    process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
  );
  const secretCode = decryptedBytes.toString(enc.Utf8);
  const user = await userRepository.findOne({
    where: {
      email: decodedToken?.email,
      role: 'student',
      secretCode: secretCode,
    },
  });
  if (!user) {
    throw new HttpErrors.Unauthorized('Student Privilege Required');
    return;
  }
  return user.id;
};

export const canAddCourse = async (
  token: string,
  userRepository: UserRepository,
) => {
  const decodedToken = decode(token, {json: true});
  if (!token) {
    throw new HttpErrors.Unauthorized('Instructor | Admin Privilege Required');
    return;
  }
  const decryptedBytes = AES.decrypt(
    decodedToken?.secretCode,
    process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
  );
  const secretCode = decryptedBytes.toString(enc.Utf8);
  const instructorUser = await userRepository.findOne({
    where: {
      email: decodedToken?.email,
      role: 'instructor',
      secretCode: secretCode,
    },
  });

  const adminUser = await userRepository.findOne({
    where: {
      email: decodedToken?.email,
      role: 'admin',
      secretCode: secretCode,
    },
  });
  if (adminUser) {
    return adminUser.id;
  } else if (instructorUser) {
    return instructorUser.id;
  } else {
    throw new HttpErrors.Unauthorized('Instructor | Admin Privilege Required');
    return;
  }
};

export const studentIsEnrolled = async (
  userId: number | undefined,
  courseId: number | undefined,
  enrollmentRepository: EnrollmentRepository,
) => {
  const enrollment = await enrollmentRepository.findOne({
    where: {
      user: userId,
      course: courseId,
    },
  });
  if (!enrollment) {
    return false;
  }
  return true;
};

export const studentIsEnrolledInGlobalTest = async (
  userId: number | undefined,
  globalTestId: number | undefined,
  globalStudentTestRepository: GlobalStudentTestRepository,
) => {
  const enrollment = await globalStudentTestRepository.findOne({
    where: {
      user: userId,
      globalTest: globalTestId,
    },
  });
  if (!enrollment) {
    return false;
  }
  return true;
};


export const shopItemIsBought = async (
  userId: number | undefined,
  shopItemId: number | undefined,
  shopItemEnrollmentRepository: ShopItemEnrollmentRepository,
) => {
  const enrollment = await shopItemEnrollmentRepository.findOne({
    where: {
      user: userId,
      shopItem: shopItemId,
    },
  });
  if (!enrollment) {
    return false;
  }
  return true;
};

export const isInCart = async (
  userId: number,
  itemId: number | undefined,
  itemType: 'course' | 'globalTest' | 'shopItem',
  cartItemRepository: CartItemRepository,
) => {
  const cartItem = await cartItemRepository.findOne({
    where: {
      user: userId,
      item: itemId,
      inCart: true,
      type: itemType,
    },
  });
  if (!cartItem) {
    return false;
  }
  return true;
};

export const isInWishlist = async (
  userId: number,
  objId: number | undefined,
  type: 'course' | 'shopItem' | 'globalTest',
  wishlistRepository: WishlistRepository,
) => {
  const wishlistItem = await wishlistRepository.findOne({
    where: {
      user: userId,
      item: objId,
      inWishlist: true,
      type: type
    },
  });
  if (!wishlistItem) {
    return false;
  }
  return true;
};

export const getUser = async (token: string, userRepository: UserRepository) => {
  const decodedToken = decode(token, {json: true});
  if (!decodedToken) {
    throw new HttpErrors.Unauthorized('Student Privilege Required');
    return;
  }
  const decryptedBytes = AES.decrypt(
    decodedToken?.secretCode,
    process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
  );
  const secretCode = decryptedBytes.toString(enc.Utf8);
  const user = await userRepository.findOne({
    where: {
      email: decodedToken?.email,
      secretCode: secretCode,
    },
  });
  if (!user) {
    return;
  }
  return user.id;
}
export const tryGetUserId = async (token: string, userRepository: UserRepository) => {
  const decodedToken = decode(token, {json: true});
  if (!decodedToken) {
    return;
  }
  const decryptedBytes = AES.decrypt(
    decodedToken?.secretCode,
    process.env.ENCRYPTION_SECRET ?? 'myencryptionsecret',
  );
  const secretCode = decryptedBytes.toString(enc.Utf8);
  const user = await userRepository.findOne({
    where: {
      email: decodedToken?.email,
      secretCode: secretCode,
    },
  });
  if (!user) {
    return;
  }
  return user.id;
}

export const getCertificateUser = async (token: string, userRepository: UserRepository) => {
  const decodedToken = decode(token, {json: true});
  if (!decodedToken) {
    throw new HttpErrors.Unauthorized('Student Privilege Required');
    return;
  }
  const user = await userRepository.findOne({
    where: {
      id: decodedToken?.id
    },
  });
  if (!user) {
    return;
  }
  return user;
}
