import {unlink} from 'fs-extra';
export const deleteImage = async (path?: string) => {
  if (path) {
    const rootPath = process.env.isProduction ? '/var/www/uh-training.com/home/uht_ssh_sr/web/uhe-learning/public' : '';
    path = rootPath + path;
    try{
      await unlink(path);
    }catch(err){
      console.log(err);
    }
  }

};