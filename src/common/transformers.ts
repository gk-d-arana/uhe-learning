import {ShopItem, TransferPayment} from '../models';
import {CartItem} from '../models/cart-item.model';
import {GlobalTest} from '../models/global-test.model';
import {Student} from '../models/student.model';
import {CartItemRepository, GlobalPaymentRepository, GlobalStudentTestRepository, GlobalTestRepository, LocalPaymentRepository, OrderRepository, PaymentMethodRepository, ShopItemEnrollmentRepository} from '../repositories';
import {AnswerRepository} from '../repositories/answer.repository';
import {ColumnAnswerRepository} from '../repositories/column-answer.repository';
import {CourseRepository} from '../repositories/course.repository';
import {ShopItemRepository} from '../repositories/shop-item.repository';
import {StudentRepository} from '../repositories/student.repository';
import { TestRepository } from '../repositories/test.repository';
import { UserRepository } from '../repositories/user.repository';
import {WishlistRepository} from '../repositories/wishlist.repository';
import {CartItemResponse, ShopItemResponse, TransferPaymentResponse} from '../types/responses';
import { isInCart, shopItemIsBought, isInWishlist, studentIsEnrolledInGlobalTest } from './middleware';

export const CartItemTransformer = async (
  userId: number | undefined,
  carts: CartItem[],
  courseRepository: CourseRepository,
  shopItemRepository: ShopItemRepository,
  wishlistRepository: WishlistRepository,
  cartItemRepository: CartItemRepository,
  globalTestRepository: GlobalTestRepository,
  shopItemEnrollmentRepository?: ShopItemEnrollmentRepository,
): Promise<CartItemResponse[]> => {
  const cartItems = [];

  for (const cartItem of carts) {
    let inWishlist = false;
    let inCart = false;
    let obj;

    if (cartItem.type === 'course') {
      obj = await courseRepository.findById(cartItem.item);
    } else if (cartItem.type === 'shopItem') {
      obj = await shopItemRepository.findById(cartItem.item);
    } else if (cartItem.type === 'globalTest') {
      obj = await globalTestRepository.findById(cartItem.item);
    }
    const wishlistItem = await wishlistRepository.find({
      where: {item: cartItem.item, inWishlist: true, user: userId},
    });
    if (wishlistItem?.[0]) {
      inWishlist = true;
    }

    const cartObj = await cartItemRepository.find({
      where: {item: cartItem.item, inCart: true, user: userId},
    });
    if (cartObj?.[0]) {
      inCart = true;
    }
    if (obj) {
      cartItems.push({
        id: cartItem.id ?? 0,
        user: cartItem.user,
        item: {
          id: obj.id ?? 0,
          name: obj.name,
          price: obj.price,
          image:
            cartItem.type === 'shopItem'
              ? obj.shopItemImage
              : obj.courseImage ?? '',
        },
        shopItem: cartItem.type === 'shopItem' && obj instanceof ShopItem ? getShopItemResponse(obj, cartItemRepository, wishlistRepository, shopItemEnrollmentRepository!, userId) : null,
        type: cartItem.type,
        createdAt: cartItem.createdAt,
        updatedAt: cartItem.updatedAt,
        quantity: cartItem.quantity,
        inCart: inCart,
        inWishlist: inWishlist,
      });
    }
  }
  return cartItems;
};

export const StudentTransformer = async (
  students: Student[],
  userRepository: UserRepository,
  studentRepository: StudentRepository,
) => {
  const res = [];
  for (const student of students) {
    const user = await userRepository.findById(student.user);
    res.push({
      id: student.id,
      user: user,
      createdAt: student.createdAt,
      updatedAt: student.updatedAt,
    });
  }
  return res;
};

export const GlobalTestTransformer = async (
  globalTest: GlobalTest,
  testRepository: TestRepository,
  answerRepository: AnswerRepository,
  columnAnswerRepository: ColumnAnswerRepository,
  courseRepository: CourseRepository,
) => {
  const course = await courseRepository.find({
    where: {id: globalTest.course},
  });
  if (course?.[0]) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    //@ts-ignore
    globalTest.course = {
      id: course[0].id,
      name: course[0].name,
      courseImage: course[0].courseImage,
      createdAt: course[0].createdAt,
    };
  }
  const tests = await testRepository.find({
    where: {
      globalTest: globalTest.id,
    },
  });
  const resTests = [];
  for (const test of tests) {
    if (test.type === 'written') {
      resTests.push({
        id: test.id,
        name: test.name,
        question: test.question,
        type: test.type,
      });
    } else if (test.type === 'optional') {
      const testOptional: {
        id?: number;
        name: string;
        question: string;
        type: string;
        answers: {
          id?: number;
          value: string;
          isTrue: boolean;
        }[];
      } = {
        id: test.id,
        name: test.name,
        question: test.question,
        type: test.type,
        answers: [],
      };

      const answers = await answerRepository.find({
        where: {test: test.id},
      });
      for (const answer of answers) {
        testOptional.answers.push({
          id: answer.id,
          value: answer.value,
          isTrue: answer.isTrue,
        });
      }
      resTests.push(testOptional);
    } else if (test.type === 'match') {
      const testMatch: {
        id?: number;
        name: string;
        question: string;
        type: string;
        columnAnswers: {
          id?: number;
          value: string;
          oppositeValue: string;
        }[];
      } = {
        id: test.id,
        name: test.name,
        question: test.question,
        type: test.type,
        columnAnswers: [],
      };
      const columnAnswers = await columnAnswerRepository.find({
        where: {test: test.id},
      });
      for (const columnAnswer of columnAnswers) {
        testMatch.columnAnswers.push({
          id: columnAnswer.id,
          value: columnAnswer.value,
          oppositeValue: columnAnswer.oppositeValue,
        });
      }
      resTests.push(testMatch);
    }
  }

  return {...globalTest, tests: resTests};
};


export const GlobalTestObjTransformer = async (
  userId: number | undefined,
  globalTest: GlobalTest,
  globalStudentTestRepository: GlobalStudentTestRepository,
  cartItemRepository: CartItemRepository,
  userRepository: UserRepository,
  wishlistRepository: WishlistRepository,
  testRepository: TestRepository
) => {
  let res;
  let inCart =  false;
  let inWishlist =  false;
  let enrolled =  false;
    if(userId){
      inCart = await isInCart(userId, globalTest.id, 'globalTest', cartItemRepository);
      inWishlist = await isInWishlist(userId, globalTest.id, 'globalTest', wishlistRepository);
      enrolled = await studentIsEnrolledInGlobalTest(userId, globalTest.id, globalStudentTestRepository)
    }
    let user ;
    try{
      user = await userRepository.findById(globalTest.user);
    }catch(err){
      //
    }
    res = {
      globalTest: {
        ...globalTest,
        testsCount: (await testRepository.find({where: {globalTest: globalTest.id}})).length
      },
      user :  user ?  {
        id: user.id,
        name: `${user.firstName} ${user.lastName}`,
        profileImage: user.profileImage
      } : null,
      inCart,
      inWishlist,
      enrolled
    }
  return res;
}

export const GlobalTestListTransformer = async (
  userId: number | undefined,
  globalTests: GlobalTest[],
  globalStudentTestRepository: GlobalStudentTestRepository,
  cartItemRepository: CartItemRepository,
  userRepository: UserRepository,
  wishlistRepository: WishlistRepository,
  testRepository: TestRepository
) => {
  const res:any = [];
  
  for (const globalTest of globalTests) {
    res.push(await GlobalTestObjTransformer(
      userId,
      globalTest,
      globalStudentTestRepository,
      cartItemRepository,
      userRepository,
      wishlistRepository,
      testRepository
    ));
  }
  return res;
}


export const transferPaymentTransformer = async (
  transferPayments: TransferPayment[],
  shopItemRepository: ShopItemRepository,
  cartItemRepository: CartItemRepository,
  courseRepository: CourseRepository,
  globalTestRepository: GlobalTestRepository,
  globalPaymentRepository: GlobalPaymentRepository,
  paymentMethodRepository: PaymentMethodRepository,
  localPaymentRepository: LocalPaymentRepository,
  orderRepository: OrderRepository
): Promise<TransferPaymentResponse[]> => {
  let res = [];
  for (const transferPayment of transferPayments) {
    if (transferPayment.isLocal) {
      const localPayment = await localPaymentRepository.find({where: {transferPayment: transferPayment.id}});
      if (localPayment?.[0]) {
        const paymentMethod = await paymentMethodRepository.find({where: {localPayment: localPayment[0].id}})
        if (paymentMethod?.[0]) {
          const order = await orderRepository.find({where: {paymentMethod: paymentMethod[0].id}});
          if (order?.[0]) {
            let items = []
            for (const cartItem of order[0].cartItems) {
              const cartItemObj = await cartItemRepository.find({where: {id: cartItem}});
              if (cartItemObj?.[0]) {
                if (cartItemObj[0].type == 'course') {
                  const course = await courseRepository.find({where: {id: cartItemObj[0].item}});
                  if (course?.[0]) {
                    items.push({
                      quantity: cartItemObj[0].quantity,
                      name: course[0].name,
                      type: cartItemObj[0].type
                    });
                  }
                } else if (cartItemObj[0].type == 'shopItem') {
                  const shopItem = await shopItemRepository.find({where: {id: cartItemObj[0].item}});
                  if (shopItem?.[0]) {
                    items.push({
                      quantity: cartItemObj[0].quantity,
                      name: shopItem[0].name,
                      type: cartItemObj[0].type
                    });
                  }
                } else {
                  const globalTest = await globalTestRepository.find({where: {id: cartItemObj[0].item}});
                  if (globalTest?.[0]) {
                    items.push({
                      quantity: cartItemObj[0].quantity,
                      name: globalTest[0].name,
                      type: cartItemObj[0].type
                    });
                  }
                }
              }
            }
            res.push({
              id: order[0].id,
              items: items,
              proof_image: transferPayment.proofImage,
              is_approved: order[0].paymentApproved ?? false
            });
          }
        }
      }
    } else {
      const globalPayment = await globalPaymentRepository.find({where: {transferPayment: transferPayment.id}});
      if (globalPayment?.[0]) {
        const paymentMethod = await paymentMethodRepository.find({where: {globalPayment: globalPayment[0].id}})
        if (paymentMethod?.[0]) {
          const order = await orderRepository.find({where: {paymentMethod: paymentMethod[0].id}});
          if (order?.[0]) {
            let items = []
            for (const cartItem of order[0].cartItems) {
              const cartItemObj = await cartItemRepository.find({where: {id: cartItem}});
              if (cartItemObj?.[0]) {
                if (cartItemObj[0].type == 'course') {
                  const course = await courseRepository.find({where: {id: cartItemObj[0].item}});
                  if (course?.[0]) {
                    items.push({
                      quantity: cartItemObj[0].quantity,
                      name: course[0].name,
                      type: cartItemObj[0].type
                    });
                  }
                } else if (cartItemObj[0].type == 'shopItem') {
                  const shopItem = await shopItemRepository.find({where: {id: cartItemObj[0].item}});
                  if (shopItem?.[0]) {
                    items.push({
                      quantity: cartItemObj[0].quantity,
                      name: shopItem[0].name,
                      type: cartItemObj[0].type
                    });
                  }
                } else {
                  const globalTest = await globalTestRepository.find({where: {id: cartItemObj[0].item}});
                  if (globalTest?.[0]) {
                    items.push({
                      quantity: cartItemObj[0].quantity,
                      name: globalTest[0].name,
                      type: cartItemObj[0].type
                    });
                  }
                }
              }
            }
            res.push({
              id: order[0].id,
              items: items,
              proof_image: transferPayment.proofImage,
              is_approved: order[0].paymentApproved ?? false
            });
          }
        }
      }
    }
  }
  return res;
};



export const getShopItemResponse = async (
  shopItem: ShopItem,
  cartItemRepository: CartItemRepository,
  wishlistRepository: WishlistRepository,
  shopItemEnrollmentRepository: ShopItemEnrollmentRepository,
  userId?: number | undefined
): Promise<ShopItemResponse> => {
  let inWishlist = false;
  let inCart = false;
  let isBought = false;
  if (userId) {
    const cartItem = await cartItemRepository.find({
      where: {item: shopItem.id, inCart: true, user: userId},
    });
    const wishlist = await wishlistRepository.find({
      where: {item: shopItem.id, inWishlist: true, user: userId},
    });

    isBought = await shopItemIsBought(userId, shopItem.id, shopItemEnrollmentRepository);

    if (cartItem?.[0]) {
      inCart = true;
    }
    if (wishlist?.[0]) {
      inWishlist = true;
    }
  }

  if (!isBought) {
    shopItem.file = '';
  }

  return {
    item: shopItem,
    isBought,
    inCart,
    inWishlist,
  };
}
