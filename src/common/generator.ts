import {outputFile} from 'fs-extra';

/* eslint-disable no-async-promise-executor */
/* eslint-disable @typescript-eslint/no-misused-promises */

export const generateString = (length: number): string => {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

export const generateImageFileFromBase64 = async (
  innerPath: string,
  base64: string,
  imageName: string,
  extension?: string,
) => {
  const stringImage = base64.split(';base64,').pop() ?? ' ';
  const rootPath = process.env.isProduction ? '/var/www/uh-training.com/home/uht_ssh_sr/web/uhe-learning/public' : '';
  let subPath = '/media/' + innerPath + '/' + imageName.replaceAll('/', '-') + '.';
  let path = rootPath + subPath;
  if (extension) {
    path += extension;
    subPath+= extension;
  } else {
    path += base64.substring(
      base64.indexOf('/') + 1,
      base64.indexOf(';base64'),
    );
    subPath+= base64.substring(
      base64.indexOf('/') + 1,
      base64.indexOf(';base64'),
    );
  }
  outputFile(
    path,
    stringImage,
    {encoding: 'base64'},
    function (err) {
      console.log(err);
    },
  );
  return subPath;
};

