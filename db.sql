-- MySQL dump 10.13  Distrib 8.0.33, for Linux (x86_64)
--
-- Host: localhost    Database: e_learning
-- ------------------------------------------------------
-- Server version	8.0.33-0ubuntu0.22.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Admin`
--

DROP TABLE IF EXISTS `Admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin`
--

LOCK TABLES `Admin` WRITE;
/*!40000 ALTER TABLE `Admin` DISABLE KEYS */;
INSERT INTO `Admin` VALUES (1,1);
/*!40000 ALTER TABLE `Admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Answer`
--

DROP TABLE IF EXISTS `Answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Answer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` varchar(512) NOT NULL,
  `test` int NOT NULL,
  `isTrue` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Answer`
--

LOCK TABLES `Answer` WRITE;
/*!40000 ALTER TABLE `Answer` DISABLE KEYS */;
INSERT INTO `Answer` VALUES (1,'b',1,0),(2,'c',1,1),(3,'d',1,1),(4,'a',1,1),(5,'a',5,0),(6,'b',5,1),(7,'c',5,0),(8,'d',5,0),(9,'b',7,0),(10,'c',7,1),(11,'d',7,1),(12,'a',7,1);
/*!40000 ALTER TABLE `Answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Audio`
--

DROP TABLE IF EXISTS `Audio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Audio` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `audioUrl` varchar(512) NOT NULL,
  `lesson` int NOT NULL,
  `audioDuration` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Audio`
--

LOCK TABLES `Audio` WRITE;
/*!40000 ALTER TABLE `Audio` DISABLE KEYS */;
INSERT INTO `Audio` VALUES (1,'audio','desc','/media/courses/audio/audio_audio_url11702.mpeg',14,27);
/*!40000 ALTER TABLE `Audio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Blog`
--

DROP TABLE IF EXISTS `Blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Blog` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `blogImage` varchar(512) NOT NULL,
  `blogVideo` varchar(512) DEFAULT NULL,
  `categories` text,
  `isPrimary` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Blog`
--

LOCK TABLES `Blog` WRITE;
/*!40000 ALTER TABLE `Blog` DISABLE KEYS */;
INSERT INTO `Blog` VALUES (1,'5 Common Server Vulnerabilities with Node.js','<p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span style=\"font-size: 2.5em;\"><strong><span data-slate-string=\"true\">Introduction</span></strong></span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">Node.js is a powerful and widely-used JavaScript runtime environment for building server-side applications. However, like any other software, Node has its own set of vulnerabilities that can lead to security issues if not properly addressed. Please do note that these vulnerabilities are not unique to Node, they can be found in every backend programming language.</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-zero-width=\"n\" data-slate-length=\"0\">﻿<br></span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">This article will explore 5 common vulnerabilities:</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-zero-width=\"n\" data-slate-length=\"0\">﻿<br></span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">Injection Attacks</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">Cross-Site Scripting (XSS)</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">Denial-of-Service (DoS)</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">Improper Authentication and Authorization</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">Insecure Direct Object References</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">1. Injection Vulnerabilities</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">Node applications are vulnerable to injection attacks, such as SQL injection, NoSQL injection, and Command Injection.</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-zero-width=\"n\" data-slate-length=\"0\">﻿<br></span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">These types of attacks occur when an attacker inputs malicious code into a vulnerable application and the application executes it.</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-zero-width=\"n\" data-slate-length=\"0\">﻿<br></span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">An injection vulnerability might be a SQL injection, when untrusted data is concatenated into a SQL query. An attacker can inject malicious code into the query, which can then be executed by the database.</span></span></span></p><p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-zero-width=\"n\" data-slate-length=\"0\">﻿<br></span></span></span></p>','2023-05-20 00:00:00','2023-05-20 00:00:00','/media/blogs/5 Common Server Vulnerabilities with Node.js.png','/media/blogs/5 Common Server Vulnerabilities with Node.js.mp4','[]',1);
/*!40000 ALTER TABLE `Blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CardPayment`
--

DROP TABLE IF EXISTS `CardPayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CardPayment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(512) NOT NULL,
  `lastName` varchar(512) DEFAULT NULL,
  `addressLine1` varchar(512) NOT NULL,
  `addressLine2` varchar(512) DEFAULT NULL,
  `zipCode` varchar(512) NOT NULL,
  `country` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CardPayment`
--

LOCK TABLES `CardPayment` WRITE;
/*!40000 ALTER TABLE `CardPayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `CardPayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CartItem`
--

DROP TABLE IF EXISTS `CartItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CartItem` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `item` int NOT NULL,
  `type` varchar(512) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `inCart` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CartItem`
--

LOCK TABLES `CartItem` WRITE;
/*!40000 ALTER TABLE `CartItem` DISABLE KEYS */;
INSERT INTO `CartItem` VALUES (1,2,1,'course','2023-05-12 16:36:21','2023-05-12 16:36:21',1,0),(2,2,1,'shopItem','2023-05-13 11:16:19','2023-05-13 11:16:19',4,0),(3,2,2,'course','2023-05-14 02:19:53','2023-05-14 02:30:52',1,0),(5,4,2,'course','2023-05-14 02:31:42','2023-06-19 13:03:01',1,0),(7,2,1,'globalTest','2023-05-14 03:05:13','2023-05-14 03:05:13',1,0),(8,2,1,'globalTest','2023-05-14 03:07:04','2023-05-14 03:07:04',1,0),(9,4,1,'globalTest','2023-05-14 03:25:02','2023-05-14 03:25:02',1,0),(10,2,3,'globalTest','2023-05-15 16:25:41','2023-05-15 16:25:41',1,0),(11,2,3,'course','2023-05-15 16:31:50','2023-05-15 16:31:50',1,0),(12,4,3,'course','2023-05-15 16:32:51','2023-05-15 16:32:51',1,0),(13,5,1,'course','2023-05-18 10:55:19','2023-05-18 10:55:19',1,0),(14,5,3,'globalTest','2023-05-18 11:02:43','2023-05-18 11:02:43',1,0),(15,5,2,'course','2023-05-18 12:30:53','2023-05-18 12:30:53',1,1),(16,7,3,'globalTest','2023-05-18 15:38:20','2023-05-18 15:38:20',1,0),(17,7,1,'shopItem','2023-05-18 15:43:04','2023-05-18 15:43:04',1,0),(18,7,6,'course','2023-05-18 16:10:10','2023-05-18 16:10:10',1,0),(20,4,1,'shopItem','2023-05-20 13:26:33','2023-05-20 13:26:33',1,0),(21,9,2,'course','2023-06-13 06:36:06','2023-06-13 06:36:06',1,1),(24,10,6,'course','2023-06-16 11:57:52','2023-06-16 11:57:52',1,0),(25,15,2,'course','2023-06-18 17:56:59','2023-06-18 17:56:59',1,0),(26,15,5,'course','2023-06-18 19:09:36','2023-06-18 19:09:36',1,0),(28,3,1,'shopItem','2023-06-19 12:49:10','2023-06-19 12:49:10',1,1),(32,18,2,'course','2023-06-21 11:15:06','2023-08-15 12:41:49',1,0),(33,18,1,'course','2023-06-21 11:28:32','2023-06-21 11:28:32',2,0),(34,18,3,'course','2023-06-21 11:38:25','2023-06-21 11:38:25',1,0),(35,18,4,'course','2023-06-21 11:38:33','2023-06-21 11:38:33',1,0),(36,19,1,'shopItem','2023-06-21 11:53:32','2023-06-21 11:53:32',1,1),(37,18,6,'course','2023-06-21 14:13:12','2023-06-21 14:13:12',1,0),(38,18,5,'course','2023-06-22 11:39:03','2023-06-22 11:39:03',1,0),(39,15,6,'course','2023-06-26 09:11:34','2023-06-26 09:11:34',1,0),(40,21,3,'globalTest','2023-06-27 06:48:30','2023-06-27 06:48:30',1,0),(41,21,3,'globalTest','2023-06-27 07:21:28','2023-06-27 07:21:28',1,0),(42,21,3,'globalTest','2023-06-27 07:23:04','2023-06-27 07:23:04',1,0),(44,22,6,'course','2023-07-06 13:54:37','2023-07-06 13:54:37',1,0),(45,22,1,'shopItem','2023-07-06 14:09:01','2023-07-06 14:09:01',1,0),(46,22,3,'globalTest','2023-07-06 14:15:51','2023-07-06 14:15:51',1,0),(47,24,6,'course','2023-07-06 14:41:44','2023-07-06 14:41:44',1,0),(48,24,1,'shopItem','2023-07-06 14:46:08','2023-07-06 14:46:08',1,0),(50,18,3,'globalTest','2023-07-30 07:13:00','2023-07-30 07:13:00',1,0),(51,25,1,'shopItem','2023-07-30 15:04:01','2023-07-30 18:59:34',2,0),(52,25,2,'course','2023-08-02 20:21:16','2023-08-02 20:21:16',2,1),(53,26,3,'course','2023-08-15 06:21:29','2023-08-15 06:21:29',1,1),(54,18,7,'globalTest','2023-08-15 12:42:35','2023-08-15 12:42:35',1,0);
/*!40000 ALTER TABLE `CartItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `coursesCount` int DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `coursesEnrollment` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Category`
--

LOCK TABLES `Category` WRITE;
/*!40000 ALTER TABLE `Category` DISABLE KEYS */;
INSERT INTO `Category` VALUES (1,'Web Development','<p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">web development description</span></span></span></p>',0,'2023-05-12 00:00:00','2023-05-12 00:00:00',0),(2,'Mobile Development','<p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">description</span></span></span></p>',0,'2023-05-12 00:00:00','2023-05-12 00:00:00',0),(3,'Social Media Marketing','<p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-string=\"true\">desc</span></span></span></p>',0,'2023-05-12 00:00:00','2023-05-12 00:00:00',0);
/*!40000 ALTER TABLE `Category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChatGroup`
--

DROP TABLE IF EXISTS `ChatGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ChatGroup` (
  `id` int NOT NULL AUTO_INCREMENT,
  `users` text NOT NULL,
  `messages` text,
  `name` varchar(512) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `user` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChatGroup`
--

LOCK TABLES `ChatGroup` WRITE;
/*!40000 ALTER TABLE `ChatGroup` DISABLE KEYS */;
INSERT INTO `ChatGroup` VALUES (1,'[3,2]','[]','Vrc 1','2023-05-14 04:40:54','2023-05-14 04:40:54',0),(2,'[8,7,2,1]','[]','Vrc 1','2023-05-18 15:35:29','2023-05-18 15:35:29',0),(3,'[8,2]','[]','Vrc 2','2023-05-19 20:43:00','2023-05-19 20:43:00',3),(4,'[3,2]','[]','Vrc 3','2023-05-19 21:36:46','2023-05-19 21:36:46',3);
/*!40000 ALTER TABLE `ChatGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChatMessage`
--

DROP TABLE IF EXISTS `ChatMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ChatMessage` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` varchar(512) NOT NULL,
  `user` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `type` varchar(512) NOT NULL,
  `chatGroup` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChatMessage`
--

LOCK TABLES `ChatMessage` WRITE;
/*!40000 ALTER TABLE `ChatMessage` DISABLE KEYS */;
INSERT INTO `ChatMessage` VALUES (1,'poll',8,'2023-05-19 21:07:54','2023-05-19 21:07:54','poll',3),(2,'Ok I\"ll answer this question can i have a video to explain it',2,'2023-05-19 21:07:54','2023-05-19 21:07:54','message',3);
/*!40000 ALTER TABLE `ChatMessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChatMessagePoll`
--

DROP TABLE IF EXISTS `ChatMessagePoll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ChatMessagePoll` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(512) NOT NULL,
  `message` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChatMessagePoll`
--

LOCK TABLES `ChatMessagePoll` WRITE;
/*!40000 ALTER TABLE `ChatMessagePoll` DISABLE KEYS */;
INSERT INTO `ChatMessagePoll` VALUES (1,'What is the correct Syntax to define a number in c++',1,'2023-05-19 21:10:34','2023-05-19 21:10:34');
/*!40000 ALTER TABLE `ChatMessagePoll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChatMessagePollAnswer`
--

DROP TABLE IF EXISTS `ChatMessagePollAnswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ChatMessagePollAnswer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` varchar(512) NOT NULL,
  `isTrue` tinyint(1) NOT NULL,
  `chatMessagePoll` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChatMessagePollAnswer`
--

LOCK TABLES `ChatMessagePollAnswer` WRITE;
/*!40000 ALTER TABLE `ChatMessagePollAnswer` DISABLE KEYS */;
INSERT INTO `ChatMessagePollAnswer` VALUES (1,'var a = 1;',0,1,'2023-05-19 21:13:07','2023-05-19 21:13:07'),(2,'number a = 1;',0,1,'2023-05-19 21:13:07','2023-05-19 21:13:07'),(3,'int a = 1;',1,1,'2023-05-19 21:13:07','2023-05-19 21:13:07'),(4,'a = 1;',0,1,'2023-05-19 21:13:07','2023-05-19 21:13:07');
/*!40000 ALTER TABLE `ChatMessagePollAnswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ChatMessagePollStudentAnswer`
--

DROP TABLE IF EXISTS `ChatMessagePollStudentAnswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ChatMessagePollStudentAnswer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `chatMessagePoll` int NOT NULL,
  `isTrue` tinyint(1) NOT NULL,
  `chatMessagePollAnswer` int NOT NULL,
  `user` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ChatMessagePollStudentAnswer`
--

LOCK TABLES `ChatMessagePollStudentAnswer` WRITE;
/*!40000 ALTER TABLE `ChatMessagePollStudentAnswer` DISABLE KEYS */;
INSERT INTO `ChatMessagePollStudentAnswer` VALUES (1,1,0,1,2,'2023-05-19 21:16:15','2023-05-19 21:16:15');
/*!40000 ALTER TABLE `ChatMessagePollStudentAnswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ColumnAnswer`
--

DROP TABLE IF EXISTS `ColumnAnswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ColumnAnswer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `test` int NOT NULL,
  `value` varchar(512) NOT NULL,
  `oppositeValue` varchar(512) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ColumnAnswer`
--

LOCK TABLES `ColumnAnswer` WRITE;
/*!40000 ALTER TABLE `ColumnAnswer` DISABLE KEYS */;
INSERT INTO `ColumnAnswer` VALUES (1,6,'a','a','2023-05-14 04:40:54','2023-05-14 04:40:54'),(2,6,'b','b','2023-05-14 04:40:54','2023-05-14 04:40:54'),(3,6,'c','c','2023-05-14 04:40:54','2023-05-14 04:40:54'),(4,6,'d','d','2023-05-14 04:40:54','2023-05-14 04:40:54'),(5,8,'c','c','2023-05-14 04:40:54','2023-05-14 04:40:54'),(6,8,'d','d','2023-05-14 04:40:54','2023-05-14 04:40:54'),(7,8,'a','a','2023-05-14 04:40:54','2023-05-14 04:40:54'),(8,8,'b','b','2023-05-14 04:40:54','2023-05-14 04:40:54'),(9,10,'tt','tt','2023-06-18 20:02:25','2023-06-18 20:02:25'),(10,10,'66','66','2023-06-18 20:02:25','2023-06-18 20:02:25'),(11,10,'11','11','2023-06-18 20:02:25','2023-06-18 20:02:25'),(12,10,'77','77','2023-06-18 20:02:25','2023-06-18 20:02:25'),(13,11,'1','1','2023-08-13 09:21:34','2023-08-13 09:21:34'),(14,11,'2','2','2023-08-13 09:21:34','2023-08-13 09:21:34');
/*!40000 ALTER TABLE `ColumnAnswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Course`
--

DROP TABLE IF EXISTS `Course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Course` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `mobileDescription` longtext NOT NULL,
  `webDescription` longtext NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `categories` text NOT NULL,
  `instructor` int NOT NULL,
  `courseImage` varchar(512) DEFAULT NULL,
  `isApproved` tinyint(1) DEFAULT NULL,
  `isReviewed` tinyint(1) DEFAULT NULL,
  `isPrimary` tinyint(1) DEFAULT NULL,
  `price` int NOT NULL,
  `overviewImage` varchar(512) DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `descriptionVideo` varchar(512) DEFAULT NULL,
  `descriptionImage` varchar(512) DEFAULT NULL,
  `enrollmentNumber` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Course`
--

LOCK TABLES `Course` WRITE;
/*!40000 ALTER TABLE `Course` DISABLE KEYS */;
INSERT INTO `Course` VALUES (1,'Course 1','Descriptionn','Descriptionn','2023-05-12 15:10:26','2023-05-12 15:10:26','[1]',1,'/media/courses/images/Course1_course_image_7962.jpeg',0,0,0,22,NULL,NULL,'/media/courses/description_videos/Course1_description_video10624.mp4','/media/courses/description_images/Course1_description_image15908.jpeg',0),(2,'course 2','description','description','2023-05-13 10:56:03','2023-05-13 10:56:03','[2]',1,'/media/courses/images/course2_course_image_20113.jpeg',0,0,0,33,NULL,NULL,'/media/courses/description_videos/course2_description_video16474.mp4','/media/courses/description_images/course2_description_image9592.jpeg',0),(3,'Course 3','Description','Description','2023-05-14 02:56:23','2023-05-14 02:56:23','[3]',1,'/media/courses/images/Course3_course_image_1794.jpeg',0,0,0,50,NULL,NULL,'/media/courses/description_videos/Course3_description_video18809.mp4','/media/courses/description_images/Course3_description_image14544.jpeg',0),(4,'Course Title','Description','Description','2023-05-18 14:17:10','2023-05-18 14:17:10','[2]',1,'/media/courses/images/CourseTitle_course_image_4126.jpeg',0,0,0,50,NULL,NULL,'/media/courses/description_videos/CourseTitle_description_video16295.mp4','/media/courses/description_images/CourseTitle_description_image11920.jpeg',0),(5,'aaa','dessscriptio','dessscriptio','2023-05-18 14:36:51','2023-05-18 14:36:51','[1]',1,'/media/courses/images/aaa_course_image_16487.jpeg',0,0,0,11,NULL,NULL,'/media/courses/description_videos/aaa_description_video18597.mp4','/media/courses/description_images/aaa_description_image19299.jpeg',0),(6,'course audio','description','description','2023-05-18 14:52:27','2023-05-18 14:52:27','[3]',1,'/media/courses/images/courseaudio_course_image_6444.jpeg',0,0,0,222,NULL,NULL,'/media/courses/description_videos/courseaudio_description_video4810.mp4','/media/courses/description_images/courseaudio_description_image11842.jpeg',0),(7,'New Course Test ','test','testsdf','2023-07-30 18:23:44','2023-07-30 18:23:44','[2,1,3]',11,'/media/courses/images/NewCourseTest_course_image_6815.png',0,0,0,200,'/media/courses/overview_images/NewCourseTest_overview_image20396.png',0,'/media/courses/description_videos/NewCourseTest_description_video10252.mp4','',0);
/*!40000 ALTER TABLE `Course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Enrollment`
--

DROP TABLE IF EXISTS `Enrollment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Enrollment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `course` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Enrollment`
--

LOCK TABLES `Enrollment` WRITE;
/*!40000 ALTER TABLE `Enrollment` DISABLE KEYS */;
INSERT INTO `Enrollment` VALUES (1,2,1),(2,2,2),(3,4,2),(4,2,3),(5,4,3),(6,5,1),(7,7,6),(8,15,2),(9,15,5),(10,10,6),(11,18,1),(12,18,2),(13,18,3),(14,18,4),(15,18,6),(16,18,5),(17,15,6),(18,22,6),(19,24,6),(20,25,2);
/*!40000 ALTER TABLE `Enrollment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Faq`
--

DROP TABLE IF EXISTS `Faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Faq` (
  `id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(512) NOT NULL,
  `answer` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Faq`
--

LOCK TABLES `Faq` WRITE;
/*!40000 ALTER TABLE `Faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `Faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `File`
--

DROP TABLE IF EXISTS `File`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `File` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) DEFAULT NULL,
  `url` varchar(512) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `File`
--

LOCK TABLES `File` WRITE;
/*!40000 ALTER TABLE `File` DISABLE KEYS */;
/*!40000 ALTER TABLE `File` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GlobalPayment`
--

DROP TABLE IF EXISTS `GlobalPayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GlobalPayment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `isPaypal` tinyint(1) NOT NULL,
  `cardPayment` int NOT NULL,
  `transferPayment` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GlobalPayment`
--

LOCK TABLES `GlobalPayment` WRITE;
/*!40000 ALTER TABLE `GlobalPayment` DISABLE KEYS */;
INSERT INTO `GlobalPayment` VALUES (1,1,0,0),(2,1,0,0),(3,1,0,0),(4,1,0,0),(5,1,0,0),(6,1,0,0),(7,1,0,0),(8,1,0,0),(9,1,0,0),(10,1,0,0),(11,1,0,0),(12,1,0,0),(13,1,0,0),(14,1,0,0),(15,1,0,0),(16,1,0,0),(17,1,0,0),(18,1,0,0),(19,1,0,0),(20,1,0,0),(21,1,0,0),(22,1,0,0),(23,1,0,0),(24,1,0,0),(25,0,0,1),(26,0,0,3),(27,0,0,4),(28,0,0,5),(29,0,0,6),(30,0,0,7),(31,1,0,0),(32,1,0,0),(33,1,0,0),(34,1,0,0),(35,1,0,0),(36,1,0,0),(37,1,0,0),(38,1,0,0),(39,1,0,0),(40,1,0,0),(41,1,0,0),(42,1,0,0),(43,1,0,0),(44,1,0,0),(45,1,0,0),(46,1,0,0),(47,1,0,0),(48,0,0,8),(49,1,0,0),(50,1,0,0),(51,0,0,9),(52,1,0,0),(53,0,0,10),(54,0,0,11),(55,1,0,0),(56,1,0,0),(57,1,0,0),(58,1,0,0),(59,1,0,0),(60,1,0,0),(61,1,0,0);
/*!40000 ALTER TABLE `GlobalPayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GlobalStudentTest`
--

DROP TABLE IF EXISTS `GlobalStudentTest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GlobalStudentTest` (
  `id` int NOT NULL AUTO_INCREMENT,
  `globalTest` int NOT NULL,
  `user` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GlobalStudentTest`
--

LOCK TABLES `GlobalStudentTest` WRITE;
/*!40000 ALTER TABLE `GlobalStudentTest` DISABLE KEYS */;
INSERT INTO `GlobalStudentTest` VALUES (1,1,2,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(2,1,4,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(3,3,2,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(4,3,5,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(5,3,7,'2023-05-18 15:35:29','2023-05-18 15:35:29'),(6,1,4,'2023-05-19 21:36:46','2023-05-19 21:36:46'),(7,1,4,'2023-06-09 15:55:13','2023-06-09 15:55:13'),(8,6,4,'2023-06-20 11:53:29','2023-06-20 11:53:29'),(9,2,4,'2023-06-20 11:53:29','2023-06-20 11:53:29'),(10,3,21,'2023-06-27 07:22:19','2023-06-27 07:22:19'),(11,3,18,'2023-07-30 07:00:29','2023-07-30 07:00:29'),(12,3,18,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(13,7,18,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(14,1,18,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(15,1,18,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(16,1,18,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(17,1,18,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(18,5,18,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(19,3,18,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(20,3,18,'2023-08-13 09:21:34','2023-08-13 09:21:34');
/*!40000 ALTER TABLE `GlobalStudentTest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GlobalTest`
--

DROP TABLE IF EXISTS `GlobalTest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GlobalTest` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `user` int NOT NULL,
  `price` int NOT NULL,
  `course` int NOT NULL,
  `lesson` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GlobalTest`
--

LOCK TABLES `GlobalTest` WRITE;
/*!40000 ALTER TABLE `GlobalTest` DISABLE KEYS */;
INSERT INTO `GlobalTest` VALUES (1,'Test 1','Test Description',3,0,3,5,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(2,'Test 2','Test 2 description',3,0,3,6,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(3,'Global Test 2','Description',3,0,0,0,'2023-06-18 20:02:25','2023-06-18 20:02:25'),(4,'Test 2','Test 2 Description',3,0,4,7,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(5,'Test 1','Test Description',3,0,4,10,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(6,'gdfg','hdfh',3,0,0,0,'2023-06-18 20:02:25','2023-06-18 20:02:25'),(7,'New Test','New Test',3,1000,0,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(8,'test123','dsadas',3,123,0,0,'2023-08-13 09:21:34','2023-08-13 09:21:34');
/*!40000 ALTER TABLE `GlobalTest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GlobalTestEnrollment`
--

DROP TABLE IF EXISTS `GlobalTestEnrollment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `GlobalTestEnrollment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `globalTest` int NOT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GlobalTestEnrollment`
--

LOCK TABLES `GlobalTestEnrollment` WRITE;
/*!40000 ALTER TABLE `GlobalTestEnrollment` DISABLE KEYS */;
INSERT INTO `GlobalTestEnrollment` VALUES (1,18,3,'2023-07-30T07:00:29.300Z'),(2,18,7,'2023-08-13T09:21:34.152Z');
/*!40000 ALTER TABLE `GlobalTestEnrollment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Instructor`
--

DROP TABLE IF EXISTS `Instructor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Instructor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `bio` longtext,
  `education` varchar(512) DEFAULT NULL,
  `isApproved` tinyint(1) DEFAULT NULL,
  `isReviewed` tinyint(1) DEFAULT NULL,
  `isPrimary` tinyint(1) DEFAULT NULL,
  `isBlocked` tinyint(1) DEFAULT NULL,
  `blockEndingLimit` varchar(512) DEFAULT NULL,
  `blockMessage` varchar(512) DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `coursesEnrollment` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Instructor`
--

LOCK TABLES `Instructor` WRITE;
/*!40000 ALTER TABLE `Instructor` DISABLE KEYS */;
INSERT INTO `Instructor` VALUES (1,3,'2023-06-20 13:26:26','2023-06-20 10:30:37','I am a teacher','',0,1,1,0,NULL,'',0,0),(2,6,'2023-05-18 12:33:02','2023-05-18 12:33:02','Looking forward to teach all','',0,0,0,0,'','',0,0),(3,8,'2023-05-18 16:16:20','2023-05-18 16:16:20','I Am A Teacher','',1,1,0,0,'','',0,0),(4,12,'2023-06-13 19:29:11','2023-06-20 09:16:30','testing bio','',0,0,0,0,'','',0,0),(5,4,'2023-06-14 17:49:15','2023-06-14 17:49:15','bio bio','edu',0,0,0,0,NULL,'',0,0),(6,13,'2023-06-14 15:05:45','2023-06-14 15:05:45','bio','',0,0,0,0,'','',0,0),(7,14,'2023-06-14 15:11:24','2023-06-14 15:11:24','','',0,0,0,0,'','',0,0),(8,15,'2023-06-16 14:55:47','2023-06-16 14:55:47','','',0,0,0,0,'','',0,0),(9,24,'2023-07-06 14:51:53','2023-07-06 14:51:53','','',1,1,0,0,'','',0,0),(10,21,'2023-07-30 06:49:34','2023-07-30 06:49:34','','',1,1,1,0,'','',0,0),(11,25,'2023-07-30 15:39:46','2023-07-31 21:52:01','','',0,0,0,0,'','',0,0),(12,27,'2023-08-16 14:10:15','2023-08-16 14:17:35','','',0,0,0,0,'','',0,0);
/*!40000 ALTER TABLE `Instructor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Lesson`
--

DROP TABLE IF EXISTS `Lesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Lesson` (
  `id` int NOT NULL AUTO_INCREMENT,
  `section` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `type` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lesson`
--

LOCK TABLES `Lesson` WRITE;
/*!40000 ALTER TABLE `Lesson` DISABLE KEYS */;
INSERT INTO `Lesson` VALUES (1,1,'2023-05-08 00:00:21','2023-05-08 00:00:21','asset'),(2,2,'2023-05-08 00:00:21','2023-05-08 00:00:21','video'),(3,3,'2023-05-12 17:20:00','2023-05-12 17:20:00','video'),(4,4,'2023-05-12 17:20:00','2023-05-12 17:20:00','asset'),(5,5,'2023-05-14 02:40:23','2023-05-14 02:40:23','test'),(6,6,'2023-05-14 02:40:23','2023-05-14 02:40:23','test'),(7,7,'2023-05-14 04:40:54','2023-05-14 04:40:54','test'),(8,7,'2023-05-14 04:40:54','2023-05-14 04:40:54','asset'),(9,8,'2023-05-14 04:40:54','2023-05-14 04:40:54','video'),(10,8,'2023-05-14 04:40:54','2023-05-14 04:40:54','test'),(11,9,'2023-05-14 04:40:54','2023-05-14 04:40:54','video'),(12,10,'2023-05-14 04:40:54','2023-05-14 04:40:54','asset'),(13,10,'2023-05-14 04:40:54','2023-05-14 04:40:54','video'),(14,11,'2023-05-14 04:40:54','2023-05-14 04:40:54','audio'),(15,12,'2023-05-14 04:40:54','2023-05-14 04:40:54','asset'),(16,13,'2023-07-30 10:43:48','2023-07-30 10:43:48','video'),(17,13,'2023-07-30 10:43:48','2023-07-30 10:43:48','video');
/*!40000 ALTER TABLE `Lesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LessonAsset`
--

DROP TABLE IF EXISTS `LessonAsset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LessonAsset` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lesson` int NOT NULL,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `fileUrl` varchar(512) NOT NULL,
  `type` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LessonAsset`
--

LOCK TABLES `LessonAsset` WRITE;
/*!40000 ALTER TABLE `LessonAsset` DISABLE KEYS */;
INSERT INTO `LessonAsset` VALUES (1,1,'Lesson 2','Description','/media/courses/assets/Lesson2_lesson_asset_url8566.pdf','pdf'),(2,4,'lesson 2','description','/media/courses/assets/lesson2_lesson_asset_url5758.pdf','pdf'),(3,8,'Asset Lesson','Lesson Description','/media/courses/assets/AssetLesson_lesson_asset_url8258.pdf','pdf'),(4,12,'lesson 11123','dddd','/media/courses/assets/lesson11123_lesson_asset_url18628.pdf','pdf'),(5,15,'ddd','desss','/media/courses/assets/ddd_lesson_asset_url8419.pdf','pdf');
/*!40000 ALTER TABLE `LessonAsset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LiveStream`
--

DROP TABLE IF EXISTS `LiveStream`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LiveStream` (
  `id` int NOT NULL AUTO_INCREMENT,
  `startDate` datetime NOT NULL,
  `name` varchar(512) NOT NULL,
  `user` int DEFAULT NULL,
  `description` longtext NOT NULL,
  `isPrivate` tinyint(1) NOT NULL,
  `isLive` tinyint(1) DEFAULT NULL,
  `isFromAdmin` tinyint(1) DEFAULT NULL,
  `password` varchar(512) DEFAULT NULL,
  `coverImage` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LiveStream`
--

LOCK TABLES `LiveStream` WRITE;
/*!40000 ALTER TABLE `LiveStream` DISABLE KEYS */;
/*!40000 ALTER TABLE `LiveStream` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LocalPayment`
--

DROP TABLE IF EXISTS `LocalPayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LocalPayment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `isEpay` tinyint(1) NOT NULL,
  `transferPayment` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LocalPayment`
--

LOCK TABLES `LocalPayment` WRITE;
/*!40000 ALTER TABLE `LocalPayment` DISABLE KEYS */;
INSERT INTO `LocalPayment` VALUES (1,0,2);
/*!40000 ALTER TABLE `LocalPayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Notification`
--

DROP TABLE IF EXISTS `Notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Notification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` varchar(512) NOT NULL,
  `user` int NOT NULL,
  `isRead` tinyint(1) DEFAULT NULL,
  `type` varchar(512) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Notification`
--

LOCK TABLES `Notification` WRITE;
/*!40000 ALTER TABLE `Notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `Notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Order`
--

DROP TABLE IF EXISTS `Order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cartItems` text NOT NULL,
  `paymentMethod` int NOT NULL,
  `user` int NOT NULL,
  `paymentApproved` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Order`
--

LOCK TABLES `Order` WRITE;
/*!40000 ALTER TABLE `Order` DISABLE KEYS */;
INSERT INTO `Order` VALUES (1,'[1]',1,2,1,'2023-05-12 15:33:27','2023-05-12 15:33:27'),(2,'[2]',2,2,1,'2023-05-12 17:20:00','2023-05-12 17:20:00'),(3,'[2]',3,2,1,'2023-05-12 17:20:00','2023-05-12 17:20:00'),(4,'[2]',4,2,1,'2023-05-13 11:44:51','2023-05-13 11:44:51'),(5,'[3]',5,2,0,'2023-05-13 11:44:51','2023-05-13 11:44:51'),(6,'[3]',6,2,0,'2023-05-13 11:44:51','2023-05-13 11:44:51'),(7,'[3]',7,2,1,'2023-05-13 11:44:51','2023-05-13 11:44:51'),(8,'[5]',8,4,1,'2023-05-13 11:44:51','2023-05-13 11:44:51'),(9,'[6]',9,4,1,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(10,'[7]',10,2,0,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(11,'[8]',11,2,1,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(12,'[9]',12,4,1,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(13,'[10]',13,2,1,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(14,'[11]',14,2,1,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(15,'[12]',15,4,1,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(16,'[13]',16,5,1,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(17,'[14]',17,5,1,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(18,'[16]',18,7,1,'2023-05-18 15:35:29','2023-05-18 15:35:29'),(19,'[17]',19,7,1,'2023-05-18 15:35:29','2023-05-18 15:35:29'),(20,'[18]',20,7,1,'2023-05-18 15:35:29','2023-05-18 15:35:29'),(21,'[22]',21,3,1,'2023-06-09 15:55:13','2023-06-09 15:55:13'),(22,'[25]',22,15,1,'2023-06-09 15:55:13','2023-06-09 15:55:13'),(23,'[26]',23,15,1,'2023-06-09 15:55:13','2023-06-09 15:55:13'),(24,'[24]',24,10,1,'2023-06-18 20:02:25','2023-06-18 20:02:25'),(25,'[20]',25,4,0,'2023-06-20 11:36:39','2023-06-20 11:36:39'),(26,'[30]',26,4,0,'2023-06-20 11:53:29','2023-06-20 11:53:29'),(27,'[29]',27,18,0,'2023-06-21 10:48:49','2023-06-21 10:48:49'),(28,'[29]',28,18,0,'2023-06-21 10:48:49','2023-06-21 10:48:49'),(29,'[32]',29,18,0,'2023-06-21 10:48:49','2023-06-21 10:48:49'),(30,'[29]',30,18,0,'2023-06-21 10:48:49','2023-06-21 10:48:49'),(31,'[33]',31,18,0,'2023-06-21 11:24:57','2023-06-21 11:24:57'),(32,'[33]',32,18,1,'2023-06-21 11:33:18','2023-06-21 11:33:18'),(33,'[32,34,35]',33,18,1,'2023-06-21 11:37:42','2023-06-21 11:37:42'),(34,'[29]',34,18,1,'2023-06-21 11:50:43','2023-06-21 11:50:43'),(35,'[37]',35,18,1,'2023-06-21 11:50:43','2023-06-21 11:50:43'),(36,'[38]',36,18,1,'2023-06-21 11:50:43','2023-06-21 11:50:43'),(37,'[39]',37,15,1,'2023-06-25 16:41:50','2023-06-25 16:41:50'),(38,'[40]',38,21,1,'2023-06-27 06:35:05','2023-06-27 06:35:05'),(39,'[41]',39,21,1,'2023-06-27 06:35:05','2023-06-27 06:35:05'),(40,'[42]',40,21,1,'2023-06-27 07:22:19','2023-06-27 07:22:19'),(41,'[44]',41,22,1,'2023-06-27 07:53:21','2023-06-27 07:53:21'),(42,'[45]',42,22,1,'2023-06-27 07:53:21','2023-06-27 07:53:21'),(43,'[46]',43,22,1,'2023-06-27 07:53:21','2023-06-27 07:53:21'),(44,'[47]',44,24,1,'2023-06-27 07:53:21','2023-06-27 07:53:21'),(45,'[48]',45,24,1,'2023-06-27 07:53:21','2023-06-27 07:53:21'),(46,'[50]',46,18,1,'2023-07-30 07:00:29','2023-07-30 07:00:29'),(47,'[51]',47,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(48,'[51]',48,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(49,'[51]',49,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(50,'[51]',50,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(51,'[51]',51,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(52,'[51]',52,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(53,'[51]',53,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(54,'[51]',54,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(55,'[51]',55,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(56,'[51]',56,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(57,'[51]',57,25,0,'2023-07-30 10:43:48','2023-07-30 10:43:48'),(58,'[51]',58,25,0,'2023-07-30 18:48:08','2023-07-30 18:48:08'),(59,'[52]',59,25,1,'2023-07-30 18:48:08','2023-07-30 18:48:08'),(60,'[51]',60,25,1,'2023-07-30 18:48:08','2023-07-30 18:48:08'),(61,'[32]',61,18,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(62,'[54]',62,18,1,'2023-08-13 09:21:34','2023-08-13 09:21:34');
/*!40000 ALTER TABLE `Order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PaymentMethod`
--

DROP TABLE IF EXISTS `PaymentMethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PaymentMethod` (
  `id` int NOT NULL AUTO_INCREMENT,
  `globalPayment` int NOT NULL,
  `localPayment` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PaymentMethod`
--

LOCK TABLES `PaymentMethod` WRITE;
/*!40000 ALTER TABLE `PaymentMethod` DISABLE KEYS */;
INSERT INTO `PaymentMethod` VALUES (1,1,0),(2,2,0),(3,3,0),(4,4,0),(5,5,0),(6,6,0),(7,7,0),(8,8,0),(9,9,0),(10,10,0),(11,11,0),(12,12,0),(13,13,0),(14,14,0),(15,15,0),(16,16,0),(17,17,0),(18,18,0),(19,19,0),(20,20,0),(21,21,0),(22,22,0),(23,23,0),(24,24,0),(25,25,0),(26,0,1),(27,26,0),(28,27,0),(29,28,0),(30,29,0),(31,30,0),(32,31,0),(33,32,0),(34,33,0),(35,34,0),(36,35,0),(37,36,0),(38,37,0),(39,38,0),(40,39,0),(41,40,0),(42,41,0),(43,42,0),(44,43,0),(45,44,0),(46,45,0),(47,46,0),(48,47,0),(49,48,0),(50,49,0),(51,50,0),(52,51,0),(53,52,0),(54,53,0),(55,54,0),(56,55,0),(57,56,0),(58,57,0),(59,58,0),(60,59,0),(61,60,0),(62,61,0);
/*!40000 ALTER TABLE `PaymentMethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Review`
--

DROP TABLE IF EXISTS `Review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Review` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `course` int NOT NULL,
  `rating` int NOT NULL,
  `textContent` varchar(512) DEFAULT NULL,
  `voiceContent` varchar(512) DEFAULT NULL,
  `videoContent` varchar(512) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Review`
--

LOCK TABLES `Review` WRITE;
/*!40000 ALTER TABLE `Review` DISABLE KEYS */;
INSERT INTO `Review` VALUES (1,2,3,3,'review 1','','','2023-05-15 16:32:05','2023-05-15 16:32:05'),(2,4,3,4,'review 2','','','2023-05-15 16:33:04','2023-05-15 16:33:04'),(3,4,3,0,'','','/media/reviews/videos/review_video6366.mp4','2023-05-15 16:35:22','2023-05-15 16:35:22'),(4,4,3,0,'','/media/reviews/voices/review_voice8449.mp3','','2023-05-15 16:36:19','2023-05-15 16:36:19'),(5,15,2,3,'','/media/reviews/voices/review_voice18760.mp3','','2023-06-21 11:08:39','2023-06-21 11:08:39'),(6,15,2,0,'','/media/reviews/voices/review_voice16705.mp3','','2023-06-21 11:12:27','2023-06-21 11:12:27'),(7,15,2,0,'','/media/reviews/voices/review_voice8416.mp3','','2023-06-21 11:21:57','2023-06-21 11:21:57'),(8,15,2,0,'','/media/reviews/voices/review_voice19514.mp3','','2023-06-21 11:27:09','2023-06-21 11:27:09'),(9,15,6,3,'','/media/reviews/voices/review_voice12206.mp3','','2023-06-26 09:17:33','2023-06-26 09:17:33'),(10,15,6,0,'','/media/reviews/voices/review_voice8910.aac','','2023-06-26 09:20:07','2023-06-26 09:20:07'),(11,15,6,3,'','/media/reviews/voices/review_voice11463.aac','','2023-06-26 09:58:39','2023-06-26 09:58:39'),(12,18,1,5,'','','','2023-06-26 12:25:51','2023-06-26 12:25:51'),(13,18,1,5,'','','','2023-06-26 12:25:53','2023-06-26 12:25:53'),(14,18,1,5,'','','','2023-06-26 12:25:54','2023-06-26 12:25:54'),(15,18,1,3,'','','','2023-06-26 12:26:09','2023-06-26 12:26:09'),(16,18,1,3,'','','','2023-06-26 12:26:57','2023-06-26 12:26:57'),(17,18,1,0,'','','','2023-06-27 11:34:55','2023-06-27 11:34:55'),(19,18,1,5,'','','/media/reviews/videos/review_video6431.mp4','2023-06-27 11:47:37','2023-06-27 11:47:37'),(20,18,1,0,'','/media/reviews/voices/review_voice19316.wav','','2023-07-03 13:04:24','2023-07-03 13:04:24'),(21,24,6,4,'','/media/reviews/voices/review_voice9213.aac','','2023-07-06 14:47:53','2023-07-06 14:47:53'),(22,24,6,3,'nnnn','/media/reviews/voices/review_voice1372.aac','','2023-07-06 14:48:21','2023-07-06 14:48:21'),(23,24,6,2,'','','/media/reviews/videos/review_video4118.mp4','2023-07-06 14:49:48','2023-07-06 14:49:48'),(24,18,1,4,'','/media/reviews/voices/review_voice14804.wav','','2023-07-25 15:38:02','2023-07-25 15:38:02');
/*!40000 ALTER TABLE `Review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Section`
--

DROP TABLE IF EXISTS `Section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Section` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `isFree` tinyint(1) NOT NULL,
  `course` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Section`
--

LOCK TABLES `Section` WRITE;
/*!40000 ALTER TABLE `Section` DISABLE KEYS */;
INSERT INTO `Section` VALUES (1,'Section 2','Aaaa','2023-05-12 15:10:27','2023-05-12 15:10:27',1,1),(2,'Section 1','Aaaa','2023-05-12 15:10:42','2023-05-12 15:10:42',0,1),(3,'section 11','Aaaa','2023-05-13 10:56:04','2023-05-13 10:56:04',0,2),(4,'section 2','Aaaa','2023-05-13 11:02:31','2023-05-13 11:02:31',1,2),(5,'Section 1','Aaaa','2023-05-14 02:56:25','2023-05-14 02:56:25',1,3),(6,'Section 2','Aaaa','2023-05-14 02:56:34','2023-05-14 02:56:34',0,3),(7,'Section 2','Aaaa','2023-05-18 14:17:11','2023-05-18 14:17:11',0,4),(8,'Section 1','Aaaa','2023-05-18 14:17:45','2023-05-18 14:17:45',1,4),(9,'section 1','Aaaa','2023-05-18 14:36:51','2023-05-18 14:36:51',0,5),(10,'section 2','Aaaa','2023-05-18 14:39:33','2023-05-18 14:39:33',1,5),(11,'Section','Aaaa','2023-05-18 14:52:28','2023-05-18 14:52:28',1,6),(12,'Section 2','Aaaa','2023-05-18 14:52:55','2023-05-18 14:52:55',0,6),(13,'test','testfdsfs','2023-07-30 18:23:45','2023-07-30 18:23:45',0,7);
/*!40000 ALTER TABLE `Section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShopItem`
--

DROP TABLE IF EXISTS `ShopItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ShopItem` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file` varchar(512) NOT NULL,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `shopItemImage` varchar(512) NOT NULL,
  `price` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `previewFile` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShopItem`
--

LOCK TABLES `ShopItem` WRITE;
/*!40000 ALTER TABLE `ShopItem` DISABLE KEYS */;
INSERT INTO `ShopItem` VALUES (1,'/media/shop-items/file.pdf','Learn Eleqount Javascript','<p data-slate-node=\"element\"><span data-slate-node=\"text\"><span data-slate-leaf=\"true\"><span data-slate-placeholder=\"true\" contenteditable=\"false\" style=\"position: absolute; pointer-events: none; width: 100%; max-width: 100%; display: block; opacity: 0.333; user-select: none; text-decoration: none;\">Write here</span><span data-slate-zero-width=\"n\" data-slate-length=\"0\">﻿<br></span></span></span></p>','/media/shop-items/Learn Eleqount Javascript.jpeg',1000,'2023-05-12 00:00:00','2023-05-12 00:00:00','/media/shop-items/file-preview.pdf');
/*!40000 ALTER TABLE `ShopItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShopItemEnrollment`
--

DROP TABLE IF EXISTS `ShopItemEnrollment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ShopItemEnrollment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `shopItem` int NOT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShopItemEnrollment`
--

LOCK TABLES `ShopItemEnrollment` WRITE;
/*!40000 ALTER TABLE `ShopItemEnrollment` DISABLE KEYS */;
INSERT INTO `ShopItemEnrollment` VALUES (1,2,1,'2023-05-13T11:44:50.873Z'),(2,4,1,'2023-05-14T02:40:23.351Z'),(3,7,1,'2023-05-18T15:35:28.767Z'),(4,3,1,'2023-06-09T15:55:13.204Z'),(5,18,1,'2023-06-21T11:50:43.035Z'),(6,22,1,'2023-06-27T07:53:20.710Z'),(7,24,1,'2023-06-27T07:53:20.710Z'),(8,25,1,'2023-07-30T18:48:07.610Z');
/*!40000 ALTER TABLE `ShopItemEnrollment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StaticData`
--

DROP TABLE IF EXISTS `StaticData`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `StaticData` (
  `id` int NOT NULL AUTO_INCREMENT,
  `introDescription` longtext,
  `freeGuideFile` varchar(512) DEFAULT NULL,
  `introVideo` varchar(512) DEFAULT NULL,
  `registeredEnrolls` int DEFAULT NULL,
  `finishedSessions` int DEFAULT NULL,
  `satisfactionRate` int DEFAULT NULL,
  `adminImage` varchar(512) DEFAULT NULL,
  `adminDescription` longtext,
  `yearsInService` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StaticData`
--

LOCK TABLES `StaticData` WRITE;
/*!40000 ALTER TABLE `StaticData` DISABLE KEYS */;
INSERT INTO `StaticData` VALUES (1,'<span class=\'sub-title test\'> UH Training is the best</span><h2 class=\'title\'>In Mentoring <br>2B a Better You.</h2><div class=\'desc\'><p>The right mentoring relationship can be a powerful tool for professional growth. Bark up the right tree.</p></div>','/media/static-data/freeguide.pdf','            <iframe src=\"https://www.youtube.com/embed/2IZKK3pJQjg\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share\" allowfullscreen></iframe>',10,24,14,'/media/static-data/freeguide.png','<div class=\'section-title\'><h2 class=\'title\'>Get rid of <span>Old School Assumptions</span></h2></div><div class=\'row\'><div class=\'quote\'><p>I assist learners in finding new effective belief and get rid of the outdated, discouraged and irrelevant life attitudes. You\'re upgrading to be a better you.</p></div></div>',19);
/*!40000 ALTER TABLE `StaticData` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Student`
--

DROP TABLE IF EXISTS `Student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Student` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Student`
--

LOCK TABLES `Student` WRITE;
/*!40000 ALTER TABLE `Student` DISABLE KEYS */;
INSERT INTO `Student` VALUES (1,2,'2023-05-12 14:21:16','2023-05-12 14:21:16'),(2,4,'2023-05-13 11:54:28','2023-06-20 10:35:55'),(3,5,'2023-05-18 10:52:43','2023-05-18 10:52:43'),(4,7,'2023-05-18 15:37:56','2023-05-18 15:37:56'),(5,9,'2023-06-13 06:34:44','2023-06-13 06:34:44'),(8,14,'2023-06-14 15:06:58','2023-06-14 15:06:58'),(9,15,'2023-06-16 14:46:07','2023-06-16 14:57:18'),(10,16,'2023-06-18 11:09:40','2023-06-18 11:09:40'),(11,17,'2023-06-18 11:09:40','2023-06-18 11:09:40'),(12,3,'2023-06-18 12:59:32','2023-06-18 12:59:32'),(13,18,'2023-06-19 10:43:21','2023-07-25 15:26:45'),(14,19,'2023-06-21 11:51:33','2023-06-21 11:51:33'),(15,20,'2023-06-23 16:52:30','2023-06-23 16:52:30'),(16,21,'2023-06-25 16:53:38','2023-07-30 06:50:02'),(17,22,'2023-07-06 13:43:45','2023-07-06 13:48:54'),(18,23,'2023-07-06 13:43:45','2023-07-06 13:43:45'),(19,24,'2023-07-06 14:39:40','2023-07-06 14:55:50'),(20,25,'2023-07-30 15:02:57','2023-08-02 20:21:06'),(21,26,'2023-08-15 06:18:36','2023-08-15 06:18:36'),(22,27,'2023-08-16 13:59:19','2023-08-16 14:19:59');
/*!40000 ALTER TABLE `Student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StudentColumnAnswer`
--

DROP TABLE IF EXISTS `StudentColumnAnswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `StudentColumnAnswer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `studentTest` int NOT NULL,
  `value` varchar(512) NOT NULL,
  `oppositeValue` varchar(512) NOT NULL,
  `isTrue` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StudentColumnAnswer`
--

LOCK TABLES `StudentColumnAnswer` WRITE;
/*!40000 ALTER TABLE `StudentColumnAnswer` DISABLE KEYS */;
INSERT INTO `StudentColumnAnswer` VALUES (1,7,'b','c',0,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(2,7,'d','b',0,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(3,7,'c','a',0,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(4,7,'a','c',0,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(5,18,'a','a',1,'2023-07-30 07:00:29','2023-07-30 07:00:29'),(6,18,'b','b',1,'2023-07-30 07:00:29','2023-07-30 07:00:29'),(7,18,'c','d',0,'2023-07-30 07:00:29','2023-07-30 07:00:29'),(8,18,'d','c',0,'2023-07-30 07:00:29','2023-07-30 07:00:29');
/*!40000 ALTER TABLE `StudentColumnAnswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StudentLesson`
--

DROP TABLE IF EXISTS `StudentLesson`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `StudentLesson` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `lesson` int NOT NULL,
  `section` int NOT NULL,
  `course` int NOT NULL,
  `createdAt` varchar(512) DEFAULT NULL,
  `updatedAt` varchar(512) DEFAULT NULL,
  `isWatched` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StudentLesson`
--

LOCK TABLES `StudentLesson` WRITE;
/*!40000 ALTER TABLE `StudentLesson` DISABLE KEYS */;
INSERT INTO `StudentLesson` VALUES (1,2,1,1,1,'2023-05-12T16:43:25.397Z','2023-05-12T16:43:25.397Z',1),(2,2,2,2,1,'2023-05-12T16:43:25.401Z','2023-05-12T16:43:25.401Z',1),(3,2,3,3,2,'2023-05-14T02:30:51.801Z','2023-05-14T02:30:51.801Z',1),(4,2,4,4,2,'2023-05-14T02:30:51.805Z','2023-05-14T02:30:51.805Z',1),(5,2,5,5,3,'2023-05-15T16:31:49.727Z','2023-05-15T16:31:49.727Z',1),(6,2,6,6,3,'2023-05-15T16:31:49.732Z','2023-05-15T16:31:49.732Z',1),(7,2,5,5,3,'2023-05-15T16:31:49.727Z','2023-05-15T16:31:49.727Z',0),(8,2,6,6,3,'2023-05-15T16:31:49.732Z','2023-05-15T16:31:49.732Z',0),(9,4,5,5,3,'2023-05-15T16:32:51.134Z','2023-05-15T16:32:51.134Z',0),(10,4,6,6,3,'2023-05-15T16:32:51.137Z','2023-05-15T16:32:51.137Z',0),(11,5,1,1,1,'2023-05-18T10:56:11.505Z','2023-05-18T10:56:11.505Z',0),(12,5,2,2,1,'2023-05-18T10:56:11.508Z','2023-05-18T10:56:11.508Z',0),(13,7,14,11,6,'2023-05-18T16:10:10.083Z','2023-05-18T16:10:10.083Z',0),(15,15,3,3,2,'2023-06-18T17:57:09.960Z','2023-06-18T17:57:09.960Z',0),(16,15,4,4,2,'2023-06-18T17:57:09.964Z','2023-06-18T17:57:09.964Z',0),(17,15,11,9,5,'2023-06-18T19:09:35.734Z','2023-06-18T19:09:35.734Z',0),(18,15,12,10,5,'2023-06-18T19:09:35.739Z','2023-06-18T19:09:35.739Z',0),(19,15,13,10,5,'2023-06-18T19:09:35.740Z','2023-06-18T19:09:35.740Z',0),(20,10,14,11,6,'2023-06-18T20:02:30.085Z','2023-06-18T20:02:30.085Z',1),(21,10,15,12,6,'2023-06-18T20:02:30.099Z','2023-06-18T20:02:30.099Z',0),(22,18,1,1,1,'2023-06-21T11:36:13.227Z','2023-06-21T11:36:13.227Z',1),(23,18,2,2,1,'2023-06-21T11:36:13.232Z','2023-06-21T11:36:13.232Z',1),(24,18,3,3,2,'2023-06-21T11:39:06.497Z','2023-06-21T11:39:06.497Z',1),(25,18,4,4,2,'2023-06-21T11:39:06.503Z','2023-06-21T11:39:06.503Z',1),(26,18,5,5,3,'2023-06-21T11:39:06.513Z','2023-06-21T11:39:06.513Z',1),(27,18,6,6,3,'2023-06-21T11:39:06.517Z','2023-06-21T11:39:06.517Z',0),(28,18,7,7,4,'2023-06-21T11:39:06.533Z','2023-06-21T11:39:06.533Z',0),(29,18,8,7,4,'2023-06-21T11:39:06.536Z','2023-06-21T11:39:06.536Z',1),(30,18,9,8,4,'2023-06-21T11:39:06.540Z','2023-06-21T11:39:06.540Z',1),(31,18,10,8,4,'2023-06-21T11:39:06.542Z','2023-06-21T11:39:06.542Z',1),(32,18,14,11,6,'2023-06-21T14:13:38.686Z','2023-06-21T14:13:38.686Z',1),(33,18,15,12,6,'2023-06-21T14:13:38.690Z','2023-06-21T14:13:38.690Z',0),(34,18,11,9,5,'2023-06-22T11:39:20.398Z','2023-06-22T11:39:20.398Z',1),(35,18,12,10,5,'2023-06-22T11:39:20.404Z','2023-06-22T11:39:20.404Z',1),(36,18,13,10,5,'2023-06-22T11:39:20.406Z','2023-06-22T11:39:20.406Z',1),(37,15,14,11,6,'2023-06-26T09:11:33.878Z','2023-06-26T09:11:33.878Z',0),(38,15,15,12,6,'2023-06-26T09:11:33.883Z','2023-06-26T09:11:33.883Z',0),(39,22,14,11,6,'2023-07-06T13:55:55.678Z','2023-07-06T13:55:55.678Z',1),(40,22,15,12,6,'2023-07-06T13:55:55.698Z','2023-07-06T13:55:55.698Z',1),(41,24,14,11,6,'2023-07-06T14:43:44.684Z','2023-07-06T14:43:44.684Z',1),(42,24,15,12,6,'2023-07-06T14:43:44.687Z','2023-07-06T14:43:44.687Z',1),(43,25,3,3,2,'2023-08-02T20:21:26.823Z','2023-08-02T20:21:26.823Z',0),(44,25,4,4,2,'2023-08-02T20:21:26.828Z','2023-08-02T20:21:26.828Z',1);
/*!40000 ALTER TABLE `StudentLesson` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StudentOptionalAnswer`
--

DROP TABLE IF EXISTS `StudentOptionalAnswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `StudentOptionalAnswer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `studentTest` int NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `isTrue` tinyint(1) NOT NULL,
  `answerId` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StudentOptionalAnswer`
--

LOCK TABLES `StudentOptionalAnswer` WRITE;
/*!40000 ALTER TABLE `StudentOptionalAnswer` DISABLE KEYS */;
INSERT INTO `StudentOptionalAnswer` VALUES (1,1,'2023-05-14 02:40:23','2023-05-14 02:40:23',1,0),(2,4,'2023-05-14 04:40:54','2023-05-14 04:40:54',1,0),(3,6,'2023-05-14 04:40:54','2023-05-14 04:40:54',0,0),(4,9,'2023-05-14 04:40:54','2023-05-14 04:40:54',1,0),(5,11,'2023-05-18 15:35:29','2023-05-18 15:35:29',0,7),(6,12,'2023-05-19 21:36:46','2023-05-19 21:36:46',1,0),(7,17,'2023-07-30 07:00:29','2023-07-30 07:00:29',1,6),(8,20,'2023-08-13 09:21:34','2023-08-13 09:21:34',1,0),(9,24,'2023-08-13 09:21:34','2023-08-13 09:21:34',0,0),(10,27,'2023-08-13 09:21:34','2023-08-13 09:21:34',0,0);
/*!40000 ALTER TABLE `StudentOptionalAnswer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `StudentTest`
--

DROP TABLE IF EXISTS `StudentTest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `StudentTest` (
  `id` int NOT NULL AUTO_INCREMENT,
  `globalStudentTest` int NOT NULL,
  `test` int NOT NULL,
  `isChecked` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `StudentTest`
--

LOCK TABLES `StudentTest` WRITE;
/*!40000 ALTER TABLE `StudentTest` DISABLE KEYS */;
INSERT INTO `StudentTest` VALUES (1,1,1,1,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(2,1,2,1,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(3,2,2,0,'2023-05-14 02:40:23','2023-05-14 02:40:23'),(4,2,1,1,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(5,3,4,1,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(6,3,5,0,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(7,3,6,0,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(8,4,4,0,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(9,4,5,0,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(10,5,4,1,'2023-05-18 15:35:29','2023-05-18 15:35:29'),(11,5,5,0,'2023-05-18 15:35:29','2023-05-18 15:35:29'),(12,6,1,0,'2023-05-19 21:36:46','2023-05-19 21:36:46'),(13,6,2,0,'2023-05-19 21:36:46','2023-05-19 21:36:46'),(14,1,3,0,'2023-06-20 11:53:29','2023-06-20 11:53:29'),(15,9,3,0,'2023-06-20 11:53:29','2023-06-20 11:53:29'),(16,11,4,1,'2023-07-30 07:00:29','2023-07-30 07:00:29'),(17,11,5,0,'2023-07-30 07:00:29','2023-07-30 07:00:29'),(18,11,6,0,'2023-07-30 07:00:29','2023-07-30 07:00:29'),(19,12,4,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(20,12,5,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(21,1,3,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(22,1,3,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(23,19,4,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(24,19,5,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(25,1,3,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(26,20,4,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(27,20,5,0,'2023-08-13 09:21:34','2023-08-13 09:21:34'),(28,1,3,0,'2023-08-13 09:21:34','2023-08-13 09:21:34');
/*!40000 ALTER TABLE `StudentTest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Test`
--

DROP TABLE IF EXISTS `Test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Test` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `globalTest` int NOT NULL,
  `question` varchar(512) NOT NULL,
  `type` varchar(512) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Test`
--

LOCK TABLES `Test` WRITE;
/*!40000 ALTER TABLE `Test` DISABLE KEYS */;
INSERT INTO `Test` VALUES (1,'optional test',1,'question?','optional'),(2,'written test',1,'Question?','written'),(3,'test 2 written',2,'question?','written'),(4,'Written Test',3,'Question?','written'),(5,'Optional Test',3,'Question?','optional'),(6,'match test',3,'question','match'),(7,'Optional Test',4,'Question?','optional'),(8,'Match Test',4,'Question?','match'),(9,'Written Test',5,'Question 1?','written'),(10,'dh',6,'hdfj','match'),(11,'First test',7,'What is ?','match'),(12,'wqe',8,'123e','written'),(13,' b',8,'ert','written');
/*!40000 ALTER TABLE `Test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TransferPayment`
--

DROP TABLE IF EXISTS `TransferPayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TransferPayment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `companyName` varchar(512) NOT NULL,
  `phoneNumber` varchar(512) NOT NULL,
  `idNumber` varchar(512) NOT NULL,
  `proofImage` varchar(512) NOT NULL,
  `isLocal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TransferPayment`
--

LOCK TABLES `TransferPayment` WRITE;
/*!40000 ALTER TABLE `TransferPayment` DISABLE KEYS */;
INSERT INTO `TransferPayment` VALUES (1,'test','+96312130999911','12130999911','/media/orders/proof2023-06-20T11:47:44.510Z.jpeg',1),(2,'test','5467654','346653','/media/orders/proof2023-06-20T12:16:36.678Z.jpeg',1),(3,'test','+9635436346','6346346','/media/orders/proof2023-06-21T11:10:24.800Z.png',1),(4,'test','+963534636364','6346346346','/media/orders/proof2023-06-21T11:13:26.571Z.',1),(5,'test','+9635436346','6346346','/media/orders/proof2023-06-21T11:15:43.831Z.png',1),(6,'test','+963951217157','6346346346','/media/orders/proof2023-06-21T11:17:16.344Z.png',1),(7,'test','+9635436346','6346346','/media/orders/proof2023-06-21T11:28:58.802Z.png',1),(8,'123123','+9631231231','1231231','/media/orders/proof2023-07-30T15:07:08.030Z.png',1),(9,'test','+963930098478','0101010101','/media/orders/proof2023-07-30T15:45:28.476Z.png',1),(10,'test','+963930098478','12345789','/media/orders/proof2023-07-30T18:24:46.149Z.png',1),(11,'test','+963930098478','010101010101','/media/orders/proof2023-07-30T18:40:38.119Z.png',1);
/*!40000 ALTER TABLE `TransferPayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `User` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(512) NOT NULL,
  `lastName` varchar(512) NOT NULL,
  `email` varchar(512) NOT NULL,
  `phoneNumber` varchar(512) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `password` varchar(512) NOT NULL,
  `age` int DEFAULT NULL,
  `role` varchar(512) DEFAULT NULL,
  `profileImage` varchar(512) DEFAULT NULL,
  `secretCode` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'admin','admin','cc@cc.com','+963936222756','2023-05-12 12:36:07','nnn','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'admin','/media/users/admin_admin_18604.jpeg','AwTVpRPwsQeg8RKhq9V8SmC9P'),(2,'ahmad','afif','ahmadafif613@gmail.com','+963093633333','2023-05-12 14:21:16','Damascus','86440000d4d568d8f36a7a6c7709a927e77294f220824286bd015c43e1f5a0ac',18,'student','/media/users/ahmad_afif_13865.jpeg','wPDBbsC5HyrCItgeHfD5aUO3P'),(3,'teacher','ahmad','ahmad@gmail.com','+963092121212121',NULL,'Damascus','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',22,'instructor','/media/users/teacher_ahmad_20231.jpeg','32JdgAasso0SwQcMcRUgqteEG'),(4,'ahmad2','afif','ahmad2@gmail.com','+96312130999911',NULL,'Address','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'student','/media/users/ahmad2_afif_12302.jpeg','gXSjlFWV0q7iwhFZV9lD6Yc0h'),(5,'test','test','test@test.com','+9630936222756','2023-05-18 10:52:43','damas','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'student','/media/users/test_test_2589.jpeg','LSRiQVRqhNpzrPuwtm0nGDovO'),(6,'shareef ','hussami ','nbnrs683@gmail.com','+963936222756','2023-05-18 12:33:02','damas','5c7fc678c7b01fade4175383eab74299fdc179e121fad206a4128ed3cf7e7f7a',61,'instructor','/media/users/shareef _hussami _13241.jpeg','1fBuG1S8lHSmJ6witFui6XQLs'),(7,'ahmaddd','afifff','ahmadd@gmail.com','+963019212121','2023-05-18 15:37:56','aaaa','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'student','/media/users/ahmaddd_afifff_8513.jpeg','CB2wyWW1m7m0LSemlwRQGaOlw'),(8,'ahmad','teacher','ahmadaf@gmail.com','+963012913131','2023-05-18 16:16:20','Address','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',13,'instructor','/media/users/ahmad_teacher_307.png','qw4XrUDrMroqz31w8tSfpsltZ'),(9,'mhd','sultan','mhd.kh@izone-me.com','+963947170750','2023-06-13 06:34:44','bramkeh','9853532e663a0d05d7de3766e88a552c7bd634e311b874dfabcf27d91aadfdef',18,'student','/media/users/mhd_sultan_8430.jpeg','ai3UXwUUyk29TTocpBILZEwBb'),(11,'nnn','nnnn','user@instructpr.com','+963936222756','2023-06-13 19:28:49','nnn','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'instructpr','/media/users/nnn_nnnn_15643.jpeg','B8qt1OAcDJSq3VJwmlPvRljNE'),(12,'nnn1','nnn1','user@instructor.com','+963936222756','2023-06-13 19:29:11','nnn1','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'instructor','/media/users/nnn1_nnn1_9990.jpeg','6MYb2pAU00KAoDVXjoxXFuFS0'),(13,'fawzi','kabbani','fawzi.kabbani@gmail.com','+963944942904','2023-06-14 15:05:45','add','96c0ebb7e54773bb71179a4ec5e63d030fe1cbe6d8083515ef3d23c8065a2a42',36,'instructor','/media/users/fawzi_kabbani_11883.jpeg','naSdPQTta7TQ0IFISaPGWyf4a'),(14,'fawzi','kabbani','fawzi.kabbani+1@gmail.com','+963944942904','2023-06-14 15:06:58','add','679e7e817264eb968755cce08a019ae2af0c20ea960504a145e0bc4932b88676',18,'instructor','/media/users/fawzi_kabbani_7676.jpeg','AINnDop8lxqeFgS1wUmyp0HrX'),(15,'ahmad','afif','ahmad3@gmail.com','+9630922222222','2023-06-16 14:46:07','Damascus','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'student','/media/users/ahmad_afif_3266.jpeg','enFzoxe2lWmz5nlncFoZiAcX4'),(16,'mahmod','test','mahmod.test@gmail.com','+963951217157','2023-06-18 11:09:40','test test','15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225',23,'student','','8YHfnE8heKXcrWRI4w1wQjZsl'),(17,'mahmod','test','mahmod.test@gmail.com','+963951217157','2023-06-18 11:09:40','test test','15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225',23,'student','','Tpk1KMewUWBv8WF7bjOIQLIb6'),(18,'Mahmod','Jnead','mahmod.jnead@izone-me.com','+963951217157','2023-06-19 10:43:21',' ff','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',30,'student','/media/users/Mahmod_Jnead_10233.jpeg','Tk067xMovDqYyDUTwWG8wTFXM'),(19,'test','test','test@test.co','+9630936222756','2023-06-21 11:51:33','twst','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'student','/media/users/test_test_21931.jpeg','12JPuvTZXcO88vb6YTYxUIDeW'),(20,'Gk D','Araña','cissci12@gmail.com','+963936222756','2023-06-23 16:52:30','address','ffc2c0326f606586ea9177f2d291d43b97a77b31905c175d4618de4f9a6cb6dc',18,'student','/media/users/Gk D_Araña_21239.https:/','J6Aq3ZsXSGAieayzgA4dovU53'),(21,'nnn','nnnn','user@student.com','+963936222756','2023-06-25 16:53:38','nnn','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'student','/media/users/nnn_nnnn_5595.jpeg','8XmJwG9pQWa0dqjqGL0tP1Nq8'),(22,'Mahmod2','test','test@tst.com','+963951217157','2023-07-06 13:43:45','Address','07010d789ed2c6c891d75f55ab14fd467970ea3b2e208853f21153b1960798aa',22,'student','/media/users/Mahmod2_test_20828.jpeg','twOTp87hyx1cuwi7v5wuwvAuk'),(23,'Mahmod','test','test@tst.com','+963951217157','2023-07-06 13:43:45','Address','07010d789ed2c6c891d75f55ab14fd467970ea3b2e208853f21153b1960798aa',22,'student','/media/users/Mahmod_test_9534.png','uIlk9uC0fYHeycJGmMMWFDwdd'),(24,'nnn','nnnn','tedtt@testt.com','+9630936222546','2023-07-06 14:39:40','address','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'student','/media/users/nnn_nnnn_2142.jpeg','AVG9QfQjx4EkrqF3wk9pSpQTC'),(25,'test','test','xefiyex132@sportrid.com','+963930098478','2023-07-30 15:02:57','test','d0dce9aff3de7736851e58a5e28d21ce1588f535b90c211c086aa82a48c5be3c',30,'student','','qd3CXdLckeESDnal5Hflj1pNn'),(26,'Anas','Anas','anas@gmail.com','+963912345678','2023-08-15 06:18:36','kkk','1718c24b10aeb8099e3fc44960ab6949ab76a267352459f203ea1036bec382c2',33,'student','','yF29PaYRvLd1AO5KnXqPh6mwf'),(27,'omar','hashem','omar.h@aigate.me','+963999999999','2023-08-16 13:59:19','heus','96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e',18,'student','','99L7KeIFLoGNY6gGjc8QMljw3');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Value`
--

DROP TABLE IF EXISTS `Value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Value` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL,
  `iconSvg` longtext NOT NULL,
  `info` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Value`
--

LOCK TABLES `Value` WRITE;
/*!40000 ALTER TABLE `Value` DISABLE KEYS */;
/*!40000 ALTER TABLE `Value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Video`
--

DROP TABLE IF EXISTS `Video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Video` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `videoUrl` varchar(512) NOT NULL,
  `lesson` int NOT NULL,
  `videoDuration` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Video`
--

LOCK TABLES `Video` WRITE;
/*!40000 ALTER TABLE `Video` DISABLE KEYS */;
INSERT INTO `Video` VALUES (1,'Lesson 1','Video Description','/media/courses/videos/Lesson1_video_url3655.mp4',2,7),(2,'lesson 1','descriotion','/media/courses/videos/lesson1_video_url16152.mp4',3,7),(3,'Lesson 1','Lesson Description','/media/courses/videos/Lesson1_video_url17877.mp4',9,4),(4,'aaaa','ddd','/media/courses/videos/aaaa_video_url5084.mp4',11,0),(5,'www','ddd','/media/courses/videos/www_video_url559.mp4',13,5),(6,'fsdfs','fsdfsdfsdf','/media/courses/videos/fsdfs_video_url2739.mp4',17,30),(7,'rerwersdfsdf','werwersdfsdf','/media/courses/videos/rerwersdfsdf_video_url2689.mp4',16,30);
/*!40000 ALTER TABLE `Video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VirtualClassroom`
--

DROP TABLE IF EXISTS `VirtualClassroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `VirtualClassroom` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` longtext NOT NULL,
  `coverImage` varchar(512) DEFAULT NULL,
  `user` int NOT NULL,
  `chatGroup` int DEFAULT NULL,
  `files` text,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VirtualClassroom`
--

LOCK TABLES `VirtualClassroom` WRITE;
/*!40000 ALTER TABLE `VirtualClassroom` DISABLE KEYS */;
INSERT INTO `VirtualClassroom` VALUES (1,'Vrc 1','Vrc Desciption','/media/virtual-classrooms/Vrc 1.jpeg',8,1,NULL,'2023-05-14 04:40:54','2023-05-14 04:40:54'),(2,'Vrc 1','Vrc Description','/media/virtual-classrooms/Vrc 1.jpeg',3,2,NULL,'2023-05-18 15:35:29','2023-05-18 15:35:29'),(3,'Vrc 2','Decription','/media/virtual-classrooms/Vrc 2.jpeg',3,3,NULL,'2023-05-19 20:43:00','2023-05-19 20:43:00'),(4,'Vrc 3','dddd','/media/virtual-classrooms/Vrc 3.jpeg',3,4,NULL,'2023-05-19 21:36:46','2023-05-19 21:36:46');
/*!40000 ALTER TABLE `VirtualClassroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Wishlist`
--

DROP TABLE IF EXISTS `Wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Wishlist` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `item` int NOT NULL,
  `quantity` int NOT NULL,
  `type` varchar(512) NOT NULL,
  `inWishlist` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Wishlist`
--

LOCK TABLES `Wishlist` WRITE;
/*!40000 ALTER TABLE `Wishlist` DISABLE KEYS */;
INSERT INTO `Wishlist` VALUES (3,9,1,1,'course',1,'2023-06-13 06:37:54','2023-06-13 06:37:54'),(5,3,1,1,'shopItem',1,'2023-06-14 14:41:32','2023-06-14 14:41:32'),(6,15,1,1,'shopItem',1,'2023-06-16 15:10:48','2023-06-16 15:10:48'),(7,4,1,1,'shopItem',1,'2023-06-19 12:16:33','2023-06-19 12:16:33'),(8,19,1,1,'shopItem',1,'2023-06-21 11:54:46','2023-06-21 11:54:46'),(10,19,1,-1,'course',1,'2023-06-21 11:55:28','2023-06-21 11:55:28'),(12,2,1,1,'shopItem',1,'2023-06-26 10:26:26','2023-06-26 10:26:26'),(16,3,1,1,'course',1,'2023-07-23 14:21:44','2023-07-23 14:21:44'),(17,25,1,1,'course',1,'2023-07-30 15:06:31','2023-07-30 15:06:31');
/*!40000 ALTER TABLE `Wishlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WrittenTestAnswer`
--

DROP TABLE IF EXISTS `WrittenTestAnswer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `WrittenTestAnswer` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` varchar(512) NOT NULL,
  `isTrue` tinyint(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `studentTest` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WrittenTestAnswer`
--

LOCK TABLES `WrittenTestAnswer` WRITE;
/*!40000 ALTER TABLE `WrittenTestAnswer` DISABLE KEYS */;
INSERT INTO `WrittenTestAnswer` VALUES (1,'Answerrr',1,'2023-05-14 02:40:23','2023-05-14 02:40:23',2),(2,'answer 2',0,'2023-05-14 02:40:23','2023-05-14 02:40:23',3),(3,'Answer 1',1,'2023-05-14 04:40:54','2023-05-14 04:40:54',5),(4,'utirurur',0,'2023-05-14 04:40:54','2023-05-14 04:40:54',8),(5,'anaswer new',1,'2023-05-18 15:35:29','2023-05-18 15:35:29',10),(6,'answer!',0,'2023-05-19 21:36:46','2023-05-19 21:36:46',13),(7,'making api',1,'2023-07-30 07:00:29','2023-07-30 07:00:29',16),(8,'first answer',0,'2023-08-13 09:21:34','2023-08-13 09:21:34',19),(9,'my first answer',0,'2023-08-13 09:21:34','2023-08-13 09:21:34',23),(10,'tgdtd',0,'2023-08-13 09:21:34','2023-08-13 09:21:34',26);
/*!40000 ALTER TABLE `WrittenTestAnswer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-08-16 20:30:03
